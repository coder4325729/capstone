<?php
function select_StudentCurriculumSubject($syear) {
    $qry = "SELECT "
         . "subjects.SubjectID AS SubjectID, "
         . "curriculum.syear AS syear, "
         . "curriculum.Semester AS Semester "
         . "FROM subjects "
         . "JOIN curriculumsubject ON subjects.SubjectID = curriculumsubject.SubjectID "
         . "JOIN curriculum ON curriculumsubject.CurriculumID = curriculum.CurriculumID"
         . "WHERE Curriculum.syear = '$syear' ";

    return $qry;
}

function console_log($output, $with_script_tags = true) {
    $js_code = 'console.log(' . json_encode($output, JSON_HEX_TAG) . ');';
    if ($with_script_tags) {
        $js_code = '<script>' . $js_code . '</script>';
    }
    echo $js_code;
}
?>