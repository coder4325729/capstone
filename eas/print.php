<?php include 'db_connect.php';
include 'query2.php'; 
include 'formatpencode.php';

// Check if the student ID is provided in the URL
if (isset($_GET['id'])) {
    // Retrieve the student ID from the URL
    $student_id = $_GET['id'];

} else {
    // Handle the case when the student ID is not provided in the URL
    echo "Student ID not provided.";
    exit; // Exit PHP execution
}

$student_curriculum_query = $conn->query("SELECT year, track, student_code, CONCAT(firstname, ' ', middlename, ' ', lastname) as name FROM students WHERE id = $student_id");
$row = $student_curriculum_query->fetch_assoc();
$year = $row['year'];
$track = $row['track'];
$name = $row['name'];
$student_code = $row['student_code'];

// Query to retrieve the school year based on the curriculum ID
$schoolyear_query = $conn->query("SELECT syear FROM curriculum WHERE syear = $year");
$row = $schoolyear_query->fetch_assoc();
$schoolyear = $row['syear'];

// Check if the form is submitted
$status_name = array("TBA", "Ongoing", "Pass", "Fail", "INC", "Dropped");
$status_color = array("White", "Green", "#add8e6", "#EE6B6E", "Red", "Black");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Printable Version</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap4.min.css">
    <style>
@media print {
    .print-container {
        display: flex;
        flex-wrap: nowrap;
        justify-content: space-between;
    }

    table.table-bordered {
        margin: 0px 0px 4px 0px; /* Add some space below tables */
        font-size: 10px; /* Reduce font size to fit content */
        border-collapse: collapse; /* Collapse table borders */
    }

    table.table-bordered th,
    table.table-bordered td {
        border: 2px solid black !important; /* Add border to table cells */
        padding: 4px 1px 4px 1px; /* Adjust cell padding */
        text-align: left; /* Align text to the left */
    }

    .table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th {
    border: 2px solid black; 
    padding: 4px 1px 4px 1px; /* Adjust cell padding */
    }

    .card.card-outline.card-secondary{
        padding: 0px;
        margin: 0px;
    }
}

    h6, p {
        font-weight: bold;
        text-align: center;
        font-size: 14px;
        }
    .headtitle{
        margin-top:5px;
    }

    .semester{
        text-align: left;
        font-size: 15px;
        margin-bottom: 5px;
    }
    .student-id, .student-name, .date{
        display: inline;
        font-size: 15px;
        margin:15px;
        justify-content: space-around;
    }
    .foot {
    display: flex;
    justify-content: space-between;
    width: 100%; /* Ensures the foot div spans the entire width of the page */
}


</style>
</head>

<body>

<h6 class="headtitle">University Of Pangasinan</h6>
<h6>BACHELOR OF SCIENCE IN INFORMATION TECHNOLOGY</h6>
<p><?php echo " Effective $year - ", $year + 1;?></p>
<p style="margin: 0 5px 5px 5px"><?php echo "$track TRACK"; ?></p>
 
<div class="print-container">
    <div class="card card-outline card-secondary">
        <div class="head">
            <div class="card-tools">
            </div>
        </div>
        <div class="card-body row" style="padding-top: 10px;">
            <?php
            // Loop through each curriculum
            for ($i = 1; $i <= 8; $i++) {
                if ($i > 8) {
                    // Fetch the curriculum data from the database for the descriptive column with a specific condition
                    $curriculumTitle_query = $conn->query("SELECT descriptive FROM curriculum WHERE semester = $i AND syear = $schoolyear");
                    $curriculumTitle_row = $curriculumTitle_query->fetch_assoc();
                    $curriculumTitle = isset($curriculumTitle_row['descriptive']) ? $curriculumTitle_row['descriptive'] : 'No data available';
                } else {
                    $curriculumTitle = ($i % 2 == 1 ? "First" : "Second") . " Semester";
                    if ($i == 1){
                        echo '<div class="col-12" style="text-align:center;"> <b> FIRST YEAR </b> </div>';
                        }
                        else if($i == 3) echo '<div class="col-12" style="text-align:center;"> <b> SECOND YEAR </b> </div>';
                        else if($i == 5) echo '<div class="col-12" style="text-align:center;"> <b> THIRD YEAR </b> </div>';
                        else if($i == 7) echo '<div class="col-12" style="text-align:center;"> <b> FOURTH YEAR </b> </div>';
                }

                // Fetch data from the database
                $qry = $conn->query(select_StudentCurriculumSubject(2024, $i, $student_id));
                $r = array();
                while ($row = $qry->fetch_assoc()) {
                    if (empty($r)) {
                        $r = [$row['Pencode'] => $row];
                    } else {
                        if  (array_key_exists($row['Pencode'], $r)) {
                            $postreq = "," . $row['Postrequisite'];
                            $r[$row['Pencode']]['Postrequisite'] .= $postreq;
                        } else {
                            $r[$row['Pencode']] = $row;
                        }
                    }
                }

                // Check if there are any rows fetched for this curriculum
                if (!empty($r)) {
                    ?>
                    <div class="col-6">
                        <h6 class="semester"><?php echo $curriculumTitle; ?></h6>
                        <table class="table table-bordered" id="list_<?php echo $i; ?>">
                            <thead>
                            <tr>
                                <th>Pen Code</th>
                                <th>Descriptive Title</th>
                                <th>Lec</th>
                                <th>Lab</th>
                                <th>Total</th>
                                <th>Pre-requisite</th>
                                <th>Grade</th>
                                <!-- <th class= "action">Action</th> -->
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($r as $key => $value) {
                                $row = $value;
                                ?>
                                <tr>
                                    <td class="col-2" id="penCodeColumn_<?php echo formatpencode($row['Pencode']) ?>">
                                        <b id="pencode_<?php echo formatpencode($row['Pencode']) ?>" class="pencode <?php echo gethighlight($row['Grade'], $row['Prerequisite'], $row['Status']) ?>"><?php echo ucwords($row['Pencode']) ?></b>
                                    </td>
                                    <td class="col-4"><?php echo ucwords($row['Description']) ?></td>
                                    <td class="col-1"><b><?php echo ucwords($row['Lec']) ?></b></td>
                                    <td class="col-1"><?php echo ucwords($row['Lab']) ?></td>
                                    <td class="col-1"><?php echo ucwords($row['Lab'] + $row['Lec']) ?></td>
                                    <td class="col-2" data-postrequisite-id="<?php echo formatpencode($row['Postrequisite'])?>" id="prerequisite_<?php echo formatpencode($row['Pencode'])?>" data-prerequisite-id="<?php echo formatpencode($row['Prerequisite'])?>"><?php echo ucwords($row['Prerequisite']) ?></td>
                                    <td class="col-1" contenteditable="true" id="grade_<?php echo formatpencode($row['Pencode']) ?>" data-grade-id="<?php echo $row['StudentCurriculumSubjectID'] ?>"><?php echo ucwords($row['Grade']) ?></td>
                                    <!-- <td class="text-center col-1 action">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-danger btn-flat delete_StudentCurriculumSubject" data-id="<?php echo $row['StudentCurriculumSubjectID'] ?>">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        </div>
                                    </td> -->
                                </tr>
                            <?php } ?>
                            </tbody>
                            <tr><td colspan ="2" style="text-align: center;"><b>TOTAL UNITS</b></td>
                                <td><?php   $total_lec = 0;
                                            $total_lab = 0;
                                            foreach ($r as $key => $value) {
                                                $total_lec += $value['Lec'];
                                                $total_lab += $value['Lab'];
                                            }
                                echo  $total_lec; ?></td>
                                <td><?php   $total_lec = 0;
                                            $total_lab = 0;
                                            foreach ($r as $key => $value) {
                                                $total_lec += $value['Lec'];
                                                $total_lab += $value['Lab'];
                                            }
                                echo  $total_lab; ?></td>
                                <td> <?php
                                            // Calculate total units for the entire table
                                            $total_lec = 0;
                                            $total_lab = 0;
                                            foreach ($r as $key => $value) {
                                                $total_lec += $value['Lec'];
                                                $total_lab += $value['Lab'];
                                            }
                                            echo $total_lec + $total_lab;
                                        ?></td>
                                <td></td>

                                <td>
                                       
                                    </td>
                        </tr>
                        </table>
                    </div>
                <?php
                }
            }
            ?>
        </div>
    
    </div>
</div>
<div class="foot">
<p class="student-id"><?php echo "Student Number: $student_code"; ?></p>
<p class="student-name"><?php echo "Name: $name"; ?></p>
<p class="date">Date printed: <?php echo date('Y-m-d');?></p>
        </div>

<script>
$(document).ready(function(){
    $('#list').dataTable();

    $('.delete_StudentCurriculumSubject').click(function(){
        var studentCurriculumSubjectID = $(this).attr('data-id');
        if(confirm("Are you sure to delete this Subject?")) {
            $.ajax({
                url: 'delete_scs.php',
                method: 'POST',
                data: { StudentCurriculumSubjectID: studentCurriculumSubjectID },
                success: function(resp){
                    alert(resp);
                    location.reload();
                },
                error: function(xhr, status, error) {
                    console.error(xhr.responseText);
                }
            });
        }
    });
    $(document).on('click', '.manage_grade', function(){
			uni_modal("Manage Grade","manage_grade.php?StudentCurriculumSubjectID="+$(this).attr('data-id'))
		});

    $('.insert_grade').click(function(){
        uni_modal("Manage Curriculum","insert_grade.php?StudentCurriculumSubjectID="+$(this).attr('data-id'));
    });




    function updatePencodeColor(pencode, grade, is_postreq = false){
        clearpencodecolor(pencode)
        var color = "pencodered"; 
        var postreq_grade = 0;
        if (is_postreq){
            postreq_grade = $("#grade_" + pencode).text();
            console.log(postreq_grade);
            if (grade >= 50){
                if(postreq_grade >= 50){
                    color = "pencodeblack";
                }
                else{
                    color = "";
                }
            }
        }
        else{
            if (grade >= 50){
                color = "pencodeblack";           
            }   
            else if (grade > 0){
                color = "";
            }
            else{ 
            }
        }
       
        $("#pencode_" + pencode).addClass(color);
    }

    function updatepostreq(pencode, grade){
        pencode
    }

    function clearpencodecolor(pencode){
        $("#pencode_"+ pencode).removeClass("pencodered pencodegreen pencodeblack")
    }

    function pencodetopostrequisite(pencode, postrequisite) {
  var similarPostrequisites = {}; 

  
  if (!similarPostrequisites[pencode]) {
    similarPostrequisites[pencode] = postrequisite;
  } else {

    similarPostrequisites[pencode] += ', ' + postrequisite;
  }

 
  console.log('Pencode:', pencode, 'Postrequisites:', similarPostrequisites[pencode]);
}


$('[contenteditable="true"]').on('input', function() {
  var studentCurriculumSubjectID = $(this).data('grade-id');
  var gradeInput = this;
  var pencode = $(this).attr('id');
  pencode = pencode.replace("grade_", "");
  var grade = $(this).text();
  var prerequisite = $(this).siblings('td[data-prerequisite-id]').data('prerequisite-id');
  var postrequisites = $(this).siblings('td[data-postrequisite-id]').data('postrequisite-id')
  var postrequisite = postrequisites.split(',');

 

    updatePencodeColor(pencode, grade);
    
    for (p in postrequisite){
       
        updatePencodeColor(postrequisite[p], grade, true)
        
    }
    
    

    
     var formData = [];
     var grade = $(this).text();
     formData.push({ StudentCurriculumSubjectID: studentCurriculumSubjectID, grade: grade });
    
        var success = false;
        $.ajax({
            url: 'process2.php',
            method: 'POST',
            data: { formData: formData },
            success: function(resp) {
                console.log(resp);
                success = true;
            },
            error: function(xhr, status, error) {
                console.error(xhr.responseText);
            }
        });
      $
    });
   
    
    
   
    function getPenCodeIdByPrerequisite(prerequisiteId) {
        var penCodeId = null; 

        $("#list_<?php echo $i; ?> tbody tr").each(function() {
            var prerequisiteTd = $(this).siblings('td[data-prerequisite-id="' + prerequisiteId + '"]').data('prerequisite-id');  

            if (prerequisiteTd.length) { 
                penCodeId = $(this).find('td:first-child').attr('id'); 
                return false; 
            }
        });

        return penCodeId; 
    }
    
   


    
    


});
$(document).ready(function(){
    // Function to load the content of the href page into the current page
    function loadHrefPageContent() {
        var href = $('#printButton').attr('href'); // Get the href attribute of the print button
        $('#contentToPrint').load(href); // Load the content of the href page into the current page
    }

    // Click event for the print button
    $('#printButton').click(function() {
        loadHrefPageContent(); // Load the content of the href page
        setTimeout(function() {
            window.print(); // Trigger the print functionality after a short delay
        }, 1000); // Adjust the delay as needed
    });
});



</script>

</body>
</html>