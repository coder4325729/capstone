<?php
// Include your database connection
include 'db_connect.php';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $grade = $_POST['Grade'];
    $studentCurriculumSubjectID = $_POST['StudentCurriculumSubjectID'];

    // Check if Postrequisite is set
    if (isset($_POST['Postrequisite'])) {
        $postrequisite = $_POST['Postrequisite'];

        // Check grade and update status accordingly
        $status = ($grade >= 3.25) ? 8 : 9;

        // Find SubjectIDs of postrequisite subjects
        $pencodes = array_map('intval', $_POST['Postrequisite']);
        $pencodesString = implode(',', $pencodes);
        $qrySubjectIDs = "SELECT SubjectID FROM subjects WHERE Pencode IN ($pencodesString)";

        // Execute the query to get SubjectIDs
        $subjectIDsResult = mysqli_query($connection, $qrySubjectIDs);
        
        if ($subjectIDsResult) {
            $subjectIDs = mysqli_fetch_all($subjectIDsResult, MYSQLI_ASSOC);

            // Now update the statuses of the postrequisite subjects
            // Construct the SQL query
            $qryUpdate = "UPDATE studentcurriculumsubject AS scs
                          JOIN subjects AS sub ON scs.SubjectID = sub.SubjectID
                          SET scs.status = $status
                          WHERE scs.StudentCurriculumSubjectID = $studentCurriculumSubjectID
                          AND sub.Pencode IN ($pencodesString)";

            // Execute the update query
            mysqli_query($connection, $qryUpdate);

            // Check for errors
            if (mysqli_errno($connection)) {
                echo "Update failed: " . mysqli_error($connection);
            } else {
                echo "Update successful!";
            }
        } else {
            echo "Failed to fetch subject IDs.";
        }
    } else {
        // No Postrequisite provided, do nothing
        echo "No Postrequisite provided, no update executed.";
    }
} else {
    // Form not submitted
    echo "Form not submitted.";
}
?>
