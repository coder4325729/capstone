<?php
include 'db_connect.php';

// Check if form is submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Get form data
    $table_id = $_POST['table_id']; // Identifier to determine which table to insert into

    // Define the table names and their corresponding column names
    $tables = array(
        1 => array('table_name' => 'table1', 'columns' => array('pen_code', 'descriptive_title', 'lecture_units', 'lab_units', 'total_units', 'pre_requisite', 'grade')),
        2 => array('table_name' => 'table2', 'columns' => array('column1', 'column2', 'column3')) // Define columns for other tables similarly
        // Add entries for other tables
    );

    // Ensure the table_id is valid
    if (array_key_exists($table_id, $tables)) {
        $table_name = $tables[$table_id]['table_name'];
        $columns = $tables[$table_id]['columns'];

        // Prepare column placeholders for the insert query
        $column_placeholders = implode(', ', array_fill(0, count($columns), '?'));

        // Dynamically build the insert query
        $insert_query = "INSERT INTO $table_name (" . implode(', ', $columns) . ") VALUES ($column_placeholders)";
        $stmt = $conn->prepare($insert_query);

        // Bind parameters
        $bind_types = str_repeat('s', count($columns)); // Assuming all columns are strings
        $stmt->bind_param($bind_types, ...array_map(function ($col) {
            return $_POST[$col];
        }, $columns));

        // Execute query
        if ($stmt->execute()) {
            // Data inserted successfully
            header("Location: your_page.php"); // Redirect to the page with the form and table
            exit();
        } else {
            // Error occurred while inserting data
            echo "Error: " . $conn->error;
        }

        // Close statement
        $stmt->close();
    } else {
        echo "Invalid table ID.";
    }
}

// Close connection
$conn->close();
?>
