<?php
include 'db_connect.php';
include 'admin_class.php';

// Check if file is uploaded
if(isset($_FILES['file']['name'])) {
    $file = $_FILES['file']['tmp_name'];

    // Open the file for reading
    $handle = fopen($file, "r");

    // Skip the header row
    fgetcsv($handle);

    // Initialize an array to store the newly inserted student IDs
    $newlyInsertedIDs = array();

    // Initialize an array to store student IDs by year
    $studentIDsByYear = array();

    // Read and process each row of the CSV file
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        // Escape special characters and sanitize data
        $student_code = mysqli_real_escape_string($conn, ucwords(strtolower($data[0]))); // Assuming Pencode is the first column
        $firstname = mysqli_real_escape_string($conn, ucwords(strtolower($data[1]))); // Assuming Description is the second column
        $middlename = mysqli_real_escape_string($conn, ucwords(strtolower($data[2]))); // Assuming Lec is the third column
        $lastname = mysqli_real_escape_string($conn, ucwords(strtolower($data[3]))); // Assuming Lab is the fourth column
        $year = mysqli_real_escape_string($conn, ucwords(strtolower($data[4]))); // Assuming Prerequisite is the fifth column
        $track = mysqli_real_escape_string($conn, strtoupper($data[5])); 

        // Insert the data into the students table
        $sql = "INSERT INTO students (student_code, `firstname`, middlename, lastname, year, track) 
                VALUES ('$student_code', '$firstname', '$middlename', '$lastname', '$year', '$track')";

        if (!mysqli_query($conn, $sql)) {
            echo "Error inserting data: " . mysqli_error($conn);
            exit;
        }

        // Get the newly inserted StudentID and store it in the array
        $id = mysqli_insert_id($conn);
        $newlyInsertedIDs[] = $id;

        // Store the StudentID in the array indexed by year
        if (!isset($studentIDsByYear[$year])) {
            $studentIDsByYear[$year] = array();
        }
        $studentIDsByYear[$year][] = $id;
    }

    fclose($handle);

    // Call insert_subjecttostudent function with the array of newly inserted IDs
    $admin_class = new Action();
    foreach ($studentIDsByYear as $year => $studentIDs) {
        $admin_class->insert_subjecttostudent($year, $studentIDs);
    }

    echo "Data loaded successfully";
} else {
    echo "No file uploaded";
}

mysqli_close($conn);
?>
