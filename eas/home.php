<?php include('db_connect.php') ?>
<?php if($_SESSION['login_type'] == 1): ?>
    <!-- Display info boxes for admin users -->
    <!-- Info boxes -->
    <div class="row">
        <!-- Total Students -->
        <div class="col-12 col-sm-6 col-md-4">
            <div class="info-box">
                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-users"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Total Students</span>
                    <span class="info-box-number">
                        <?php echo $conn->query("SELECT * FROM students")->num_rows; ?>
                    </span>
                </div>
            </div>
        </div>
        <!-- Total Classes -->
        <div class="col-12 col-sm-6 col-md-4">
            <div class="info-box">
                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-th-list"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Total Curriculum</span>
                    <span class="info-box-number">
                        <?php echo $conn->query("SELECT DISTINCT syear FROM curriculum")->num_rows; ?>
                    </span>
                </div>
            </div>
        </div>
        <!-- Total Subjects -->
        <div class="col-12 col-sm-6 col-md-4">
            <div class="info-box">
                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-book"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Total Subject</span>
                    <span class="info-box-number">
                        <?php echo $conn->query("SELECT * FROM subjects")->num_rows; ?>
                    </span>
                </div>
            </div>
        </div>
    </div>
<?php else: ?>
    <!-- Display welcome message for non-admin users -->
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                Welcome <?php echo $_SESSION['login_name'] ?>!
            </div>
        </div>
    </div>
<?php endif; ?>

<!-- Display audit table -->

