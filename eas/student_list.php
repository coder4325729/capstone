<?php include 'db_connect.php' ?>
<style>
    .btn-square1 {
    width: 50px; /* Adjust the width as needed */
    height: 50px; /* Same as the width to make it square */
    border-radius: 0;
    background-color: #FFBF00; /* Removes rounded corners */
	color: white;
}
.btn-square {
    width: 50px; /* Adjust the width as needed */
    height: 50px; /* Same as the width to make it square */
    border-radius: 0;
    background-color: green;
	color: white; /* Removes rounded corners */
}
</style>
<div class="col-lg-12">
	<div class="card card-outline card-secondary">
		<div class="card-header">
			<div class="card-tools">
				<a class="btn btn-block btn-sm btn-default btn-flat border-secondary " href="./index.php?page=new_student"><i class="fa fa-plus"></i> Add New</a>
			</div>
		</div>
		<div class="card-body">
			<div class="table-responsive"> <!-- Wrap the table with this div -->
				<table class="table tabe-hover table-bordered" id="list">
					<colgroup>
						<col width="10%">
						<col width="15%">
						<col width="25%">
						<col width="10%">
					<col width="20%">
						<col width="20%">
					</colgroup>
					<thead>
						<tr>
							<th class="text-center">#</th>
							<th>Student ID</th>
							<th>Name</th>
							<th>Curriculum Year</th>
							<th>Track</th>
					
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$i = 1;
						$qry = $conn->query("SELECT s.*, s.track, CONCAT(s.firstname, ' ', s.middlename, ' ', s.lastname) as name FROM students s ORDER BY name ASC");

						while($row= $qry->fetch_assoc()):
							?>
							
						<tr>
							<td class="text-center"><?php echo $i++ ?></td>
							<td class=""><b><?php echo $row['student_code'] ?></b></td>
							<td><b><?php echo ucwords($row['name']) ?></b></td>
						    <td><b><?php echo ucwords($row['year']) ?></b></td>
						<td><b><?php echo ucwords($row['track']) ?></b></td>
						
							<td class="text-center">
			                    <div class="btn-group border-0" >
			                        <a style="background-color: #1F3761;" href="index.php?page=edit_student2&id=<?php echo $row['id'] ?>" class="btn btn-secondary btn-flat ">
			                          <i class="fas fa-edit"></i>
			                        </a>

			

	<a href="./index.php?page=advise&id=<?php echo $row['id']; ?>" class="btn border-0 btn-square" role="button" data-id="<?php echo $row['id']; ?>">
	    <i class="fas fa-book"></i>
	</a>

			                        <button type="button" class="btn btn-danger btn-flat delete_student" data-id="<?php echo $row['id'] ?>">
			                          <i class="fas fa-trash"></i>
			                        </button>
			                      </div>
							</td>
						</tr>	
					<?php endwhile; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<style>
	table td{
		vertical-align: middle !important;
	}
</style>
<script>
    $(document).ready(function(){
        $('#list').dataTable();
        $(document).on('click', '.save_StudentCurriculumSubject', function(){
            uni_modal("Input Subject to Student","input_curriculum.php?id="+$(this).attr('data-id'));
        });
        $(document).on('click', '.delete_student', function(){
            _conf("Are you sure to delete this Student?","delete_student",[$(this).attr('data-id')]);
        });
    });

    function delete_student($id){
        start_load();
        $.ajax({
            url:'ajax.php?action=delete_student',
            method:'POST',
            data:{id:$id},
            success:function(resp){
                if(resp==1){
                    alert_toast("Data successfully deleted",'success');
                    setTimeout(function(){
                        location.reload();
                    },1500);
                }
            }
        });
    }
</script>
