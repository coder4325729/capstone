<?php
include 'db_connect.php';
if(isset($_GET['StudentCurriculumSubjectID'])){
	$qry = $conn->query("SELECT * FROM studentcurriculumsubject where StudentCurriculumSubjectID ={$_GET['StudentCurriculumSubjectID']}")->fetch_array();
	foreach($qry as $k => $v){
		$$k = $v;
	}
}
?>
<div >
	<form id="curriculum-form" method="post">
		<input type="hidden" name="StudentCurriculumSubjectID" value="<?php echo isset($StudentCurriculumSubjectID) ? $StudentCurriculumSubjectID : '' ?>">
		<div id="msg" class="form-group"></div>
		
		
		<div class="form-group">
    <p>STATUS</p>    
    <div>
        <input type="radio" name="Status" id="status_pass" value="2" <?php isset($Status) && $Status == '2' ? 'checked' : '' ?>>
        <label for="status_pass" class="control-label">Pass</label>
    </div>   
    <div>
        <input type="radio" name="Status" id="status_fail" value="3" <?php isset($Status) && $Status == '3' ? 'checked' : '' ?>>
        <label for="status_fail" class="control-label">FA</label>
    </div>  
    <div>
        <input type="radio" name="Status" id="status_ongoing" value="1" <?php isset($Status) && $Status == '1' ? 'checked' : '' ?>>
        <label for="status_ongoing" class="control-label">NC</label>
    </div> 
    <div>
        <input type="radio" name="Status" id="status_inc" value="4" <?php isset($Status) && $Status == '4' ? 'checked' : '' ?>>
        <label for="status_inc" class="control-label">INC</label>
    </div>
</div>

	</form>
</div>


<div class="modal fade" id="successModal" tabindex="-1" aria-labelledby="successModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="successModalLabel">Success</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                Data successfully saved.
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('#curriculum-form').submit(function(e){
            e.preventDefault();
            start_load();
            var formData = $('#curriculum-form').serialize();
            $.ajax({
                url: 'process8.php',
                method: 'POST',
                data: formData,
                success: function(response){
                    $('#successModal').modal('show');
                    setTimeout(function(){
							location.reload()	
						},1750)
                    end_load();
                }
            });
        });
    });
</script>