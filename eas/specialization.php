<?php
// Connect to the database
$conn = mysqli_connect("localhost", "root", "", "eas_db");

// Check the connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

// Retrieve all HTML content from the database
$result = mysqli_query($conn, "SELECT * FROM specialization ORDER BY id DESC");
$htmlContents = [];

if ($result && mysqli_num_rows($result) > 0) {
    while ($row = mysqli_fetch_assoc($result)) {
        $htmlContents[] = [
            'id' => $row['id'],
            'tcontent' => $row['tcontent']
        ];
    }
} else {
    $htmlContents[] = ["No HTML content found."];
}

// Close the connection
mysqli_close($conn);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Saved HTML Content</title>
</head>
<body>
    <?php foreach ($htmlContents as $htmlContent): ?>
    <div class="content-container">
        <div class="content">
            <?php echo $htmlContent['tcontent']; ?>
        </div>
        <div class="actions">
            <button onclick="window.print()">Print</button>
            <a href="editspe.php?id=<?php echo $htmlContent['id']; ?>">Edit</a>
            <a href="delete.php?id=<?php echo $htmlContent['id']; ?>" onclick="return confirm('Are you sure you want to delete this content?')">Delete</a>
        </div>
        <hr>
    </div>
    <?php endforeach; ?>
</body>
</html>