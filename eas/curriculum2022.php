<?php include 'db_connect.php'; ?>
<?php
// Check if the student ID is provided in the URL
if (isset($_GET['student_code'])) {
    // Retrieve the student ID from the URL
    $student_code = $_GET['student_code'];
} else {
    // Handle the case when the student ID is not provided in the URL
    echo "Student ID not provided.";
    exit; // Exit PHP execution
}

// Check if the form is submitted
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Retrieve the HTML content from the POST data
    $tableHtml = $_POST['tableHtml'];

    // Update the content column in the students table for the specified student_code
    $updateQuery = "UPDATE students SET content = ? WHERE student_code = ?";
    $stmt = $conn->prepare($updateQuery);
    $stmt->bind_param('ss', $tableHtml, $student_code);
    if ($stmt->execute()) {
        echo "Data saved successfully!";
    } else {
        echo "Failed to save data.";
    }
    exit; // Exit PHP execution after saving data
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<style>
    table, th, td {
        border: 1px solid black;
    }
    .curriculum {
        margin-left: 30px;
        margin-right: 30px;
    }

    body {
        margin: 0;
        padding: 0;
    }
    .firsttable {
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .text {
        display: flex;
        justify-content: center;
    }
    .buttons {
        position: fixed;
        bottom: 20px;
        left: 20px; /* Adjust left position as needed */
        text-align: left;
    }

    .buttons button {
        display: block;
        margin-bottom: 10px;
        background-color: transparent;
        border: 1px solid #77d491;
        border-radius: 5px;
        cursor: pointer;
        transition: background-color 0.3s ease;
        background-color: greenyellow;
        padding: 10px 15px;
    }

    .buttons button:hover {
        background-color: #77d491;
        color: white;
    }
    
   
    .highlight-green {
            background-color: lightgreen;
        }
        .highlight-red {
            background-color: salmon;
        }
</style>

<body class class="curriculum" id="editableTable">



    <div class="text" id="curriculumTitle"s>
        <h1>2021-2022</h1>
    </div>
    <div class="text">
        <h1>First Year</h1>
    </div>
<div class="tablewhole">
    <div class="firsttable">
        <table class="curriculum">
            <tr>
                <th colspan="7">First Semester</th>
            </tr>
            <tr>
                <th rowspan="2">Pen Code</th>
                <th rowspan="2">Descriptive Title</th>
                <th colspan="3">Units</th>
                <th rowspan="2">Pre-requisite</th>
                <th rowspan="2">Grade</th>
            </tr>
            <tr>
                <th>Lec</th>
                <th>Lab</th>
                <th>Total</th>
            </tr>
            <?php
    $i = 1;
    $qry = $conn->query("SELECT * FROM 11curriculum2022 ORDER BY 1pc ASC, 1dt ASC, 1lc ASC, 1lb ASC, 1to ASC, 1pr ASC, 1gr ASC");
    while ($row = $qry->fetch_assoc()):    

        // Retrieve the corresponding 1pc1 data from 11curriculum2023 for each row in 11curriculum2022
        $qry_1pc1 = $conn->query("SELECT 1pc1 FROM 11curriculum2023 WHERE 1pc1 = '{$row['1pc']}'");
        
        // Check if the query returned a result
        if ($qry_1pc1 && $row_1pc1 = $qry_1pc1->fetch_assoc()) {
            // Get the value for comparison
            $value_1pc = $row['1pc'];
            $value_1pc1 = $row_1pc1['1pc1'];

            // Check if the values are the same and apply highlighting
            $highlight_class = ($value_1pc == $value_1pc1) ? 'highlight-green' : 'highlight-red';
        } else {
            // If no matching record found, set default highlight class
            $highlight_class = 'highlight-red';
        }
?>
        <tr>
            <td><span class="<?php echo $highlight_class; ?>"><?php echo $row['1pc']; ?></span></td>
            <td><span class="hidden"><?php echo $row['1dt']; ?></span></td>
            <td><span class="hidden"><?php echo $row['1lc']; ?></span></td>
            <td><span class="hidden"><?php echo $row['1lb']; ?></span></td>
            <td><span class="hidden"><?php echo $row['1to']; ?></span></td>
            <td><span class="hidden"><?php echo $row['1pr']; ?></span></td>
            <td><span class="hidden"><?php echo $row['1gr']; ?></span></td>
        </tr>
<?php endwhile; ?>

        </table>
        <table class="curriculum">
            <tr>
                <th colspan="7">Second Semester</th>
            </tr>
            <tr>
                <th rowspan="2">Pen Code</th>
                <th rowspan="2">Descriptive Title</th>
                <th colspan="3">Units</th>
                <th rowspan="2">Pre-requisite</th>
                <th rowspan="2">Grade</th>
            </tr>
            <tr>
                <th>Lec</th>
                <th>Lab</th>
                <th>Total</th>
            </tr>
            <?php
    $i = 1;
    $qry = $conn->query("SELECT * FROM 12curriculum2022 ORDER BY 2pc ASC, 2dt ASC, 2lc ASC, 2lb ASC, 2to ASC, 2pr ASC, 2gr ASC");
    while ($row = $qry->fetch_assoc()):    

        // Retrieve the corresponding 1pc1 data from 11curriculum2023 for each row in 11curriculum2022
        $qry_2pc1 = $conn->query("SELECT 2pc1 FROM 12curriculum2023 WHERE 2pc1 = '{$row['2pc']}'");
        
        // Check if the query returned a result
        if ($qry_2pc1 && $row_2pc1 = $qry_2pc1->fetch_assoc()) {
            // Get the value for comparison
            $value_2pc = $row['2pc'];
            $value_2pc1 = $row_2pc1['2pc1'];

            // Check if the values are the same and apply highlighting
            $highlight_class = ($value_2pc == $value_2pc1) ? 'highlight-green' : 'highlight-red';
        } else {
            // If no matching record found, set default highlight class
            $highlight_class = 'highlight-red';
        }
?>
        <tr>
            <td><span class="<?php echo $highlight_class; ?>"><?php echo $row['2pc']; ?></span></td>
            <td><span class="hidden"><?php echo $row['2dt']; ?></span></td>
            <td><span class="hidden"><?php echo $row['2lc']; ?></span></td>
            <td><span class="hidden"><?php echo $row['2lb']; ?></span></td>
            <td><span class="hidden"><?php echo $row['2to']; ?></span></td>
            <td><span class="hidden"><?php echo $row['2pr']; ?></span></td>
            <td><span class="hidden"><?php echo $row['2gr']; ?></span></td>
        </tr>
<?php endwhile; ?>

        </table>
    </div>
    <div class="text">
        <h1>Second Year</h1>
    </div>

    <div class="firsttable">
        <table class="curriculum">
            <tr>
                <th colspan="7">First Semester</th>
            </tr>
            <tr>
                <th rowspan="2">Pen Code</th>
                <th rowspan="2">Descriptive Title</th>
                <th colspan="3">Units</th>
                <th rowspan="2">Pre-requisite</th>
                <th rowspan="2">Grade</th>
            </tr>
            <tr>
                <th>Lec</th>
                <th>Lab</th>
                <th>Total</th>
            </tr>
            <?php
    $i = 1;
    $qry = $conn->query("SELECT * FROM 21curriculum2022 ORDER BY 3pc ASC, 3dt ASC, 3lc ASC, 3lb ASC, 3to ASC, 3pr ASC, 3gr ASC");
    while ($row = $qry->fetch_assoc()):    

        // Retrieve the corresponding 1pc1 data from 11curriculum2023 for each row in 11curriculum2022
        $qry_3pc1 = $conn->query("SELECT 3pc1 FROM 21curriculum2023 WHERE 3pc1 = '{$row['3pc']}'");
        
        // Check if the query returned a result
        if ($qry_3pc1 && $row_3pc1 = $qry_3pc1->fetch_assoc()) {
            // Get the value for comparison
            $value_3pc = $row['3pc'];
            $value_3pc1 = $row_3pc1['3pc1'];

            // Check if the values are the same and apply highlighting
            $highlight_class = ($value_3pc == $value_3pc1) ? 'highlight-green' : 'highlight-red';
        } else {
            // If no matching record found, set default highlight class
            $highlight_class = 'highlight-red';
        }
?>
        <tr>
            <td><span class="<?php echo $highlight_class; ?>"><?php echo $row['3pc']; ?></span></td>
            <td><span class="hidden"><?php echo $row['3dt']; ?></span></td>
            <td><span class="hidden"><?php echo $row['3lc']; ?></span></td>
            <td><span class="hidden"><?php echo $row['3lb']; ?></span></td>
            <td><span class="hidden"><?php echo $row['3to']; ?></span></td>
            <td><span class="hidden"><?php echo $row['3pr']; ?></span></td>
            <td><span class="hidden"><?php echo $row['3gr']; ?></span></td>
        </tr>
<?php endwhile; ?>

        </table>
        <table class="curriculum">
            <tr>
                <th colspan="7">Second Semester</th>
            </tr>
            <tr>
                <th rowspan="2">Pen Code</th>
                <th rowspan="2">Descriptive Title</th>
                <th colspan="3">Units</th>
                <th rowspan="2">Pre-requisite</th>
                <th rowspan="2">Grade</th>
            </tr>
            <tr>
                <th>Lec</th>
                <th>Lab</th>
                <th>Total</th>
            </tr>
            <?php
    $i = 1;
    $qry = $conn->query("SELECT * FROM 22curriculum2022 ORDER BY 4pc ASC, 4dt ASC, 4lc ASC, 4lb ASC, 4to ASC, 4pr ASC, 4gr ASC");
    while ($row = $qry->fetch_assoc()):    

        // Retrieve the corresponding 1pc1 data from 11curriculum2023 for each row in 11curriculum2022
        $qry_4pc1 = $conn->query("SELECT 4pc1 FROM 22curriculum2023 WHERE 4pc1 = '{$row['4pc']}'");
        
        // Check if the query returned a result
        if ($qry_4pc1 && $row_4pc1 = $qry_4pc1->fetch_assoc()) {
            // Get the value for comparison
            $value_4pc = $row['4pc'];
            $value_4pc1 = $row_4pc1['4pc1'];

            // Check if the values are the same and apply highlighting
            $highlight_class = ($value_4pc == $value_4pc1) ? 'highlight-green' : 'highlight-red';
        } else {
            // If no matching record found, set default highlight class
            $highlight_class = 'highlight-red';
        }
?>
        <tr>
            <td><span class="<?php echo $highlight_class; ?>"><?php echo $row['4pc']; ?></span></td>
            <td><span class="hidden"><?php echo $row['4dt']; ?></span></td>
            <td><span class="hidden"><?php echo $row['4lc']; ?></span></td>
            <td><span class="hidden"><?php echo $row['4lb']; ?></span></td>
            <td><span class="hidden"><?php echo $row['4to']; ?></span></td>
            <td><span class="hidden"><?php echo $row['4pr']; ?></span></td>
            <td><span class="hidden"><?php echo $row['4gr']; ?></span></td>
        </tr>
<?php endwhile; ?>

        </table>
    </div>
    <div class="text">
        <h1>Third Year</h1>
    </div>

    <div class="firsttable">
        <table class="curriculum">
            <tr>
                <th colspan="7">First Semester</th>
            </tr>
            <tr>
                <th rowspan="2">Pen Code</th>
                <th rowspan="2">Descriptive Title</th>
                <th colspan="3">Units</th>
                <th rowspan="2">Pre-requisite</th>
                <th rowspan="2">Grade</th>
            </tr>
            <tr>
                <th>Lec</th>
                <th>Lab</th>
                <th>Total</th>
            </tr>
            <?php
    $i = 1;
    $qry = $conn->query("SELECT * FROM 31curriculum2022 ORDER BY 5pc ASC, 5dt ASC, 5lc ASC, 5lb ASC, 5to ASC, 5pr ASC, 5gr ASC");
    while ($row = $qry->fetch_assoc()):    

        // Retrieve the corresponding 1pc1 data from 11curriculum2023 for each row in 11curriculum2022
        $qry_5pc1 = $conn->query("SELECT 5pc1 FROM 31curriculum2023 WHERE 5pc1 = '{$row['5pc']}'");
        
        // Check if the query returned a result
        if ($qry_5pc1 && $row_5pc1 = $qry_5pc1->fetch_assoc()) {
            // Get the value for comparison
            $value_5pc = $row['5pc'];
            $value_5pc1 = $row_5pc1['5pc1'];

            // Check if the values are the same and apply highlighting
            $highlight_class = ($value_5pc == $value_5pc1) ? 'highlight-green' : 'highlight-red';
        } else {
            // If no matching record found, set default highlight class
            $highlight_class = 'highlight-red';
        }
?>
        <tr>
            <td><span class="<?php echo $highlight_class; ?>"><?php echo $row['5pc']; ?></span></td>
            <td><span class="hidden"><?php echo $row['5dt']; ?></span></td>
            <td><span class="hidden"><?php echo $row['5lc']; ?></span></td>
            <td><span class="hidden"><?php echo $row['5lb']; ?></span></td>
            <td><span class="hidden"><?php echo $row['5to']; ?></span></td>
            <td><span class="hidden"><?php echo $row['5pr']; ?></span></td>
            <td><span class="hidden"><?php echo $row['5gr']; ?></span></td>
        </tr>
<?php endwhile; ?>
        </table>
        <table class="curriculum">
            <tr>
                <th colspan="7">Second Semester</th>
            </tr>
            <tr>
                <th rowspan="2">Pen Code</th>
                <th rowspan="2">Descriptive Title</th>
                <th colspan="3">Units</th>
                <th rowspan="2">Pre-requisite</th>
                <th rowspan="2">Grade</th>
            </tr>
            <tr>
                <th>Lec</th>
                <th>Lab</th>
                <th>Total</th>
            </tr>
            <?php
    $i = 1;
    $qry = $conn->query("SELECT * FROM 32curriculum2022 ORDER BY 6pc ASC, 6dt ASC, 6lc ASC, 6lb ASC, 6to ASC, 6pr ASC, 6gr ASC");
    while ($row = $qry->fetch_assoc()):    

        // Retrieve the corresponding 1pc1 data from 11curriculum2023 for each row in 11curriculum2022
        $qry_6pc1 = $conn->query("SELECT 6pc1 FROM 32curriculum2023 WHERE 6pc1 = '{$row['6pc']}'");
        
        // Check if the query returned a result
        if ($qry_6pc1 && $row_6pc1 = $qry_6pc1->fetch_assoc()) {
            // Get the value for comparison
            $value_6pc = $row['6pc'];
            $value_6pc1 = $row_6pc1['6pc1'];

            // Check if the values are the same and apply highlighting
            $highlight_class = ($value_6pc == $value_6pc1) ? 'highlight-green' : 'highlight-red';
        } else {
            // If no matching record found, set default highlight class
            $highlight_class = 'highlight-red';
        }
?>
        <tr>
            <td><span class="<?php echo $highlight_class; ?>"><?php echo $row['6pc']; ?></span></td>
            <td><span class="hidden"><?php echo $row['6dt']; ?></span></td>
            <td><span class="hidden"><?php echo $row['6lc']; ?></span></td>
            <td><span class="hidden"><?php echo $row['6lb']; ?></span></td>
            <td><span class="hidden"><?php echo $row['6to']; ?></span></td>
            <td><span class="hidden"><?php echo $row['6pr']; ?></span></td>
            <td><span class="hidden"><?php echo $row['6gr']; ?></span></td>
        </tr>
<?php endwhile; ?>
        </table>
    </div>
    <div class="text">
        <h1>Fourth Year</h1>
    </div>

    <div class="firsttable">
        <table class="curriculum">
            <tr>
                <th colspan="7">First Semester</th>
            </tr>
            <tr>
                <th rowspan="2">Pen Code</th>
                <th rowspan="2">Descriptive Title</th>
                <th colspan="3">Units</th>
                <th rowspan="2">Pre-requisite</th>
                <th rowspan="2">Grade</th>
            </tr>
            <tr>
                <th>Lec</th>
                <th>Lab</th>
                <th>Total</th>
            </tr>
            <?php
    $i = 1;
    $qry = $conn->query("SELECT * FROM 41curriculum2022 ORDER BY 7pc ASC, 7dt ASC, 7lc ASC, 7lb ASC, 7to ASC, 7pr ASC, 7gr ASC");
    while ($row = $qry->fetch_assoc()):    

        // Retrieve the corresponding 1pc1 data from 11curriculum2023 for each row in 11curriculum2022
        $qry_7pc1 = $conn->query("SELECT 7pc1 FROM 41curriculum2023 WHERE 7pc1 = '{$row['7pc']}'");
        
        // Check if the query returned a result
        if ($qry_7pc1 && $row_7pc1 = $qry_7pc1->fetch_assoc()) {
            // Get the value for comparison
            $value_7pc = $row['7pc'];
            $value_7pc1 = $row_7pc1['7pc1'];

            // Check if the values are the same and apply highlighting
            $highlight_class = ($value_7pc == $value_7pc1) ? 'highlight-green' : 'highlight-red';
        } else {
            // If no matching record found, set default highlight class
            $highlight_class = 'highlight-red';
        }
?>
        <tr>
            <td><span class="<?php echo $highlight_class; ?>"><?php echo $row['7pc']; ?></span></td>
            <td><span class="hidden"><?php echo $row['7dt']; ?></span></td>
            <td><span class="hidden"><?php echo $row['7lc']; ?></span></td>
            <td><span class="hidden"><?php echo $row['7lb']; ?></span></td>
            <td><span class="hidden"><?php echo $row['7to']; ?></span></td>
            <td><span class="hidden"><?php echo $row['7pr']; ?></span></td>
            <td><span class="hidden"><?php echo $row['7gr']; ?></span></td>
        </tr>
<?php endwhile; ?>
        </table>
        <table class="curriculum">
            <tr>
                <th colspan="7">Second Semester</th>
            </tr>
            <tr>
                <th rowspan="2">Pen Code</th>
                <th rowspan="2">Descriptive Title</th>
                <th colspan="3">Units</th>
                <th rowspan="2">Pre-requisite</th>
                <th rowspan="2">Grade</th>
            </tr>
            <tr>
                <th>Lec</th>
                <th>Lab</th>
                <th>Total</th>
            </tr>
            <?php
    $i = 1;
    $qry = $conn->query("SELECT * FROM 42curriculum2022 ORDER BY 8pc ASC, 8dt ASC, 8lc ASC, 8lb ASC, 8to ASC, 8pr ASC, 8gr ASC");
    while ($row = $qry->fetch_assoc()):    

        // Retrieve the corresponding 8pc1 data from 22curriculum2023 for each row in 22curriculum2022
        $qry_8pc1 = $conn->query("SELECT 8pc1 FROM 42curriculum2023 WHERE 8pc1 = '{$row['8pc']}'");
        
        // Check if the query returned a result
        if ($qry_8pc1 && $row_8pc1 = $qry_8pc1->fetch_assoc()) {
            // Get the value for comparison
            $value_8pc = $row['8pc'];
            $value_8pc1 = $row_8pc1['8pc1'];

            // Check if the values are the same and apply highlighting
            $highlight_class = ($value_8pc == $value_8pc1) ? 'highlight-green' : 'highlight-red';
        } else {
            // If no matching record found, set default highlight class
            $highlight_class = 'highlight-red';
        }
?>
        <tr>
            <td><span class="<?php echo $highlight_class; ?>"><?php echo $row['8pc']; ?></span></td>
            <td><span class="hidden"><?php echo $row['8dt']; ?></span></td>
            <td><span class="hidden"><?php echo $row['8lc']; ?></span></td>
            <td><span class="hidden"><?php echo $row['8lb']; ?></span></td>
            <td><span class="hidden"><?php echo $row['8to']; ?></span></td>
            <td><span class="hidden"><?php echo $row['8pr']; ?></span></td>
            <td><span class="hidden"><?php echo $row['8gr']; ?></span></td>
        </tr>
<?php endwhile; ?>

        </table>
    </div>
</div>
<div class="buttons">
        <button onclick="makeTableEditable()">Edit</button>
        <button onclick="saveData('<?php echo $student_code; ?>')">Save</button>
        <button onclick="printPage()">Print</button>
        <a href="./mainpage.php?page=results" class="nav-link nav-results nav-new_result nav-edit_result"><button>Back</button></a>
    </div>
</body>
<script>
    function hideHintText(input) {
        input.classList.remove('hint-text');
    }

    let setsCreated = 0;

function makeTableEditable() {
    const table = document.getElementById('editableTable');
    const cells = table.querySelectorAll('td');
    cells.forEach(cell => {
        cell.contentEditable = true;
    });
}
function synchronizeColumnWidths() {
// Get all tables with class 'curriculum'
const tables = document.querySelectorAll('.curriculum');

// Initialize an array to store the maximum width of each column
const maxWidths = Array.from({ length: tables[0].rows[0].cells.length }, () => 0);

// Loop through each table to find the maximum width of each column
tables.forEach(table => {
    table.rows.forEach(row => {
        row.cells.forEach((cell, index) => {
            maxWidths[index] = Math.max(maxWidths[index], cell.offsetWidth);
        });
    });
});

// Set the width of each cell in each table to the maximum width of the corresponding column
tables.forEach(table => {
    table.rows.forEach(row => {
        row.cells.forEach((cell, index) => {
            cell.style.width = maxWidths[index] + 'px';
        });
    });
});
}

function saveData(student_code) {
    // Exclude the script and buttons before gathering the HTML content
    const tableHtml = document.getElementById('editableTable').cloneNode(true);
    tableHtml.querySelector('.buttons').remove(); // Remove the buttons div

    // Remove highlighting classes from all cells
    const cells = tableHtml.querySelectorAll('td');
    cells.forEach(cell => {
        cell.classList.remove('highlight-green');
        cell.classList.remove('highlight-red');
    });

    // Remove specific words from the HTML content
    let htmlContent = tableHtml.outerHTML;
    htmlContent = htmlContent.replace(/highlight-red|highlight-green/g, '');

    // Include styles in the HTML content
    const styles = document.querySelectorAll('style');
    styles.forEach(style => {
        htmlContent += style.outerHTML; // Append style tags to the HTML content
    });

    // Prepare the data to be sent
    const formData = new FormData();
    formData.append('student_code', student_code);
    formData.append('tableHtml', htmlContent); // Use modified HTML content

    // Send the data using AJAX
    const xhr = new XMLHttpRequest();
    xhr.open('POST', 'save_data.php', true);
    xhr.onload = function () {
        if (xhr.status === 200) {
            alert('Data saved successfully!');
        } else {
            alert('Failed to save data.');
        }
    };
    xhr.send(formData);
}


function printPage() {
            window.print();
        }

</script>
</html>

