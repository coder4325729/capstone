<?php
// Include your database connection file
include 'db_connect.php';

// Check if the form is submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Retrieve the StudentCurriculumID to determine which rows to delete
    $StudentCurriculumSubjectID = $_POST['StudentCurriculumSubjectID'];

    // Perform SQL query to delete data from the studentcurriculumsubject table
    $query = "DELETE FROM studentcurriculumsubject WHERE StudentCurriculumSubjectID = '$StudentCurriculumSubjectID'";
    $result = $conn->query($query);

    // Check if the query was successful
    if ($result) {
        echo "Data deleted successfully!";
    } else {
        echo "Error: " . $conn->error;
    }

    // Close database connection
    $conn->close();
}
?>
