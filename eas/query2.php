<?php
function select_StudentCurriculumSubject($schoolyear, $semester, $StudentID) {
    $qry = "SELECT "
         . "A.PenCode AS Pencode, "
         . "A.Description AS Description, "
         . "A.Lab AS Lab, "
         . "A.Lec AS Lec, "
         . "A.SubjectID AS subjectID, "
         . "A.Prerequisite AS Prerequisite, "
         . "studentcurriculumsubject.Grade AS Grade, "
         . "studentcurriculumsubject.Status AS Status, "
         . "studentcurriculumsubject.StudentCurriculumSubjectID AS StudentCurriculumSubjectID, "
         . "B.Pencode AS Postrequisite "
         . "FROM subjects A "         
         . "JOIN studentcurriculumsubject ON A.SubjectID = studentcurriculumsubject.SubjectID "
         . "JOIN curriculumsubject ON curriculumsubject.SubjectID = studentcurriculumsubject.SubjectID "
         . "JOIN curriculum ON curriculumsubject.CurriculumID = curriculum.CurriculumID "
         . "LEFT OUTER JOIN subjects B ON A.Pencode = B.Prerequisite "
         . "WHERE studentcurriculumsubject.Semester = '$semester' "
         . "AND studentcurriculumsubject.StudentID = '$StudentID' ";
         
       
    return $qry;
}

function console_log($output, $with_script_tags = true) {
    $js_code = 'console.log(' . json_encode($output, JSON_HEX_TAG) . ');';
    if ($with_script_tags) {
        $js_code = '<script>' . $js_code . '</script>';
    }
    echo $js_code;
}
?>


