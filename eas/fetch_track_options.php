<?php
// Include database connection file
include 'db_connect.php';

// Check if curriculumYear is set and not empty
if (isset($_POST['curriculumYear']) && !empty($_POST['curriculumYear'])) {
    // Sanitize the input to prevent SQL injection
    $curriculumYear = mysqli_real_escape_string($conn, $_POST['curriculumYear']);

    // Query to fetch distinct descriptive values based on the selected curriculum year
    $query = "SELECT DISTINCT descriptive FROM curriculum WHERE syear = '$curriculumYear' ORDER BY descriptive ASC";
    
    // Execute the query
    $result = $conn->query($query);

    // Check if there are any results
    if ($result->num_rows > 0) {
        // Start building the HTML options
        $options = '<option value="">Select Track</option>';
        
        // Fetch associative array of rows
        while ($row = $result->fetch_assoc()) {
            // Append each descriptive value as an option
            $options .= '<option value="' . $row['descriptive'] . '">' . $row['descriptive'] . '</option>';
        }
        $options .= '<option value="none">NONE</option>';
        // Echo the HTML options back to the main script
        echo $options;
    } else {
        // No tracks found for the selected year
        echo '<option value="">No Tracks Available</option>';
    }
} else {
    // Invalid or empty curriculum year parameter
    echo '<option value="">Invalid Curriculum Year</option>';
}
?>
