<?php
include 'db_connect.php';
if(isset($_GET['id'])){
	$qry = $conn->query("SELECT * FROM curriculum where id={$_GET['id']}")->fetch_array();
	foreach($qry as $k => $v){
		$$k = $v;
	}
}
?>
<div class="container-fluid">
	<form action="" id="addcurriculum">
		<input type="hidden" name="id" value="<?php echo isset($id) ? $id : '' ?>">
		<div id="msg" class="form-group"></div>
        <div class="form-group">
			<label for="year" class="control-label">Curriculum Year</label>
			<input type="text" class="form-control form-control-sm" name="semester" id="semester" value="<?php echo isset($syear) ? $syear : '' ?>">
		</div>
		<div class="form-group">
			<label for="semester" class="control-label">Semester</label>
			<input type="text" class="form-control form-control-sm" name="semester" id="semester" value="<?php echo isset($semester) ? $semester : '' ?>">
		</div>
		<div class="form-group">
			<label for="pencode" class="control-label">Pencode</label>
			<input type="text" class="form-control form-control-sm" name="pencode" id="pencode" value="<?php echo isset($pencode) ? $pencode : '' ?>">
		</div>
        <div class="form-group">
			<label for="descriptive" class="control-label">Descriptive Title</label>
			<input type="text" class="form-control form-control-sm" name="descriptive" id="descriptive" value="<?php echo isset($descriptive) ? $descriptive : '' ?>">
		</div>
		<div class="form-group">
			<label for="lec" class="control-label">lec</label>
			<input type="text" class="form-control form-control-sm" name="lec" id="lec" value="<?php echo isset($lec) ? $lec : '' ?>">
            <div class="form-group">
			<label for="level" class="control-label">lab</label>
			<input type="text" class="form-control form-control-sm" name="lab" id="lab" value="<?php echo isset($lab) ? $lab : '' ?>">
		</div>
		<div class="form-group">
			<label for="prereq" class="control-label">prereq</label>
			<input type="text" class="form-control form-control-sm" name="prereq" id="prereq" value="<?php echo isset($prereq) ? $prereq : '' ?>">
		</div>
        <div class="form-group">
			<label for="grade" class="control-label">grade</label>
			<input type="text" class="form-control form-control-sm" name="grade" id="grade" value="<?php echo isset($grade) ? $grade : '' ?>">
		</div>
	</form>
</div>
<script>
	$(document).ready(function(){
		$('#addcurriculum').submit(function(e){
			e.preventDefault();
			start_load()
			$('#msg').html('')
			$.ajax({
				url:'ajax.php?action=save_curriculum',
				method:'POST',
				data:$(this).serialize(),
				success:function(resp){
					if(resp == 1){
						alert_toast("Data successfully saved.","success");
						setTimeout(function(){
							location.reload()	
						},1750)
					}else if(resp == 2){
						$('#msg').html('<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Class already exist.</div>')
						end_load()
					}
				}
			})
		})
	})

</script>