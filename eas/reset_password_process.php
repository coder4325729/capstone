<?php
// Include necessary files and start session if required
include('./db_connect.php');
session_start();

if($_SERVER["REQUEST_METHOD"] == "POST") {
    // Retrieve form data
    $email = $_POST['email'];
    $token = $_POST['token'];
    $password = $_POST['password'];
    $confirm_password = $_POST['confirm_password'];

    // Validate form data
    if(empty($password) || empty($confirm_password)) {
        // Password fields are empty
        $_SESSION['reset_password_error'] = "Please enter a new password.";
        header("Location: reset_password.php?token=$token&email=$email");
        exit();
    } elseif($password !== $confirm_password) {
        // Passwords do not match
        $_SESSION['reset_password_error'] = "Passwords do not match.";
        header("Location: reset_password.php?token=$token&email=$email");
        exit();
    } else {
        // Hash the new password
        $hashed_password = password_hash($password, PASSWORD_DEFAULT);
        
        // Update the user's password in the database
        $query = "UPDATE users SET password = ? WHERE email = ?";
        $stmt = $conn->prepare($query);
        $stmt->bind_param("ss", $hashed_password, $email);
        $stmt->execute();

        // Delete the reset token from the database
        $delete_query = "DELETE FROM password_reset WHERE email = ?";
        $delete_stmt = $conn->prepare($delete_query);
        $delete_stmt->bind_param("s", $email);
        $delete_stmt->execute();

        // Redirect the user to the login page with a success message
        $_SESSION['reset_password_success'] = "Your password has been reset successfully. You can now login with your new password.";
        header("Location: login.php");
        exit();
    }
} else {
    // If the form is not submitted via POST method, redirect to the home page or appropriate page
    header("Location: index.php");
    exit();
}
?>
