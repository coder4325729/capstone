<?php
// update_student.php

// Include your database connection file
include 'db_connect.php';

// Check if student ID is provided
if (isset($_POST['student_id'])) {
    $studentId = $_POST['student_id'];

    // Check if admission status is being updated
    if (isset($_POST['admission_status'])) {
        $admissionStatus = $_POST['admission_status'];
  
        // Update the admission status in the database
        $updateQuery = "UPDATE students SET Status = '$admissionStatus' WHERE id = '$studentId'";
        echo $updateQuery;
        if ($conn->query($updateQuery) === TRUE) {
            echo "Admission status updated successfully";
        } else {
            echo "Error updating admission status: " . $conn->error;
        }
    }

    // Check if year level is being updated
    if (isset($_POST['year_level'])) {
        $yearLevel = $_POST['year_level'];
         
        // Update the year level in the database
        $updateQuery = "UPDATE students SET Standing = '$yearLevel' WHERE id = '$studentId'";
        echo $updateQuery;
        if ($conn->query($updateQuery) === TRUE) {
            echo "Year level updated successfully";
        } else {
            echo "Error updating year level: " . $conn->error;
        }
    }
} else {
    echo "Student ID not provided";
}
?>