<?php
// Assuming you have a database connection established
include 'db_connect.php';
// Check if the request method is POST
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Check if student_id and prerequisite are set in the POST data
    if (isset($_POST["student_id"]) && isset($_POST["prerequisite"])) {
        // Sanitize the inputs
        $studentId = $_POST["student_id"];
        $prerequisite = $_POST["prerequisite"];

        // Prepare and execute the SQL query to update the status
        $query = "UPDATE studentcurriculumsubject scs
                  INNER JOIN subjects s ON scs.SubjectID = s.SubjectID
                  SET scs.Status = 1 
                  WHERE scs.StudentID = ? AND s.Prerequisite = ?";
        $statement = $conn->prepare($query);
        $statement->bind_param("ss", $studentId, $prerequisite);
        
        if ($statement->execute()) {
            // Return a success message
            echo "Pencode status updated successfully!";
        } else {
            // Return an error message
            echo "Error updating pencode status: " . $statement->error;
        }

        // Close the statement and connection
        $statement->close();
        $conn->close();
    } else {
        // Return an error message if student_id or prerequisite are not set
        echo "Error: Missing student ID or prerequisite.";
    }
} else {
    // Return an error message if the request method is not POST
    echo "Error: Invalid request method.";
}
?>
