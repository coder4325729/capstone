<?php
session_start();
ini_set('display_errors', 1);
Class Action {
	private $db;

	public function __construct() {
		ob_start();
   	include 'db_connect.php';
	include 'history.php';
	include 'query.php';
    $this->db = $conn;
	}
	function __destruct() {
	    $this->db->close();
	    ob_end_flush();
	}

	function login(){
		extract($_POST);
		$qry = $this->db->query("SELECT *, id, concat(firstname,' ',lastname) as name FROM users where username = '".$username."' and password = '".md5($password)."' and type= 1 ");
		if($qry->num_rows > 0){
			foreach ($qry->fetch_array() as $key => $value) {
				if($key != 'password' && !is_numeric($key))
					$_SESSION['login_'.$key] = $value;
			}
			// Get user ID and call get_history
			$user_id = $_SESSION['login_id']; // Assuming 'id' is the user ID field
			get_history($this->db, $user_id);
	
			return 1;
		}else{
			return 2;
		}
	}
	function logout(){
		session_destroy();
		foreach ($_SESSION as $key => $value) {
			unset($_SESSION[$key]);
		}
		header("location:login.php");
	}
	function login2(){
		extract($_POST);
			$qry = $this->db->query("SELECT *,concat(lastname,', ',firstname,' ',middlename) as name FROM students where student_code = '".$student_code."' ");
		if($qry->num_rows > 0){
			foreach ($qry->fetch_array() as $key => $value) {
				if($key != 'password' && !is_numeric($key))
					$_SESSION['rs_'.$key] = $value;
			}
				return 1;
		}else{
			return 3;
		}
	}
	function save_user(){
		extract($_POST);
		$data = "";
		foreach($_POST as $k => $v){
			if(!in_array($k, array('id','cpass','password')) && !is_numeric($k)){
				if(empty($data)){
					$data .= " $k='$v' ";
				}else{
					$data .= ", $k='$v' ";
				}
			}
		}
		if(!empty($cpass) && !empty($password)){
					$data .= ", password=md5('$password') ";

		}
		$check = $this->db->query("SELECT * FROM users where email ='$email' ".(!empty($id) ? " and id != {$id} " : ''))->num_rows;
		if($check > 0){
			return 2;
			exit;
		}
		if(isset($_FILES['img']) && $_FILES['img']['tmp_name'] != ''){
			$fname = strtotime(date('y-m-d H:i')).'_'.$_FILES['img']['name'];
			$move = move_uploaded_file($_FILES['img']['tmp_name'],'../assets/uploads/'. $fname);
			$data .= ", avatar = '$fname' ";

		}
		if(empty($id)){
			$save = $this->db->query("INSERT INTO users set $data");
		}else{
			$save = $this->db->query("UPDATE users set $data where id = $id");
		}

		if($save){
			return 1;
		}
	}
	function signup(){
		extract($_POST);
		$data = "";
		foreach($_POST as $k => $v){
			if(!in_array($k, array('id','cpass')) && !is_numeric($k)){
				if($k =='password'){
					if(empty($v))
						continue;
					$v = md5($v);

				}
				if(empty($data)){
					$data .= " $k='$v' ";
				}else{
					$data .= ", $k='$v' ";
				}
			}
		}

		$check = $this->db->query("SELECT * FROM users where email ='$email' ".(!empty($id) ? " and id != {$id} " : ''))->num_rows;
		if($check > 0){
			return 2;
			exit;
		}
		if(isset($_FILES['img']) && $_FILES['img']['tmp_name'] != ''){
			$fname = strtotime(date('y-m-d H:i')).'_'.$_FILES['img']['name'];
			$move = move_uploaded_file($_FILES['img']['tmp_name'],'../assets/uploads/'. $fname);
			$data .= ", avatar = '$fname' ";

		}
		if(empty($id)){
			$save = $this->db->query("INSERT INTO users set $data");

		}else{
			$save = $this->db->query("UPDATE users set $data where id = $id");
		}

		if($save){
			if(empty($id))
				$id = $this->db->insert_id;
			foreach ($_POST as $key => $value) {
				if(!in_array($key, array('id','cpass','password')) && !is_numeric($key))
					$_SESSION['login_'.$key] = $value;
			}
					$_SESSION['login_id'] = $id;
			return 1;
		}
	}

	function update_user(){
		extract($_POST);
		$data = "";
		foreach($_POST as $k => $v){
			if(!in_array($k, array('id','cpass','table')) && !is_numeric($k)){
				if($k =='password')
					$v = md5($v);
				if(empty($data)){
					$data .= " $k='$v' ";
				}else{
					$data .= ", $k='$v' ";
				}
			}
		}
		if($_FILES['img']['tmp_name'] != ''){
			$fname = strtotime(date('y-m-d H:i')).'_'.$_FILES['img']['name'];
			$move = move_uploaded_file($_FILES['img']['tmp_name'],'assets/uploads/'. $fname);
			$data .= ", avatar = '$fname' ";

		}
		$check = $this->db->query("SELECT * FROM users where email ='$email' ".(!empty($id) ? " and id != {$id} " : ''))->num_rows;
		if($check > 0){
			return 2;
			exit;
		}
		if(empty($id)){
			$save = $this->db->query("INSERT INTO users set $data");
		}else{
			$save = $this->db->query("UPDATE users set $data where id = $id");
		}

		if($save){
			foreach ($_POST as $key => $value) {
				if($key != 'password' && !is_numeric($key))
					$_SESSION['login_'.$key] = $value;
			}
			if($_FILES['img']['tmp_name'] != '')
			$_SESSION['login_avatar'] = $fname;
			return 1;
		}
	}
	function delete_user(){
		extract($_POST);
		$delete = $this->db->query("DELETE FROM users where id = ".$id);
		if($delete)
			return 1;
	}
	function save_system_settings(){
		extract($_POST);
		$data = '';
		foreach($_POST as $k => $v){
			if(!is_numeric($k)){
				if(empty($data)){
					$data .= " $k='$v' ";
				}else{
					$data .= ", $k='$v' ";
				}
			}
		}
		if($_FILES['cover']['tmp_name'] != ''){
			$fname = strtotime(date('y-m-d H:i')).'_'.$_FILES['cover']['name'];
			$move = move_uploaded_file($_FILES['cover']['tmp_name'],'../assets/uploads/'. $fname);
			$data .= ", cover_img = '$fname' ";

		}
		$chk = $this->db->query("SELECT * FROM system_settings");
		if($chk->num_rows > 0){
			$save = $this->db->query("UPDATE system_settings set $data where id =".$chk->fetch_array()['id']);
		}else{
			$save = $this->db->query("INSERT INTO system_settings set $data");
		}
		if($save){
			foreach($_POST as $k => $v){
				if(!is_numeric($k)){
					$_SESSION['system'][$k] = $v;
				}
			}
			if($_FILES['cover']['tmp_name'] != ''){
				$_SESSION['system']['cover_img'] = $fname;
			}
			return 1;
		}
	}
	function save_image(){
		extract($_FILES['file']);
		if(!empty($tmp_name)){
			$fname = strtotime(date("Y-m-d H:i"))."_".(str_replace(" ","-",$name));
			$move = move_uploaded_file($tmp_name,'../assets/uploads/'. $fname);
			$protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https'?'https':'http';
			$hostName = $_SERVER['HTTP_HOST'];
			$path =explode('/',$_SERVER['PHP_SELF']);
			$currentPath = '/'.$path[1]; 
			if($move){
				return $protocol.'://'.$hostName.$currentPath.'/assets/uploads/'.$fname;
			}
		}
	}
	function save_class(){
		extract($_POST);
		$data = "";
		foreach($_POST as $k => $v){
			if(!in_array($k, array('id')) && !is_numeric($k)){
				if(empty($data)){
					$data .= " $k='$v' ";
				}else{
					$data .= ", $k='$v' ";
				}
			}
		}
		$chk = $this->db->query("SELECT * FROM classes where level ='$level' and section = '$section' and id != '$id' ");
		if($chk->num_rows > 0){
			return 2;
			exit;
		}
		if(empty($id)){
			$save = $this->db->query("INSERT INTO classes set $data");
		}else{
			$save = $this->db->query("UPDATE classes set $data where id = $id");
		}
		if($save){
			return 1;
		}
	}
	function delete_class(){
		extract($_POST);
		$delete = $this->db->query("DELETE FROM classes where id = $id");
		if($delete){
			return 1;
		}
	}
	
	function save_curriculum(){
		extract($_POST);
		$data = "";
		foreach($_POST as $k => $v){
			if($k !== 'CurriculumID' && !is_numeric($k)){
				if(empty($data)){
					$data .= "$k='$v'";
				}else{
					$data .= ", $k='$v'";
				}
			}
		}
		if(isset($CurriculumID)){
			$chk = $this->db->query("SELECT * FROM curriculum WHERE CurriculumID = '$CurriculumID' ");
		}else{
			$chk = $this->db->query("SELECT * FROM curriculum");
		}
		if($chk->num_rows > 0){
			return 2;
		}
	
		if(isset($CurriculumID)){
			$save = $this->db->query("UPDATE curriculum SET $data WHERE CurriculumID = $CurriculumID");
		}else{
			$save = $this->db->query("INSERT INTO curriculum SET $data");
		}
		if($save){
			return 1;
		}
	}
	function delete_curriculum(){
		extract($_POST);
		$delete = $this->db->query("DELETE FROM curriculum where CurriculumID = $CurriculumID");
		if($delete){
			return 1;
		}
	}
	function save_subject(){
		extract($_POST);
		$data = "";
		foreach($_POST as $k => $v){
			if(!in_array($k, array('SubjectID')) && !is_numeric($k)){
				if(empty($data)){
					$data .= " $k='$v' ";
				}else{
					$data .= ", $k='$v' ";
				}
			}
		}
		$chk = $this->db->query("SELECT * FROM subjects where Pencode ='$Pencode' and SubjectID != '$SubjectID' ");
		if($chk->num_rows > 0){
			return 2;
			exit;
		}
		if(empty($SubjectID)){
			$save = $this->db->query("INSERT INTO subjects set $data");
		}else{
			$save = $this->db->query("UPDATE subjects set $data where SubjectID = $SubjectID");
		}
		if($save){
			return 1;
		}
	}
	function delete_subject(){
		extract($_POST);
		$delete = $this->db->query("DELETE FROM subjects where SubjectID = $SubjectID");
		if($delete){
			return 1;
		}
	}
	public function get_curriculumsubject($syear, $StudentID) {
        $qry = "SELECT "
             . "subjects.SubjectID AS SubjectID, "
             . "subjects.Description AS Description, "
             . "subjects.Prerequisite AS Prerequisite, "
             . "subjects.Pencode AS Pencode, "
             . "curriculum.syear AS syear, "
             . "curriculum.semester AS Semester "
             . "FROM subjects "
             . "JOIN curriculumsubject ON subjects.SubjectID = curriculumsubject.SubjectID "
             . "JOIN curriculum ON curriculumsubject.CurriculumID = curriculum.CurriculumID "
             . "WHERE curriculum.syear = '$syear' "
             . "ORDER BY curriculumsubject.CurriculumSubjectID ASC";

        $rqry = "";
        $result = $this->db->query($qry);

        $electives = $this->get_specialization($StudentID, $syear);

        $i = 0;
        $j = 0;
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $rqry .= ($i == 0 ? "" : ",");
                $rqry .= "(" . $StudentID . ",";
                if (str_contains(strtolower($row['Description']), "elective")) {
                    if (!empty($electives) && isset($electives[$j])) {
                        $rqry .= $electives[$j]['SubjectID'] . ",";
                        $rqry .= $row['syear'] . ",";
                        $rqry .= $row['Semester'] . ",";
                        $rqry .= (strtolower($electives[$j]['Prerequisite']) == "none" ? 1 : 1) . ")";
                        $j++;
                    } else {
                        $rqry .= $row['SubjectID'] . ",";
                        $rqry .= $row['syear'] . ",";
                        $rqry .= $row['Semester'] . ",";
                        $rqry .= (strtolower($row['Prerequisite']) == "none" ? 0 : 1) . ")";
                    }
                } else {
                    $rqry .= $row['SubjectID'] . ",";
                    $rqry .= $row['syear'] . ",";
                    $rqry .= $row['Semester'] . ",";
                    $rqry .= (strtolower($row['Prerequisite']) == "none" ? 0 : 1) . ")";
                }
                $i++;
            }
        }
        return $rqry;
    }

    public function get_specialization($StudentID, $syear) {
        $track = [];
        $qry = "SELECT * "
             . "FROM subjects "
             . "JOIN curriculumsubject ON curriculumsubject.SubjectID = subjects.SubjectID "
             . "JOIN curriculum ON curriculum.CurriculumID = curriculumsubject.CurriculumID "
             . "JOIN students ON students.track = curriculum.descriptive "
             . "WHERE students.id = $StudentID AND curriculum.syear = $syear "
             . "ORDER BY curriculumsubject.CurriculumSubjectID ASC";

        $result = $this->db->query($qry);

        while ($row = $result->fetch_assoc()) {
            $track[] = $row;
        }

        return $track;
    }

    public function insert_subjecttostudent($syear, $StudentIDs) {
        if (!is_array($StudentIDs)) {
            $StudentIDs = [$StudentIDs]; // Ensure $StudentIDs is an array
        }

        foreach ($StudentIDs as $StudentID) {
            // Insert subjects for the student from the new curriculum
            $this->remove_old_syear_data($syear, $StudentID); // Adjusted to only remove non-matching subjects

            $qry = "INSERT INTO studentcurriculumsubject (StudentID, SubjectID, syear, Semester, Status) VALUES "
                 . $this->get_curriculumsubject($syear, $StudentID);
            $result = $this->db->query($qry);

            if ($result) {
                echo "Insertion successful for StudentID: $StudentID"; // Individual success message

                // Transfer grades and status
                $this->transfer_grade_and_status($syear, $StudentID);

                // Remove old subjects after transferring
                $this->remove_old_subjects($syear, $StudentID);
            } else {
                echo "Error during insertion for StudentID: $StudentID - " . $this->db->error; // Individual error message
            }
        }

        echo "Bulk insertion completed."; // Overall completion message
    }

    public function transfer_grade_and_status($syear, $StudentID) {
        // Fetch old subjects with grades and statuses
        $qry_old = "SELECT scs.SubjectID, scs.Grade, scs.Status, s.Pencode "
                 . "FROM studentcurriculumsubject scs "
                 . "JOIN subjects s ON scs.SubjectID = s.SubjectID "
                 . "WHERE scs.StudentID = '$StudentID' AND scs.syear <> '$syear' AND s.Pencode IS NOT NULL";

        $result_old = $this->db->query($qry_old);

        // Update new subjects with matching pencode
        if ($result_old->num_rows > 0) {
            while ($row_old = $result_old->fetch_assoc()) {
                $pencode = $row_old['Pencode'];
                $grade = $row_old['Grade'];
                $status = $row_old['Status'];

                // Transfer grade if it's non-zero, otherwise just transfer the status
                $qry_update = "UPDATE studentcurriculumsubject scs "
                            . "JOIN subjects s ON scs.SubjectID = s.SubjectID "
                            . "SET scs.Grade = IF('$grade' != 0, '$grade', scs.Grade), scs.Status = '$status' "
                            . "WHERE scs.StudentID = '$StudentID' AND scs.syear = '$syear' AND s.Pencode = '$pencode'";

                $this->db->query($qry_update);
            }
        }
    }

    public function remove_old_syear_data($syear, $StudentID) {
        // Remove subjects from previous syear that do not match the new curriculum
        $qry_delete = "DELETE FROM studentcurriculumsubject WHERE StudentID = '$StudentID' AND SubjectID NOT IN (
            SELECT subjects.SubjectID
            FROM subjects
            JOIN curriculumsubject ON curriculumsubject.SubjectID = subjects.SubjectID
            JOIN curriculum ON curriculum.CurriculumID = curriculumsubject.CurriculumID
            WHERE curriculum.syear = '$syear'
        )";
        $this->db->query($qry_delete);

        // Get the track of the student
        $qry_track = "SELECT track FROM students WHERE id = '$StudentID'";
        $result_track = $this->db->query($qry_track);
        $row_track = $result_track->fetch_assoc();
        $track = $row_track['track'];

        // If the track is "none", do not replace the electives
        if (strtolower($track) !== "none") {
            // Replace IT Elective subjects with new subjects from the track
            $electives = $this->get_specialization($StudentID, $syear);

            // Sort electives by description to ensure correct order (e.g., IT Elective 1, 2, 3, 4)
            usort($electives, function($a, $b) {
                return strcmp($a['Description'], $b['Description']);
            });

            $j = 0;

            foreach ($electives as $elective) {
                $qry_replace_electives = "UPDATE studentcurriculumsubject scs
                                          JOIN subjects s ON scs.SubjectID = s.SubjectID
                                          JOIN curriculumsubject cs ON cs.SubjectID = s.SubjectID
                                          JOIN curriculum c ON c.CurriculumID = cs.CurriculumID
                                          SET scs.SubjectID = " . $elective['SubjectID'] . "
                                          WHERE scs.StudentID = '$StudentID'
                                          AND c.syear = '$syear'
                                          AND LOWER(s.Description) LIKE '%elective%'
                                          AND s.Description LIKE 'IT Elective " . ($j + 1) . "'";
                $this->db->query($qry_replace_electives);
                $j++;
            }
        }

        // Update the semester for subjects that belong to the new syear
        $qry_update_semester = "UPDATE studentcurriculumsubject scs
                                JOIN subjects s ON scs.SubjectID = s.SubjectID
                                JOIN curriculumsubject cs ON cs.SubjectID = s.SubjectID
                                JOIN curriculum c ON c.CurriculumID = cs.CurriculumID
                                SET scs.Semester = c.semester
                                WHERE scs.StudentID = '$StudentID'
                                AND c.syear = '$syear'";
        $this->db->query($qry_update_semester);
    }

    public function remove_old_subjects($syear, $StudentID) {
        $qry_delete = "DELETE FROM studentcurriculumsubject WHERE StudentID = '$StudentID' AND syear <> '$syear'";
        $this->db->query($qry_delete);
    }
	
	



	
	



function save_student(){
	extract($_POST);
	$data = "";
	$syear = 2024;
	foreach($_POST as $k => $v){
		if(!in_array($k, array('id','areas_id')) && !is_numeric($k)){
			
			if($k == 'description')
				$v = htmlentities(str_replace("'","&#x2019;",$v));
			if($k  == 'year'){
				$syear = $v;
				echo $v;
			}
			if(empty($data)){
				$data .= " $k='$v' ";
			}else{
				$data .= ", $k='$v' ";
			}
		}
	}
	$chk = $this->db->query("SELECT * FROM students where student_code ='$student_code' and id != '$id' ")->num_rows;
	if($chk > 0){
		return $id."x";
	}
	if(empty($id)){
		$save = $this->db->query("INSERT INTO students set $data");
		$id = $this->db->insert_id; // Get the inserted id
		log_history($this->db, 4, $id, "Add Student", "", "$student_code", 1, date('Y-m-d H:i:s'));
	}else{
		$query = "SELECT * FROM students WHERE id = $id";
$result = mysqli_query($this->db, $query);
$row = mysqli_fetch_array($result);
$_firstname = $row['firstname'];
$_middlename = $row['middlename'];
$_lastname = $row['lastname'];
$_studentcode = $row['student_code'];
$_curriculumid = $row['year'];
$_trackid= $row['track'];
 
		$save = $this->db->query("UPDATE students set $data where id = $id");
		$query = "SELECT * FROM students WHERE id = $id";
$result = mysqli_query($this->db, $query);
$row = mysqli_fetch_array($result);
$firstname = $row['firstname'];
$middlename = $row['middlename'];
$lastname = $row['lastname'];
$studentcode = $row['student_code'];
$curriculumid = $row['year'];
$trackid= $row['track'];
		log_history($this->db, 4, $id, "Edit Student", "$_firstname, $_middlename, $_lastname, $_studentcode, $_curriculumid, $_trackid", "$firstname, $middlename, $lastname, $studentcode, $curriculumid, $trackid", 1, date('Y-m-d H:i:s'));
	
	}
	if($save){
		$this->insert_subjecttostudent($syear, $id);
		return $id."y";
	}
		}

	
			
				
	function delete_student(){
		extract($_POST);
		$delete = $this->db->query("DELETE FROM students where id = $id");
		if($delete){
			return 1;
		}
	}
	function insert_subject(){
		extract($_POST);
		$data = "";
		foreach($_POST as $k => $v){
			if(!in_array($k, array('CurriculumSubjectID')) && !is_numeric($k)){
				if(empty($data)){
					$data .= " $k='$v' ";
				}else{
					$data .= ", $k='$v' ";
				}
			}
		}
	
		// Check if the combination of CurriculumID and SubjectID already exists in the curriculumsubject table
		$chk = $this->db->query("SELECT * FROM curriculumsubject where CurriculumID = '$CurriculumID' AND SubjectID = '$SubjectID'");
		if($chk->num_rows > 0){
			return 2; // Combination already exists
		}
	
		// Insert the CurriculumID and SubjectID into the curriculumsubject table
		$save = $this->db->query("INSERT INTO curriculumsubject (CurriculumID, SubjectID) VALUES ('$CurriculumID', '$SubjectID')");
		if($save){
			return 1; // Success
		} else {
			return 0; // Error
		}
	}
	function delete_CurriculumSubject(){
		extract($_POST);
	
		// Start a transaction
		$this->db->begin_transaction();
	
		try {
			// Retrieve the SubjectID, syear, and Semester from the curriculumsubject table
			$subject_query = $this->db->query("
				SELECT cs.SubjectID, c.syear, c.semester
				FROM curriculumsubject cs
				JOIN curriculum c ON cs.CurriculumID = c.CurriculumID
				WHERE cs.CurriculumSubjectID = $CurriculumSubjectID
			");
			
			if ($subject_query->num_rows > 0) {
				$subject_row = $subject_query->fetch_assoc();
				$SubjectID = $subject_row['SubjectID'];
				$syear = $subject_row['syear'];
				$Semester = $subject_row['semester'];
	
				// Delete corresponding entries in studentcurriculumsubject with the specific syear and Semester
				$delete_student_curriculum = $this->db->query("
					DELETE FROM studentcurriculumsubject 
					WHERE SubjectID = $SubjectID AND syear = $syear AND Semester = $Semester
				");
				if (!$delete_student_curriculum) {
					throw new Exception("Error deleting from studentcurriculumsubject: " . $this->db->error);
				}
	
				// Delete the entry from curriculumsubject table
				$delete_curriculum = $this->db->query("DELETE FROM curriculumsubject WHERE CurriculumSubjectID = $CurriculumSubjectID");
				if (!$delete_curriculum) {
					throw new Exception("Error deleting from curriculumsubject: " . $this->db->error);
				}
	
				// Commit the transaction
				$this->db->commit();
				return 1;
			} else {
				throw new Exception("Error: CurriculumSubjectID not found.");
			}
		} catch (Exception $e) {
			// Rollback the transaction in case of error
			$this->db->rollback();
			echo $e->getMessage();
			return 0;
		}
	}
	
	function save_StudentCurriculumSubject(){
		extract($_POST);
		$data = "";
		foreach($_POST as $k => $v){
			if(!in_array($k, array('StudentCurriculumSubjectID')) && !is_numeric($k)){
				if(empty($data)){
					$data .= " $k='$v' ";
				}else{
					$data .= ", $k='$v' ";
				}
			}
		}

		
		if($Prerequisite = "None" ){
			$data .= ", Status= 1 ";
		}else{
			$data .= ", Status= 0 ";

		}
		$chk = $this->db->query("SELECT * FROM studentcurriculumsubject where StudentCurriculumSubjectID ='$StudentCurriculumSubjectID'");
		if($chk->num_rows > 0){
			return 2;
			exit;
		}
		if(empty($StudentCurriculumSubjectID)){
			$save = $this->db->query("INSERT INTO studentcurriculumsubject set $data");
		}else{
			$save = $this->db->query("UPDATE studentcurriculumsubject set $data where StudentCurriculumSubjectID = $StudentCurriculumSubjectID");
		}
		if($save){
			return 1;
		}
	}
	function delete_StudentCurriculumSubject(){
		extract($_POST);
		$delete = $this->db->query("DELETE FROM studentcurriculumsubject where StudentCurriculumSubjectID = $StudentCurriculumSubjectID");
		if($delete){
			return 1;
		}
	}
	
			
	

	function save_result(){
		extract($_POST);
		$data = "";
		foreach($_POST as $k => $v){
			if(!in_array($k, array('id','mark','subject_id')) && !is_numeric($k)){
				if(empty($data)){
					$data .= " $k='$v' ";
				}else{
					$data .= ", $k='$v' ";
				}
			}
		}
		$chk = $this->db->query("SELECT * FROM results where student_id ='$student_id' and class_id='$class_id' and id != '$id' ");
		if($chk->num_rows > 0){
			return 2;
			exit;
		}
		if(empty($id)){
			$save = $this->db->query("INSERT INTO results set $data");
		}else{
			$save = $this->db->query("UPDATE results set $data where id = $id");
		}
		if($save){
				$id = empty($id) ? $this->db->insert_id : $id;
				$this->db->query("DELETE FROM result_items where result_id = $id");
				foreach($subject_id as $k => $v){
					$data= " result_id = $id ";
					$data.= ", subject_id = $v ";
					$data.= ", mark = '{$mark[$k]}' ";
					$this->db->query("INSERT INTO result_items set $data");
				}
				return 1;
		}
	}
	function delete_result(){
		extract($_POST);
		$delete = $this->db->query("DELETE FROM results where id = $id");
		if($delete){
			return 1;
		}
	}
	
}



