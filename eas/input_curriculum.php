<?php include 'db_connect.php'; ?>
<?php include 'logger.php'; ?>

<?php
if(isset($_GET['id'])){
    $qry = $conn->query("SELECT * FROM students where id ={$_GET['id']}")->fetch_array();
    foreach($qry as $k => $v){
        $$k = $v;
    }
}
?>

<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $studentID = $_POST['StudentID'];
    $selectedSubjects = $_POST['selected_subjects'];
    $semester = $_POST['semester'];

    // Fetch syear based on selected CurriculumID
    $curriculumID = $_POST['syear'];
    $curriculum_query = $conn->query("SELECT syear FROM curriculum WHERE CurriculumID = '$curriculumID'");
    $curriculum_row = $curriculum_query->fetch_assoc();
    $syear = $curriculum_row['syear'];

    foreach ($selectedSubjects as $subjectID) {
        // Insert the data into the studentcurriculumsubject table
        $query = "INSERT INTO studentcurriculumsubject (StudentID, SubjectID, Semester, syear) VALUES ('$studentID', '$subjectID', '$semester', '$syear')";
        $result = $conn->query($query);
        if (!$result) {
            echo "Error: " . $conn->error;
            exit();
        }
    }

    // Check if insertion was successful
    if ($result) {
        // Return success message as JSON
        echo json_encode(array('status' => 'success'));
        exit();
    } else {
        echo json_encode(array('status' => 'error', 'message' => 'Error saving data.'));
        exit();
    }
}
?>

<div class="container-fluid">
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" id="manage-curriculum-form" method="post">
        <input type="hidden" name="StudentID" value="<?php echo isset($_GET['id']) ? $_GET['id'] : '' ?>">
        <div id="msg" class="form-group"></div>
        
        <div class="form-group">
            <label for="syear" class="control-label">Curriculum</label>
            <select name="syear" id="syear" class="custom-select custom-select-sm" required>
    <?php 
    $qry = $conn->query("SELECT * FROM curriculum WHERE descriptive != '' ORDER BY CurriculumID ASC");
    while($row = $qry->fetch_array()):
    ?>
        <option value="<?php echo $row['CurriculumID'] ?>" <?php echo isset($syear) && $syear == $row['syear'] ? "selected" : '' ?>>
            <?php echo $row['descriptive'] . ' - ' . $row['syear']; ?>
        </option>
    <?php endwhile; ?>
</select>

        </div>
        <div id="semester_data" class=""></div>
        
        <div class="form-group text-dark" id="selected-subjects"></div>
        <div class="form-group text-dark">
            <div class="form-check">
                <input class="form-check-input" type="checkbox" id="select-all-pencode">
                <label class="form-check-label" for="select-all-pencode">
                    Select All
                </label>
            </div>
        </div>
        <div class="form-group text-dark">
            <div class="form-group">
                <label for="semester">Semester:</label>
                <select name="semester" id="semester" class="form-control form-control-sm" required>
                    <option value="1" <?php echo isset($Semester) && $Semester == '1' ? 'selected' : '' ?>>First Year - First Semester</option>
                    <option value="2" <?php echo isset($Semester) && $Semester == '2' ? 'selected' : '' ?>>First Year - Second Semester</option>
                    <option value="3" <?php echo isset($Semester) && $Semester == '3' ? 'selected' : '' ?>>Second Year - First Semester</option>
                    <option value="4" <?php echo isset($Semester) && $Semester == '4' ? 'selected' : '' ?>>Second Year - Second Semester</option>
                    <option value="5" <?php echo isset($Semester) && $Semester == '5' ? 'selected' : '' ?>>Third Year - First Semester</option>
                    <option value="6" <?php echo isset($Semester) && $Semester == '6' ? 'selected' : '' ?>>Third Year - Second Semester</option>
                    <option value="7" <?php echo isset($Semester) && $Semester == '7' ? 'selected' : '' ?>>Fourth Year - First Semester</option>
                    <option value="8" <?php echo isset($Semester) && $Semester == '8' ? 'selected' : '' ?>>Fourth Year - Second Semester</option>
                </select>
            </div>
        </div>
      
        <!-- Save button -->
    </form>
</div>
<div class="modal fade" id="successModal" tabindex="-1" aria-labelledby="successModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="successModalLabel">Success</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        Data successfully saved.
      </div>
    </div>
  </div>
</div>
<script>
    $(document).ready(function(){
        $('#manage-curriculum-form').submit(function(e){
            e.preventDefault();
            start_load();
            var formData = $(this).serialize(); // Serialize form data
            $.ajax({
                url: $(this).attr('action'),
                method: $(this).attr('method'),
                data: formData,
                dataType: 'json', // Expect JSON response
                success: function(response){
                    if(response.status == 'success'){
                        // Show modal
                        $('#successModal').modal('show');
                        setTimeout(function(){
                            location.reload(); // Reload page after 1.5 seconds
                        }, 1500);
                    } else {
                        // Show error toast
                        alert_toast(response.message, 'error');
                    }
                    end_load();
                }
            });
        });

        $('#syear').change(function(){
            var curriculumID = $(this).val();
            $.ajax({
                url: 'getsemester.php',
                method: 'POST',
                data: { CurriculumID: curriculumID },
                success: function(response){
                    $('#semester_data').html(response);
                }
            });
        });

        // Handle checkbox selection
        $(document).on('change', '.pencode-checkbox', function() {
            var subjectID = $(this).data('subject-id');
            var isChecked = $(this).prop('checked');
            var selectedSubjects = $('#selected-subjects');
            if (isChecked) {
                if (!selectedSubjects.find('input[value="' + subjectID + '"]').length) {
                    selectedSubjects.append('<input type="hidden" name="selected_subjects[]" value="' + subjectID + '">');
                }
            } else {
                selectedSubjects.find('input[value="' + subjectID + '"]').remove();
            }
        });

        // Function to check/uncheck all aligned pencode checkboxes
        $('#select-all-pencode').change(function(){
            var isChecked = $(this).prop('checked');
            $('.pencode-checkbox').prop('checked', isChecked).trigger('change');
        });
    });
</script>
