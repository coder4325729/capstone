<?php include 'db_connect.php';
include 'query2.php'; 
include 'formatpencode.php';

// Check if the student ID is provided in the URL
if (isset($_GET['id'])) {
    // Retrieve the student ID from the URL
    $student_id = $_GET['id'];

} else {
    // Handle the case when the student ID is not provided in the URL
    echo "Student ID not provided.";
    exit; // Exit PHP execution
}

$student_curriculum_query = $conn->query("SELECT Status, Standing, year, student_code, track, CONCAT(students.firstname, ' ', students.middlename, ' ', students.lastname) as name FROM students WHERE id = $student_id");
$row = $student_curriculum_query->fetch_assoc();
$year = $row['year'];
$name = $row['name'];
$track = $row['track'];
$student_code = $row['student_code'];
$standing = $row['Standing'];
$Status = $row['Status'];

// Query to retrieve the school year based on the curriculum ID
$schoolyear_query = $conn->query("SELECT syear FROM curriculum WHERE syear = $year");
$row = $schoolyear_query->fetch_assoc();
$schoolyear = $row['syear'];

// Check if the form is submitted
$status_name = array("None", "None", "NC", "Passed", "Failed", "FA", "INC", "C");
$status_color = array("pencodegreen", "pencodered", "pencodered", "pencodeblack", "pencodegreen", "pencodered", "pencodeyellow", "pencodeblack");
$hasHonor = true;


?>

<style>
 .btn-square {
    width: 50px; /* Adjust the width as needed */
    height: 50px; /* Same as the width to make it square */
    border-radius: 0;
    background-color: green; /* Removes rounded corners */
}

/* Style for the fixed button */
#saveGradesBtn {
    position: fixed;
    bottom: 20px; /* Adjust the distance from the bottom */
    right: 20px; /* Adjust the distance from the right */
    z-index: 9999; /* Ensure the button stays above other content */
}
.pencodegreen{
 color: green;
}
.pencodeblack{
 color: black;
}
.pencodered{
 color: red;
}
.pencodeyellow{
 color: darkgoldenrod;
}
@media print {
    th.action,
    td.action {
        display: none;
    }
    .no-print {
        display: none !important;
    }
}
.table{
    font-size: 14px;
}
.action button{
    padding:6px;
}
.table td,
.table th {
    padding: 4px; 
}
.student-header{
    padding: 10px;
}
h4{
    margin: 14px 0 14px 4px;
    font-size: 22px;
}
.status{
    margin: 10px  0 10px;
}
.col-4{
    align-self:center;
    
}
#printButton{
    margin: 0 0 5px 2px;
}
#edits{
    margin: 0 0 5px 2px;
    border-radius: 3px;
}
</style>
<a href="javascript:void(0)" id="edits" data-id='<?php echo $student_id; ?>' class="btn btn-flat manage_student" style="background-color: #1F3761; color: white;">
                                    Edit Student
                                </a>
<div class="card student-header">
    <div class="row">
    <div class="col-4">
        <h4><?php echo $student_code?></h4>
        <h4><?php echo $name?></h4>
        <h4>Curriculum Year: <?php echo strtoupper($year)?> - <?php echo strtoupper($track)?></h4>
    </div>

    <div class="col-8">
        <div class="status">
        <label for="">Admission Status: </label>
        <select name="selector1">
        <option value ="2" <?php echo ($Status == 2? 'selected': ''); ?>>Irregular</option>
            <option value ="1"<?php echo ($Status == 1? 'selected': ''); ?>>Regular</option>
        </select>
    </div>
    <div>
    <p><b>Standing:</b> 
    <?php 
        // Assuming $standing contains the numeric value
        switch($standing) {
            case 1:
                echo "1st Year";
                break;
            case 2:
                echo "2nd Year";
                break;
            case 3:
                echo "3rd Year";
                break;
            case 4:
                echo "4th Year";
                break;
            default:
                echo "Unknown";
        }
    ?>
    </p>
</div>
    <div><b>Honor Standing:</b> <span id="honorStanding"></span></div>
</div>
</div>
</div>

<div class="no-print">
    
<button id="printButton" class="btn" style="background-color: #1F3761; color: white;" id="printButton" data-id="<?php echo $student_id; ?>">Print</button>

</div>
    <div class="card card-outline card-secondary">
        <div class="card-header">
            <div class="card-tools">
            </div>
        </div>
       
            <div class="card-body row">
            <div class="col-12">
            </div>
            <?php
        $total_units_year1 = 0;
       $total_units_year1_earned = 0;
       $total_units_year2 = 0;
       $total_units_year2_earned = 0;
       $total_units_year3 = 0;
       $total_units_year3_earned = 0;
       $total_units_year4 = 0;
       $total_units_year4_earned = 0;
        $totalEarnedUnitsAllTables = 0;
        $allGrades = [];
        $status_colors = [];
        for ($i = 1; $i <= 8; $i++) {
           
            if ($i > 8) {
                // Fetch the curriculum data from the database for the descriptive column with a specific condition
                $curriculumTitle_query = $conn->query("SELECT descriptive FROM curriculum WHERE semester = $i AND syear = $schoolyear");
                $curriculumTitle_row = $curriculumTitle_query->fetch_assoc();
                $curriculumTitle = isset($curriculumTitle_row['descriptive']) ? $curriculumTitle_row['descriptive'] : 'No data available';
            } else {
                $curriculumTitle = "Year " . ceil($i / 2) . " - " . ($i % 2 == 1 ? "First" : "Second") . " Semester";
            }
            
            // Fetch data from the database
            $qry = $conn->query(select_StudentCurriculumSubject(2024, $i, $student_id));
            $r = array();
            while ($row = $qry->fetch_assoc()) {
                if (empty($r)) {
                    $r = [$row['Pencode'] => $row];
                } else {
                    if  (array_key_exists($row['Pencode'], $r)) {
                        $postreq = "," . $row['Postrequisite'];
                        $r[$row['Pencode']]['Postrequisite'] .= $postreq;
                    } else {
                        $r[$row['Pencode']] = $row;
                    }
                }
            }
            
            // Check if there are any rows fetched for this curriculum
            if (!empty($r)) {
        ?>
                <div class="col-6">
                    <h5><?php echo $curriculumTitle; ?></h5>
                    <div class="table-responsive">
                        <table class="table tabe-hover table-bordered" id="list_<?php echo $i; ?>">
                            <thead>
                                <tr>
                                    <th>Pen Code</th>
                                    <th>Descriptive Title</th>
                                    <th>Lec</th>
                                    <th>Lab</th>
                                    <th>Total</th>
                                    <th>Pre-requisite</th>
                                    <th>Grade</th>
                                    <th>Status</th>
                                    <th class= "action">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                           $total_lec = 0;
                           $total_lab = 0;
                           $total_lec_earned = 0;
                           $total_lab_earned = 0;
                           $total_units = 0;
                           $total_units_e = 0;
                            $totalGradePoints = 0;
                            $totalGradesCount = 0;
                           
                           
                            
                            foreach ($r as $key => $value) {
                                $row = $value;

                                $total_lec += $row['Lec'];
                                $total_lab += $row['Lab'];
                                if ($status_color[$row['Status']] === "pencodeblack") {
                                    // Add lecture and laboratory units for black-highlighted subjects
                                    $total_lec_earned += $row['Lec'];
                                    $total_lab_earned += $row['Lab'];
                                }
                               
                                // Exclude the grade if Pencode contains "NST"
                                if (strpos($row['Pencode'], 'NST') === false && strpos($row['Pencode'], 'SSP') === false && strpos($row['Pencode'], 'PED') === false) {
                                    $totalGradePoints += $row['Grade'];
                                    $totalGradesCount++;
                                }
                                $total_units =  $total_lec + $total_lab;
                                $total_units_e =  $total_lec_earned + $total_lab_earned;
                                $student_status = "No Honor";
                            
                                    if ($status_color[$row['Status']] === "pencodeblack" && $row['Grade'] <= 2.00) {
                                        $student_status = "Running for Honors";
                                    }
                                 
                               if (is_numeric($row['Grade'])) {
                                        $allGrades[] = $row['Grade'];
                                        $status_colors[] = $status_color[$row['Status']];
                                    }
                            ?>
                                <tr>
                                    <td class="col-2" id="penCodeColumn_<?php echo formatpencode($row['Pencode']) ?>">
                                        <b id="pencode_<?php echo formatpencode($row['Pencode']) ?>" class="pencode <?php echo $status_color[$row['Status']] ?>" data-pencode-id ="<?php echo $row['Pencode'] ?>"><?php echo ucwords($row['Pencode']) ?></b>
                                    </td>
                                    <td class="col-4"><?php echo ucwords($row['Description']) ?></td>
                                    <td class="col-1"><b><?php echo ucwords($row['Lec']) ?></b></td>
                                    <td class="col-1"><?php echo ucwords($row['Lab']) ?></td>
                                    <td class="col-1"><?php echo ucwords($row['Lab'] + $row['Lec']) ?></td>
                                    <td class="col-1" data-postrequisite-id="<?php echo formatpencode($row['Postrequisite'])?>" id="prerequisite_<?php echo formatpencode($row['Pencode'])?>" data-prerequisite-id="<?php echo formatpencode($row['Prerequisite'])?>"><?php echo ucwords($row['Prerequisite']) ?></td>
                                    <td class="col-1 grade_column" contenteditable="false" id="grade_<?php echo formatpencode($row['Pencode']) ?>" data-grade-id="<?php echo $row['StudentCurriculumSubjectID'] ?>"><?php   if(strpos($row['Pencode'], 'SSP') !== false) {
        // Display only letters if Pencode contains "SSP"
        echo ucwords($status_name[$row['Status']]);
    } else {
        // Otherwise, display the grade as it is
        echo $row['Grade'];
    }?></td>
                                    <td class="col-1 status_column"><b><?php echo ucwords($status_name[$row['Status']]) ?></b></td>
                                    <td class="text-center col-1 action">
                                        <div class="btn-group">
                                            <!-- Edit button -->
                                            <button type="button" class="btn btn-info btn-flat edit_grade" data-id="<?php echo $row['StudentCurriculumSubjectID'] ?>">
                                                <i class="fas fa-edit"></i>
                                            </button>
                                            <!-- Save button (hidden by default) -->
                                            <button type="button" class="btn btn-success btn-flat save_grade" pencode="<?php echo $row['Pencode'] ?>" postrequisite="<?php echo formatpencode($row['Postrequisite'])?>" prerequisite= "<?php echo formatpencode($row['Prerequisite'])?>"  data-id="<?php echo $row['StudentCurriculumSubjectID'] ?>" style="display: none;">
                                                <i class="fas fa-save"></i>
                                            </button>
                                            <!-- Delete button -->
                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
               
            </button>
            <div class="dropdown-menu">
    <?php
    // Fetch audit records from the database for the specific StudentCurriculumSubjectID
    $audit_query = $conn->query("SELECT audit.AuditID, audit.Action, audit.OldData, audit.NewData, audit.ModifiedDate, users.firstname, users.username FROM audit JOIN users ON users.id = audit.ModifiedBy WHERE audit.ContentID = '".$row['StudentCurriculumSubjectID']."'");
    // Loop through each audit record
    while ($audit_row = $audit_query->fetch_assoc()):
    ?>
        <a class="dropdown-item" href="#">Last Modified By - <?php echo $audit_row['username']; ?></a>
    <?php endwhile; ?>
</div>
                                      </div>
                                      
                                    </td>
                            <?php } 
                            $totalEarnedUnitsAllTables += $total_units_e;
                            if ((ceil($i / 2) == 1 || ceil($i / 2) == 2 || ceil($i / 2) == 3 || ceil($i / 2) == 4) && ($i % 2 == 1 || $i % 2 == 0)) {
                            if (ceil($i / 2) == 1) {
                                $total_units_year1 += $total_units;
                                $total_units_year1_earned += $total_units_e;
                            } else if (ceil($i / 2) == 2) {
                                $total_units_year2 += $total_units;
                                $total_units_year2_earned += $total_units_e;
                            } else if (ceil($i / 2) == 3) {
                                $total_units_year3 += $total_units;
                                $total_units_year3_earned += $total_units_e;
                            }else if (ceil($i / 2) == 4) {
                                $total_units_year4 += $total_units;
                                $total_units_year4_earned += $total_units_e;
                            }
                                                }  
                                                
                                                // Calculate total grade points and count grades
                                               
                           ?>
                            </tbody>
                                
                            </tr>
                                <tr><td colspan ="2" style="text-align: center;"><b>TOTAL UNITS</b></td>
                                <?php echo "<td>$total_lec</td>";
                                 echo "<td>$total_lab</td>";
                                 echo "<td>$total_units</td>";?>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                       
                                    </td>
                        </tr>
                        </tr>
                                <tr><td colspan ="2" style="text-align: center;"><b>EARNED UNITS</b></td>
                                <?php echo "<td>$total_lec_earned</td>";
                                  echo "<td>$total_lab_earned</td>";
                                  echo "<td>$total_units_e</td>";?>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                       
                                    </td>
                        </tr>
                        </tr>
                                <tr><td colspan ="2" style="text-align: center;"><b>GWA & Student Status</b></td>
                                <td colspan ="7" style="text-align: center;" ><?php
  $average = ($totalGradesCount > 0) ? round($totalGradePoints / $totalGradesCount, 2) : 0;
  echo $average;
?>:<?php
                                            // Calculate total units for the entire table
                                            echo $student_status;
                                        ?> </td>
                                </tr>
                        </table>
                    </div>
                </div>
        <?php
            }
            }
       ?>
    </div>
    
</div>
<div class="modal fade" id="successModal" tabindex="-1" aria-labelledby="successModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="successModalLabel">Error</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                Input must be alphabetical.
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function(){
    // Event handler for edit button click
    function changeHighlightFromRedToGreen1() {
        // Loop through each table
        $('[id^="list_"]').each(function(index) {
            var currentTable = $(this);
            var allPencodesBlack = currentTable.find('.pencode').not('.pencodeblack').length === 0;

            // If all pencodes in the current table are highlighted as pencodeblack
            if (allPencodesBlack) {
                // Traverse the next tables to find the first one that doesn't have all pencodes highlighted as pencodeblack
                for (var nextIndex = index + 1; nextIndex < $('[id^="list_"]').length; nextIndex++) {
                    var nextTable = $('[id^="list_"]').eq(nextIndex);
                    var notAllPencodesBlack = nextTable.find('.pencode').not('.pencodeblack').length > 0;

                    // If the table does not have all pencodes highlighted black
                    if (notAllPencodesBlack) {
                        // Change the highlight from pencodered to pencodegreen for the pencodes in the next table
                        nextTable.find('.pencode.pencodered').removeClass('pencodered').addClass('pencodegreen');
                        break; // Exit the loop after finding the first matching table
                    }
                }
            }
        });
    }

    // Event handler for select change
    $('select[name="selector1"]').change(function() {
       
        changeHighlightFromRedToGreen1();
    });

    // Trigger change event of admission status select element only if the admission status is "Regular" or "Irregular"
    const triggerChangeIfNeeded = () => {
        const admissionStatus = $('select[name="selector1"]').val();
        if (admissionStatus === "1" || admissionStatus === "2") {
            $('select[name="selector1"]').trigger('change');
        }
    };

    // Initial trigger if needed
    triggerChangeIfNeeded();
    var allGrades = <?php echo json_encode($allGrades); ?>;
    var statusColors = <?php echo json_encode($status_colors); ?>;
    var totalGradesCount = allGrades.length;
    var totalGradePoints = 0;
    var honorStanding = "No Honor";

    // Calculate total grade points only for grades with numeric values and with status "Passed" or "Completed"
    for (var i = 0; i < totalGradesCount; i++) {
        if (!isNaN(allGrades[i]) && (statusColors[i] === "pencodegreen" || statusColors[i] === "pencodeblack")) {
            totalGradePoints += parseFloat(allGrades[i]);
        }
    }
    var hasDisqualifyingStatus = statusColors.some(function(status) {
        return status === "pencodered" || status === "pencodeyellow";
    });

    // Calculate highest GWA
    var highestGWA = (totalGradesCount > 0) ? totalGradePoints / totalGradesCount : 0;
if(!hasDisqualifyingStatus){
    // Determine honor standing based on highest GWA
    if (highestGWA <= 1.75 && highestGWA > 0) {
        // Check if there is no grade lower than 2.00
        var noGradeBelow2 = allGrades.every(function(grade) {
            return isNaN(grade) || grade <= 2.00;
        });

        // Update honor standing if conditions are met
        if (noGradeBelow2) {
            honorStanding = "Cum Laude";
        }
    }

    if (highestGWA <= 1.50 && highestGWA > 0) {
        // Check if there is no grade lower than 1.75
        var noGradeBelow175 = allGrades.every(function(grade) {
            return isNaN(grade) || grade <= 1.75;
        });

        // Update honor standing if conditions are met
        if (noGradeBelow175) {
            honorStanding = "Magna Cum Laude";
        }
    }

    if (highestGWA <= 1.25 && highestGWA > 0) {
        // Check if there is no grade lower than 1.50
        var noGradeBelow150 = allGrades.every(function(grade) {
            return isNaN(grade) || grade <= 1.50;
        });

        // Update honor standing if conditions are met
        if (noGradeBelow150) {
            honorStanding = "Summa Cum Laude";
        }
    }
}

    // Display honor standing
    $('#honorStanding').text(honorStanding);



    $(document).on('click', '.edit_grade', function() {
        var row = $(this).closest('tr'); // Get the closest row to the clicked button
        var gradeCell = row.find('.grade_column'); // Find the grade cell within that row
        var pencodeCell = row.find('.pencode'); // Find the pencode cell within that row
        
        // Check if the pencode cell has the class "pencodered"
        if (pencodeCell.hasClass('pencodered')) {
            // Display the confirmation dialog
            if (!confirm("Are you sure you want to edit this grade?")) {
                return; // If the user clicks "No", exit the function
            }
        }
        
        gradeCell.text(''); // Clear the grade cell content
        gradeCell.prop('contenteditable', true); // Enable editing for the grade cell
        gradeCell.focus(); // Focus on the grade cell
        $(this).hide(); // Hide the edit button
        $(this).siblings('.save_grade').show(); // Show the save button
    });

    // Event handler for save button click
    $(document).on('click', '.save_grade', function() {
        var gradeCell = $(this).closest('tr').find('.grade_column');
        var statusCell = $(this).closest('tr').find('.status_column'); // Get the status cell
        var studentCurriculumSubjectID = $(this).data('id');
        var grade = gradeCell.text().trim();
        var pencode = $(this).attr('pencode');
        var prerequisite = $(this).attr('prerequisite');
        var postrequisite = $(this).attr('postrequisite').split(',');
       
        // Disable editing of the grade cell
        gradeCell.prop('contenteditable', false);

        // Hide the save button and show the edit button
        $(this).hide();
        $(this).siblings('.edit_grade').show();

        // If the entered grade is a whole number, append ".00"
        var parsedGrade = parseFloat(grade);
        if (!isNaN(parsedGrade) && parsedGrade === parseInt(parsedGrade)) {
            grade += '.00';
        }

        // Send the updated grade to the server for saving
        $.ajax({
            url: 'process2.php',
            method: 'POST',
            data: {
                StudentCurriculumSubjectID: studentCurriculumSubjectID,
                Grade: grade
            },
            success: function(resp) {
             location.reload();
        setTimeout(function() {
            location.reload();
        }, 100); // Small delay to ensure the second reload occurs
    },
            error: function(xhr, status, error) {
                console.error(xhr.responseText);
            }
        });
    });
    
   $('select[name="selector1"]').change(function() {
        var admissionStatus = $(this).val(); // Get the selected admission status
        var studentId = '<?php echo $student_id; ?>'; // Get the student ID

        // Send AJAX request to update the admission status in the database
        $.ajax({
            url: 'update_student.php',
            method: 'POST',
            data: {
                student_id: studentId,
                admission_status: admissionStatus
            },
            success: function(response) {
                console.log(response); // Log the response from the server
                if (admissionStatus === "2") { // If "Irregular" is selected
                location.reload(); // Reload the page
                }
            },
            error: function(xhr, status, error) {
                console.error(xhr.responseText); // Log any errors
            }
        });
    });
    
    var totalearnedunits = parseFloat('<?php echo $totalEarnedUnitsAllTables; ?>');
    var studentId = '<?php echo $student_id; ?>'; // Get the student ID
    var year1total = parseFloat('<?php echo $total_units_year1; ?>');
    var year2total = parseFloat('<?php echo $total_units_year2 + $total_units_year1; ?>');
    var year3total = parseFloat('<?php echo $total_units_year3 + $total_units_year2 + $total_units_year1; ?>');
    // Determine the yearLevel based on the total earned units
    console.log(year2total);
    var yearLevel;
    if (totalearnedunits <= (year1total - 1)) {
        yearLevel = 1;
    } else if (totalearnedunits <= (year2total - 1)) {
        yearLevel = 2;
    } else if (totalearnedunits <= year3total) {
        yearLevel = 3;
    } else {
        yearLevel = 4;
    }
    
    
    console.log(studentId, yearLevel);
    
    // Send AJAX request to update the yearLevel in the database
    $.ajax({
        url: 'update_student.php',
        method: 'POST',
        data: {
            student_id: studentId,
            year_level: yearLevel // Send the calculated yearLevel instead of year_level
        },
        success: function(response) {
            console.log(response); // Log the response from the server
        },
        error: function(xhr, status, error) {
            console.error(xhr.responseText); // Log any errors
        }
    });


    $(document).on('click', '.manage_student', function () {
             uni_modal("Manage Student","edit_student.php?id="+$(this).attr('data-id'));
        });

    $(document).on('click', '.manage_grade', function(){
        uni_modal("Manage Grade","manage_grade.php?StudentCurriculumSubjectID="+$(this).attr('data-id'))
    });

    $('.insert_status').click(function(){
        uni_modal("Set Status","insert_grade.php?StudentCurriculumSubjectID="+$(this).attr('data-id'));
    });
    function updatePencodeColor(pencode, grade, status, is_postreq = false) {
    // Clear previous highlight explicitly
    if (!is_postreq) {
        $("#pencode_" + pencode).removeClass("pencodeyellow pencodered pencodegreen pencodeblack");
    }

    var color = "";

    // Check for numeric grade first
    if (!isNaN(parseFloat(grade))) {
        var numericGrade = parseFloat(grade);
        if (numericGrade > 0 && numericGrade <= 3) {
            color = "pencodeblack"; // Black for pencode (passing grade 0-3)
        } else if (numericGrade > 3) {
            color = "pencodegreen"; // Green for pencode (grades > 3)
        }
    } else {
        // Check for special grades (INC, NC, FA)
        if (grade.toLowerCase() === "inc") {
            color = "pencodeyellow";
        } else if (grade.toLowerCase() === "nc" || grade.toLowerCase() === "fa") {
            color = "pencodered";
        } else if (grade.toLowerCase() === "c") {
            color = "pencodeblack";
        }
    }

    // Separate logic for postrequisite (green for <= 3, red for > 3)
    if (is_postreq && $('select[name="selector1"]').val() === "2"  || $('select[name="selector1"]').val() === "1") {
        if ($("#pencode_" + pencode).hasClass("pencodeblack")) {
            return; // Exit function if postreq is already highlighted as pencodeblack
        }
        if (numericGrade > 0 && numericGrade <= 3 || grade.toLowerCase() === "c") {
            color = "pencodegreen"; // Green for postreq (passing grade)
            // Update status to 0 (passed) for each pencode in the array
            var pencodesArray = pencode.split(","); // Split postreq into an array
            // Add space to pencodes
            pencodesArray = pencodesArray.map(p => p.replace(/([a-zA-Z]+)(\d+)/, "$1 $2"));
            updateStatusInDatabase(pencodesArray, 0);
            console.log(pencodesArray);
        } else {
            color = "pencodered"; // Red for postreq (grades > 3)
            // Update status to 1 (failed) for each pencode in the array
            var pencodesArray = pencode.split(","); // Split postreq into an array
            // Add space to pencodes
            pencodesArray = pencodesArray.map(p => p.replace(/([a-zA-Z]+)(\d+)/, "$1 $2"));
            updateStatusInDatabase(pencodesArray, 1);
        }
        
        // Check if admission status is "Irregular" and remove postreq highlight
        if ($('select[name="selector1"]').val() !== "1") {
            for (var i = 0; i < pencodesArray.length; i++) {
                $("#pencode_" + pencodesArray[i]).removeClass("pencodered pencodegreen");
        }
    }
    }
    // Add the color class to the Pencode cell
    $("#pencode_" + pencode).addClass(color);
}


// Function to update status in database
function updateStatusInDatabase(pencodes, status) {
    // Retrieve student ID
    console.log(pencodes);
    var studentID = <?php echo $student_id; ?>;
    // AJAX call to update status in database
    $.ajax({
        url: 'update_status.php', // Replace 'update_status.php' with the actual PHP file name
        type: 'POST',
        data: {
            student_id: studentID,
            pencodes: pencodes, // Pass array of pencodes
            status: status
        },
        success: function(response) {
            // Handle success response
            console.log('Status updated successfully');
        },
        error: function(xhr, status, error) {
            // Handle error
            console.error('Error updating status:', error);
        }
    });
}

// Define a function to handle highlighting based on admission status and year level
// Define a function to handle highlighting based on admission status and year level
function highlightBasedOnAdmissionStatusAndEarnedUnitsTotal() {
    var admissionStatus = $('select[name="selector1"]').val();
    var totalEarnedUnits = parseFloat('<?php echo $totalEarnedUnitsAllTables; ?>');
    var year1total = parseFloat('<?php echo $total_units_year1; ?>');
    var year2total = parseFloat('<?php echo $total_units_year2 + $total_units_year1; ?>');
    var year3total = parseFloat('<?php echo $total_units_year3 + $total_units_year2 + $total_units_year1; ?>');
    // Check if admission status is "Regular"
    if (admissionStatus === "1") {
        // Remove any previous highlights
        $('.pencode').removeClass('pencodegreen pencodered');

        // Highlight based on total earned units
        if (totalEarnedUnits <= (year1total - 1)) {
            $('#list_1').find('.pencode').addClass('pencodegreen');
            $('[id^="list_"]:not(#list_1)').find('.pencode').addClass('pencodered');
        } else if (totalEarnedUnits <= (year2total - 1)) {
            $('#list_1, #list_2').find('.pencode').addClass('pencodeblack');
            $('#list_3').find('.pencode').addClass('pencodegreen');
            $('[id^="list_"]:not(#list_1):not(#list_2):not(#list_3)').find('.pencode').addClass('pencodered');
        } else if (totalEarnedUnits <= (year3total - 1)) {
            $('#list_1, #list_2, #list_3, #list_4').find('.pencode').addClass('pencodeblack');
            $('#list_5').find('.pencode').addClass('pencodegreen');
            $('[id^="list_"]:not(#list_1):not(#list_2):not(#list_3):not(#list_4):not(#list_5)').find('.pencode').addClass('pencodered');
        } else {
            $('#list_1, #list_2, #list_3, #list_4, #list_5, #list_6').find('.pencode').addClass('pencodeblack');
            $('#list_7').find('.pencode').addClass('pencodegreen');
            $('[id^="list_"]:not(#list_1):not(#list_2):not(#list_3):not(#list_4):not(#list_5):not(#list_6):not(#list_7)').find('.pencode').addClass('pencodered');
        }

        // Check if the current table is the first semester of any year and if all pencodes are highlighted as pencodeblack
        if ($('#selector2').val() % 2 === 1 && $('#list_' + $('#selector2').val()).find('.pencode').length === $('#list_' + $('#selector2').val()).find('.pencodeblack').length) {
            // Highlight all pencodes in the second semester of the same year as pencodegreen
            $('#list_' + (parseInt($('#selector2').val()) + 1)).find('.pencode').addClass('pencodegreen');
        }
    }
}

// Trigger the change event of admission status select element after the page loads
$(document).ready(function() {
    // Call the highlighting function when admission status changes
    $('select[name="selector1"]').change(function() {
        highlightBasedOnAdmissionStatusAndEarnedUnitsTotal();
        changeHighlightFromRedToGreen();
    });
    function changeHighlightFromRedToGreen() {
    // Loop through each table
    $('[id^="list_"]').each(function(index) {
        var currentTable = $(this);
        var nextTable = $('[id^="list_"]').eq(index + 1); // Get the next table

        // Check if both the current and next tables exist
        if (currentTable.length && nextTable.length) {
            // Check if all pencodes in the current table are highlighted as pencodeblack
            var allPencodesBlack = currentTable.find('.pencode').not('.pencodeblack').length === 0;
            // Check if all pencodes in the next table are highlighted as pencodered
            var allPencodesRed = nextTable.find('.pencode').not('.pencodered').length === 0;

            // If all pencodes in the current table are pencodeblack and all pencodes in the next table are pencodered
            if (allPencodesBlack && allPencodesRed) {
                // Change the highlight from pencodered to pencodegreen for the pencodes in the next table
                
                nextTable.find('.pencode').removeClass('pencodered').addClass('pencodegreen');
            }
        }
    });
}


    // Trigger change event of admission status select element only if the admission status is "Regular"
    if ($('select[name="selector1"]').val() === "1") {
        $('select[name="selector1"]').trigger('change');
    }
  
});

function checkAndUpdateStatus1() {
        var admissionStatus = $('select[name="selector1"]').val(); // Get the selected admission status
        var totalEarnedUnits = parseFloat('<?php echo $totalEarnedUnitsAllTables; ?>'); // Get the total earned units
        var studentId = '<?php echo $student_id; ?>'; // Get the student ID
        var year2total = parseFloat('<?php echo $total_units_year2 + $total_units_year1; ?>');
        console.log(admissionStatus, totalEarnedUnits);

        // Check if the admission status is "Irregular" and the total earned units are below 94
        if (admissionStatus === "2" && totalEarnedUnits == year2total) {
        // Check if the pencode of the prerequisite "3rd year standing" is highlighted black
        if ($('.pencode[data-prerequisite="3RD YEAR STANDING"]').hasClass('pencodeblack')) {
            // Update the status of the pencode with the prerequisite of "3rd year standing"
            updatePencodeStatus1(studentId);
        }
    }
}

    // Call the function on page load
    checkAndUpdateStatus1();

    // Call the function on selector change
    $('select[name="selector1"]').change(function() {
        checkAndUpdateStatus1();
    });

    // Function to update the status of the pencode with the prerequisite of "3rd year standing" to zero
    function updatePencodeStatus1(studentId) {
        var prerequisite = "3RD YEAR STANDING"; // Prerequisite text to match

        // Send AJAX request to update the status in the database
        $.ajax({
            url: 'update_pencode_status.php',
            method: 'POST',
            data: {
                student_id: studentId,
                prerequisite: prerequisite
            },
            success: function(response) {
                console.log(response); // Log the response from the server
            },
            error: function(xhr, status, error) {
                console.error(xhr.responseText); // Log any errors
            }
        });
    }
function checkAndUpdateStatus() {
        var admissionStatus = $('select[name="selector1"]').val(); // Get the selected admission status
        var totalEarnedUnits = parseFloat('<?php echo $totalEarnedUnitsAllTables; ?>'); // Get the total earned units
        var studentId = '<?php echo $student_id; ?>'; // Get the student ID
        var year2total = parseFloat('<?php echo $total_units_year2 + $total_units_year1; ?>');
        console.log(admissionStatus, totalEarnedUnits);

        // Check if the admission status is "Irregular" and the total earned units are below 94
        if (admissionStatus === "2" && totalEarnedUnits < year2total) {
            // Update the status of the pencode with the prerequisite of "3rd year standing"
            if ($('.pencode[data-prerequisite="3RD YEAR STANDING"]').hasClass('pencodeblack')) {
            updatePencodeStatus(studentId);
        }
    }
    }

    // Call the function on page load
    checkAndUpdateStatus();

    // Call the function on selector change
    $('select[name="selector1"]').change(function() {
        checkAndUpdateStatus();
    });

    // Function to update the status of the pencode with the prerequisite of "3rd year standing" to zero
    function updatePencodeStatus(studentId) {
        var prerequisite = "3RD YEAR STANDING"; // Prerequisite text to match

        // Send AJAX request to update the status in the database
        $.ajax({
            url: 'update_pencode_status1.php',
            method: 'POST',
            data: {
                student_id: studentId,
                prerequisite: prerequisite
            },
            success: function(response) {
                console.log(response); // Log the response from the server
            },
            error: function(xhr, status, error) {
                console.error(xhr.responseText); // Log any errors
            }
        });
    }

    function highlightITE311(tableRowsSelector) {
    var hasOtherStatus = false; // Flag to track if any row has a status other than "Passed" or "Completed"

    // Loop through all rows
    $(tableRowsSelector).each(function() {
        // Get the status text of the current row
        var statusText = $(this).find('.status_column').text().trim().toLowerCase();
        // Get the Pencode text of the current row
        var pencodeCell = $(this).find('.pencode');

        // Check if the Pencode contains "ITE 311"
        if (pencodeCell.text().trim().toUpperCase().includes("ITE 311")) {
            // Remove any existing highlight and add the pencodegreen class to the Pencode cell
            pencodeCell.removeClass('pencodered pencodegreen').addClass('pencodegreen');
        } else if (statusText !== "") { // Exclude rows with no status
            // Check if the status is neither "Passed" nor "Completed"
            if (statusText !== "passed" && statusText !== "c") {
                hasOtherStatus = true; // Set flag to true if any row has another status
            }
        }
    });

    // Remove any previous highlight for ITE 311
    $('.table tbody tr .pencode:contains("ITE 311")').removeClass('pencodered pencodegreen');

    // Check the flag to determine highlighting for ITE 311
    if (!hasOtherStatus) {
        // If all relevant rows have status "Passed" or "Completed", highlight ITE 311 as pencodegreen
        $('.table tbody tr .pencode:contains("ITE 311")').addClass('pencodegreen');
    } else {
        // If any relevant row has a status other than "Passed" or "Completed", highlight ITE 311 as pencodered
        $('.table tbody tr .pencode:contains("ITE 311")').addClass('pencodered');
    }
}

// Example of how to call the function:
highlightITE311('.table tbody tr');

    function updatepostreq(pencode, grade){
        pencode
    }

    function clearpencodecolor(pencode){
        $("#pencode_"+ pencode).removeClass("pencodered pencodegreen pencodeblack")
    }

    function updatepostreq(pencode, grade){
        pencode
    }


    function pencodetopostrequisite(pencode, postrequisite) {
        var similarPostrequisites = {}; 

        if (!similarPostrequisites[pencode]) {
            similarPostrequisites[pencode] = postrequisite;
        } else {

            similarPostrequisites[pencode] += ', ' + postrequisite;
        }

        console.log('Pencode:', pencode, 'Postrequisites:', similarPostrequisites[pencode]);
    }


    $('[contenteditable="false"]').on('input', function() {
        var studentCurriculumSubjectID = $(this).data('grade-id');
        var gradeInput = this;
        var pencode = $(this).attr('id');
        pencode = pencode.replace("grade_", "");
        var grade = $(this).text();
        var prerequisite = $(this).siblings('td[data-prerequisite-id]').data('prerequisite-id');
        var postrequisites = $(this).siblings('td[data-postrequisite-id]').data('postrequisite-id')
        var postrequisite = postrequisites.split(',');
 
        updatePencodeColor(pencode, grade);
        for (p in postrequisite){
            updatePencodeColor(postrequisite[p], grade, <?php echo $Status; ?>, true);
            console.log(<?php echo $Status; ?>);
        }
       
        // Prepare the formData to be sent to the server
        var formData = { 
            StudentCurriculumSubjectID: studentCurriculumSubjectID, 
            Grade: grade 
        };
     
        // Send the formData to the server for processing
        $.ajax({
        url: 'process2.php',
        method: 'POST',
        data: formData,
        success: function(resp) {
            console.log(resp);
        },
        error: function(xhr, status, error) {
            console.error(xhr.responseText);
        }
    });
});

    function getPenCodeIdByPrerequisite(prerequisiteId) {
        var penCodeId = null; 

        $("#list_<?php echo $i; ?> tbody tr").each(function() {
            var prerequisiteTd = $(this).siblings('td[data-prerequisite-id="' + prerequisiteId + '"]').data('prerequisite-id');  

            if (prerequisiteTd.length) { 
                penCodeId = $(this).find('td:first-child').attr('id'); 
                return false; 
            }
        });

        return penCodeId; 
    }
});

document.getElementById('printButton').addEventListener('click', function() {
    var studentId = this.getAttribute('data-id');
    var printWindow = window.open('print.php?id=' + studentId, '_blank');
    printWindow.onload = function() {
// Hide date/time and text for printing
        printWindow.document.querySelectorAll('.no-print').forEach(function(element) {
            element.style.display = 'none';
        });
        printWindow.print();
    };
});
document.addEventListener("DOMContentLoaded", function () {
        // Get all grade columns
        var gradeColumns = document.querySelectorAll(".grade_column");

        // Loop through each grade column
        gradeColumns.forEach(function (column) {
            // Get the corresponding Pencode for this column
            var pencode = column.getAttribute("id").replace("grade_", "");

            // Check if the Pencode contains "SSP"
            if (pencode.includes("SSP")) {
                // Add event listener for when the content of the grade column changes
                column.addEventListener("input", function () {
                    // Get the entered grade
                    var grade = column.innerText.trim();

                    // Validate if the grade contains only alphabetical characters
                    if (!/^[a-zA-Z]+$/.test(grade)) {
                        // If not, display an error message
                        $('#successModal').modal('show'); 
                        setTimeout(function(){
                        location.reload(); // Reload the page after 2 seconds
                    }, 1500);
                        // Reset the grade column to an empty value
                        column.innerText = "";
                    }
                });
            }
        });
    });


    function checkAndUpdateStatus2() {
        var admissionStatus = $('select[name="selector1"]').val(); // Get the selected admission status
        var totalEarnedUnits = parseFloat('<?php echo $totalEarnedUnitsAllTables; ?>'); // Get the total earned units
        var studentId = '<?php echo $student_id; ?>'; // Get the student ID
        var year1total = parseFloat('<?php echo $total_units_year1; ?>');
        console.log(admissionStatus, totalEarnedUnits);

        // Check if the admission status is "Irregular" and the total earned units are below 94
        if (admissionStatus === "2" && totalEarnedUnits == year1total) {
            // Update the status of the pencode with the prerequisite of "3rd year standing"
            if ($('.pencode[data-prerequisite="2ND YEAR STANDING"]').hasClass('pencodeblack')) {
            updatePencodeStatus2(studentId);
        }
    }
}

    // Call the function on page load
    checkAndUpdateStatus2();

    // Call the function on selector change
    $('select[name="selector1"]').change(function() {
        checkAndUpdateStatus2();
    });

    // Function to update the status of the pencode with the prerequisite of "3rd year standing" to zero
    function updatePencodeStatus2(studentId) {
        var prerequisite = "2ND YEAR STANDING"; // Prerequisite text to match

        // Send AJAX request to update the status in the database
        $.ajax({
            url: 'update_pencode_status.php',
            method: 'POST',
            data: {
                student_id: studentId,
                prerequisite: prerequisite
            },
            success: function(response) {
                console.log(response); // Log the response from the server
            },
            error: function(xhr, status, error) {
                console.error(xhr.responseText); // Log any errors
            }
        });
    }
function checkAndUpdateStatus3() {
        var admissionStatus = $('select[name="selector1"]').val(); // Get the selected admission status
        var totalEarnedUnits = parseFloat('<?php echo $totalEarnedUnitsAllTables; ?>'); // Get the total earned units
        var studentId = '<?php echo $student_id; ?>'; // Get the student ID
        var year1total = parseFloat('<?php echo $total_units_year1; ?>');
        console.log(admissionStatus, totalEarnedUnits);

        // Check if the admission status is "Irregular" and the total earned units are below 94
        if (admissionStatus === "2" && totalEarnedUnits <= (year1total - 1)) {
            // Update the status of the pencode with the prerequisite of "3rd year standing"
            if ($('.pencode[data-prerequisite="2ND YEAR STANDING"]').hasClass('pencodeblack')) {
            updatePencodeStatus3(studentId);
        }
    }
}

    // Call the function on page load
    checkAndUpdateStatus3();

    // Call the function on selector change
    $('select[name="selector1"]').change(function() {
        checkAndUpdateStatus3();
    });

    // Function to update the status of the pencode with the prerequisite of "3rd year standing" to zero
    function updatePencodeStatus3(studentId) {
        var prerequisite = "2ND YEAR STANDING"; // Prerequisite text to match

        // Send AJAX request to update the status in the database
        $.ajax({
            url: 'update_pencode_status1.php',
            method: 'POST',
            data: {
                student_id: studentId,
                prerequisite: prerequisite
            },
            success: function(response) {
                console.log(response); // Log the response from the server
            },
            error: function(xhr, status, error) {
                console.error(xhr.responseText); // Log any errors
            }
        });
    }
   



    // Trigger change event of admission status select element only if the admission status is "Regular"
    


</script>