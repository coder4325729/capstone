<?php
include('db_connect.php');

// Check if the AuditID is provided
if (isset($_POST['AuditID'])) {
    // Retrieve AuditID from the POST data
    $auditID = $_POST['AuditID'];

    // Prepare SQL statement to delete the audit record
    $stmt = $conn->prepare("DELETE FROM audit WHERE AuditID = ?");
    $stmt->bind_param("i", $auditID);

    // Execute the statement
    if ($stmt->execute()) {
        // Return success message
        echo "Audit record deleted successfully.";
    } else {
        // Return error message
        echo "Error deleting audit record.";
    }

    // Close the statement
    $stmt->close();
}

// Close database connection
$conn->close();
?>
