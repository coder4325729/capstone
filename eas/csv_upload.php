<?php
include 'db_connect.php';


// Check if file is uploaded
if(isset($_FILES['file']['name'])) {
    $file = $_FILES['file']['tmp_name'];

    // Open the file for reading
    $handle = fopen($file, "r");

    // Skip the header row
    fgetcsv($handle);

    // Read and process each row of the CSV file
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        // Escape special characters and sanitize data
        $Pencode = mysqli_real_escape_string($conn, strtoupper($data[0])); // Assuming Pencode is the first column
        $Description = mysqli_real_escape_string($conn, ucwords(strtolower($data[1]))); // Assuming Description is the second column
        $Lec = mysqli_real_escape_string($conn, ucwords(strtolower($data[2]))); // Assuming Lec is the third column
        $Lab = mysqli_real_escape_string($conn, ucwords(strtolower($data[3]))); // Assuming Lab is the fourth column
        $Prerequisite = mysqli_real_escape_string($conn, strtoupper($data[4])); // Assuming Prerequisite is the fifth column
        
        // Insert the data into the subjects table
        $sql = "INSERT INTO subjects (Pencode, `Description`, Lec, Lab, Prerequisite) 
                VALUES ('$Pencode', '$Description', '$Lec', '$Lab', '$Prerequisite')";

        if (!mysqli_query($conn, $sql)) {
            echo "Error inserting data: " . mysqli_error($conn);
            exit;
        }
    }

    fclose($handle);
    echo "Data loaded successfully";
} else {
    echo "No file uploaded";
}

mysqli_close($conn);
?>
