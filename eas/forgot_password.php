<?php include('./header.php'); ?>
<body>
    <main id="main">
        <div class="title">
            <h4 style="color: white;"><?php echo $_SESSION['system']['name'] ?> - Forgot Password</h4>
        </div>
        <div id="login-center">
            <div class="card" style=" border-radius: 20px;">
                <div class="card-body">
                    <form id="forgot-password-form" action="reset_password.php" method="post">
                        <div class="form-group">
                            <label for="email">Enter your Email</label>
                            <input type="email" id="email" name="email" class="form-control form-control-sm">
                        </div>
                        <div class="w-100 d-flex justify-content-center align-items-center">
                            <button type="submit" class="btn-primary">Reset Password</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>
</body>
<?php include 'footer.php' ?>
</html>