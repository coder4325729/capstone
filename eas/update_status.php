<?php
include 'db_connect.php';

// Check if the request method is POST
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Retrieve the student ID, pencodes array, and status from the POST data
    $studentID = $_POST['student_id'];
    $pencodes = $_POST['pencodes']; // Array of pencodes
    $status = $_POST['status'];

    // Convert the pencodes array to a comma-separated string for SQL query
    $pencodesString = implode("','", $pencodes);

    // Perform necessary database operations to update the statuses
    // Example: Update the statuses of the postrequisites in the studentcurriculumsubject table based on the join with the subjects table
    // Replace 'your_database_connection_code' with your actual database connection code
    include 'db_connect.php'; // Include your database connection file

    // Assuming you have tables named 'studentcurriculumsubject' and 'subjects' with a common column 'SubjectID'
    $updateQuery = "UPDATE studentcurriculumsubject scs
                    INNER JOIN subjects s ON scs.SubjectID = s.SubjectID
                    SET scs.Status = '$status'
                    WHERE scs.StudentID = '$studentID' AND s.Pencode IN ('$pencodesString')";
    $result = $conn->query($updateQuery);
    
    // Check if the query was successful
    if ($result) {
        // Return a success message
        echo json_encode(array("success" => true, "message" => "Statuses updated successfully."));
    } else {
        // Return an error message
        echo json_encode(array("success" => false, "message" => "Failed to update statuses."));
    }
} else {
    // If the request method is not POST, return an error message
    echo json_encode(array("success" => false, "message" => "Invalid request method."));
}
?>
