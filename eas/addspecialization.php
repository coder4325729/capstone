
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<style>
    table, th, td {
        border: 1px solid black;
    }
    .curriculum {
        margin-left: 30px;
        margin-right: 30px;
    }

    body {
        margin: 0;
        padding: 0;
    }
    .firsttable {
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .text {
        display: flex;
        justify-content: center;
    }
    .buttons button {
        margin-right: 5px;
        background-color: transparent;
        border: 1px solid #77d491;
        border-radius: 5px;
        cursor: pointer;
        transition: background-color 0.3s ease;
    }

    .buttons button:hover {
        background-color: #77d491;
        color: white;
    }
    .buttons {
        margin-top: 10px;
        text-align: center;
    }
</style>
    <body class="curriculum" id="editableTable">
        <div class="text" id="curriculumTitle" contenteditable="true">
            <h1>Curriculum Specialization</h1>
        </div>
       
    
        <div class="firsttable">
            <table class="curriculum">
                <tr>
                    <th>PEN CODE</th>
                    <th>SPECIALIZATIONS</th>
                    <th colspan="3">UNITS</th>
                </tr>
                <tr>
                    <td colspan="2"></td>
                    <th >LEC</th>
                    <th >LAB</th>
                    <th >TOTAL</th>
                    
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                   
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                   
                </tr>
               
            </table>
        </div>
            <div class="buttons">
                <input type="text" id="specializationInput" placeholder="Enter Specialization" required>
                <button onclick="makeTableEditable()">Edit</button>
                <button onclick="saveTableData()">Save</button>
                <button onclick="printTable()">Print</button>
                </div>
            
                <script>
                
                        let setsCreated = 0;
                    
                        function makeTableEditable() {
                            const table = document.getElementById('editableTable');
                            const cells = table.querySelectorAll('td');
                            cells.forEach(cell => {
                                cell.contentEditable = true;
                            });
                        }
                    
                        function saveTableData() {
                            // Get the entered cyear value
                            const specializationInput = document.getElementById('specializationInput');
                            const specializationValue = specializationInput.value;
                    
                            // Check if cyear is entered
                            if (!specializationValue) {
                                alert('Please enter specialization before saving.');
                                return;
                            }
                    
                            // Clone the table element
                            const clonedTable = document.getElementById('editableTable').cloneNode(true);
                    
                            // Remove buttons and script from the clone
                            const buttons = clonedTable.querySelectorAll('.buttons');
                            buttons.forEach(button => button.remove());
                    
                            const scripts = clonedTable.querySelectorAll('script');
                            scripts.forEach(script => script.remove());
                    
                            // Get the edited content
                            const editedContent = clonedTable.outerHTML;
                    
                            // Send the data to the server
                            fetch('savespe.php', {
                                method: 'POST',
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded',
                                },
                                body: 'htmlContent=' + encodeURIComponent(editedContent) + '&track=' + encodeURIComponent(specializationValue),
                            })
                            .then(response => response.json())
                            .then(data => {
                                console.log('Data saved:', data);
                                // Optionally, you can handle the response or provide user feedback.
                                
                                // Reset the content of the cyear input
                                specializationInput.value = '';
                            })
                            .catch(error => {
                                console.error('Error saving data:', error);
                                // Handle errors or provide user feedback.
                            });
                        }
                    
                </script>
</body>
</html>