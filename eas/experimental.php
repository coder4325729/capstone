<?php include 'db_connect.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<style>
    table, th, td {
        border: 1px solid black;
    }
    .curriculum {
        margin-left: 30px;
        margin-right: 30px;
    }

    body {
        margin: 0;
        padding: 0;
    }
    .firsttable {
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .text {
        display: flex;
        justify-content: center;
    }
    .buttons {
        position: fixed;
        bottom: 20px; /* Adjust this value as needed */
        left: 0;
        right: 0;
        text-align: center;
    }
    .spacer {
        height: 100px;
    }

    .buttons button {
        margin: 0 5px; /* Adjust horizontal margin as needed */
        background-color: transparent;
        border: 1px solid #77d491;
        border-radius: 5px;
        cursor: pointer;
        transition: background-color 0.3s ease;
        display: inline-block; /* Ensure buttons align horizontally */
        vertical-align: middle; /* Align buttons vertically */
        background-color: greenyellow;
        padding: 10px 15px;
    }

    .buttons button:hover {
        background-color: #77d491;
        color: white;
    }
   
</style>

<body class class="curriculum" id="editableTable">
    <div class="text" id="curriculumTitle" >
        <h1>2021-2022</h1>
    </div>
    <div class="text">
        <h1>First Year</h1>
    </div>
<div class="tablewhole">
    <div class="firsttable">
        <table class="curriculum">
            <tr>
                <th colspan="7">First Semester</th>
            </tr>
            <tr>
                <th rowspan="2">Pen Code</th>
                <th rowspan="2">Descriptive Title</th>
                <th colspan="3">Units</th>
                <th rowspan="2">Pre-requisite</th>
                <th rowspan="2">Grade</th>
            </tr>
            <tr>
                <th>Lec</th>
                <th>Lab</th>
                <th>Total</th>
            </tr>
            <tr>
            <?php
					$i = 1;
					$qry = $conn->query("SELECT * FROM 11curriculum2022 order by 1pc asc, 1dt asc, 1lc asc, 1lb asc, 1to asc, 1pr asc, 1gr asc");
					while($row= $qry->fetch_assoc()):
					?>
                  <input type="hidden" name="1pc[]" value="<?php echo $row['1pc'] ?>">
            <td><?php echo $row['1pc'] ?></td>
            <td><?php echo $row['1dt'] ?></td>
            <td><?php echo $row['1lc'] ?></td>
            <td><?php echo $row['1lb'] ?></td>
            <td><?php echo $row['1to'] ?></td>
            <td><?php echo $row['1pr'] ?></td>
            <td><?php echo $row['1gr'] ?></td>
            </tr>
            <?php endwhile; ?>
          
        </table>
        <table class="curriculum">
            <tr>
                <th colspan="7">Second Semester</th>
            </tr>
            <tr>
                <th rowspan="2">Pen Code</th>
                <th rowspan="2">Descriptive Title</th>
                <th colspan="3">Units</th>
                <th rowspan="2">Pre-requisite</th>
                <th rowspan="2">Grade</th>
            </tr>
            <tr>
                <th>Lec</th>
                <th>Lab</th>
                <th>Total</th>
            </tr>
            <tr>
            <?php
					$i = 1;
					$qry = $conn->query("SELECT * FROM 12curriculum2022 order by 2pc asc, 2dt asc, 2lc asc, 2lb asc, 2to asc, 2pr asc, 2gr asc");
					while($row= $qry->fetch_assoc()):
					?>
            <td><?php echo $row['2pc'] ?></td>
            <td><?php echo $row['2dt'] ?></td>
            <td><?php echo $row['2lc'] ?></td>
            <td><?php echo $row['2lb'] ?></td>
            <td><?php echo $row['2to'] ?></td>
            <td><?php echo $row['2pr'] ?></td>
            <td><?php echo $row['2gr'] ?></td>
            </tr>
            <?php endwhile; ?>
        </table>
    </div>
    <div class="text">
        <h1>Second Year</h1>
    </div>

    <div class="firsttable">
        <table class="curriculum">
            <tr>
                <th colspan="7">First Semester</th>
            </tr>
            <tr>
                <th rowspan="2">Pen Code</th>
                <th rowspan="2">Descriptive Title</th>
                <th colspan="3">Units</th>
                <th rowspan="2">Pre-requisite</th>
                <th rowspan="2">Grade</th>
            </tr>
            <tr>
                <th>Lec</th>
                <th>Lab</th>
                <th>Total</th>
            </tr>
            <tr>
            <?php
					$i = 1;
					$qry = $conn->query("SELECT * FROM 21curriculum2022 order by 3pc asc, 3lc asc, 3dt asc, 3lb asc, 3to asc, 3pr asc, 3gr asc");
					while($row= $qry->fetch_assoc()):
					?>
            <td><?php echo $row['3pc'] ?></td>
            <td><?php echo $row['3dt'] ?></td>
            <td><?php echo $row['3lc'] ?></td>
            <td><?php echo $row['3lb'] ?></td>
            <td><?php echo $row['3to'] ?></td>
            <td><?php echo $row['3pr'] ?></td>
            <td><?php echo $row['3gr'] ?></td>
            </tr>
            <?php endwhile; ?>
        </table>
        <table class="curriculum">
            <tr>
                <th colspan="7">Second Semester</th>
            </tr>
            <tr>
                <th rowspan="2">Pen Code</th>
                <th rowspan="2">Descriptive Title</th>
                <th colspan="3">Units</th>
                <th rowspan="2">Pre-requisite</th>
                <th rowspan="2">Grade</th>
            </tr>
            <tr>
                <th>Lec</th>
                <th>Lab</th>
                <th>Total</th>
            </tr>
            <tr>
            <?php
					$i = 1;
					$qry = $conn->query("SELECT * FROM 22curriculum2022 order by  4pc asc, 4dt asc, 4lc asc, 4lb asc, 4to asc, 4pr asc, 4gr asc");
					while($row= $qry->fetch_assoc()):
					?>
            <td><?php echo $row['4pc'] ?></td>
            <td><?php echo $row['4dt'] ?></td>
            <td><?php echo $row['4lc'] ?></td>
            <td><?php echo $row['4lb'] ?></td>
            <td><?php echo $row['4to'] ?></td>
            <td><?php echo $row['4pr'] ?></td>
            <td><?php echo $row['4gr'] ?></td>
            </tr>
            <?php endwhile; ?>
        </table>
    </div>
    <div class="text">
        <h1>Third Year</h1>
    </div>

    <div class="firsttable">
        <table class="curriculum">
            <tr>
                <th colspan="7">First Semester</th>
            </tr>
            <tr>
                <th rowspan="2">Pen Code</th>
                <th rowspan="2">Descriptive Title</th>
                <th colspan="3">Units</th>
                <th rowspan="2">Pre-requisite</th>
                <th rowspan="2">Grade</th>
            </tr>
            <tr>
                <th>Lec</th>
                <th>Lab</th>
                <th>Total</th>
            </tr>
            <tr>
            <?php
					$i = 1;
					$qry = $conn->query("SELECT * FROM 31curriculum2022 order by 5pc asc, 5dt asc, 5lc asc, 5lb asc, 5to asc, 5pr asc, 5gr asc");
					while($row= $qry->fetch_assoc()):
					?>
            <td><?php echo $row['5pc'] ?></td>
            <td><?php echo $row['5dt'] ?></td>
            <td><?php echo $row['5lc'] ?></td>
            <td><?php echo $row['5lb'] ?></td>
            <td><?php echo $row['5to'] ?></td>
            <td><?php echo $row['5pr'] ?></td>
            <td><?php echo $row['5gr'] ?></td>
            </tr>
            <?php endwhile; ?>
        </table>
        <table class="curriculum">
            <tr>
                <th colspan="7">Second Semester</th>
            </tr>
            <tr>
                <th rowspan="2">Pen Code</th>
                <th rowspan="2">Descriptive Title</th>
                <th colspan="3">Units</th>
                <th rowspan="2">Pre-requisite</th>
                <th rowspan="2">Grade</th>
            </tr>
            <tr>
                <th>Lec</th>
                <th>Lab</th>
                <th>Total</th>
            </tr>
            <tr>
            <?php
					$i = 1;
					$qry = $conn->query("SELECT * FROM 32curriculum2022 order by 6pc asc, 6dt asc, 6lc asc, 6lb asc, 6to asc, 6pr asc, 6gr asc");
					while($row= $qry->fetch_assoc()):
					?>
            <td><?php echo $row['6pc'] ?></td>
            <td><?php echo $row['6dt'] ?></td>
            <td><?php echo $row['6lc'] ?></td>
            <td><?php echo $row['6lb'] ?></td>
            <td><?php echo $row['6to'] ?></td>
            <td><?php echo $row['6pr'] ?></td>
            <td><?php echo $row['6gr'] ?></td>
            </tr>
            <?php endwhile; ?>
        </table>
    </div>
    <div class="text">
        <h1>Fourth Year</h1>
    </div>

    <div class="firsttable">
        <table class="curriculum">
            <tr>
                <th colspan="7">First Semester</th>
            </tr>
            <tr>
                <th rowspan="2">Pen Code</th>
                <th rowspan="2">Descriptive Title</th>
                <th colspan="3">Units</th>
                <th rowspan="2">Pre-requisite</th>
                <th rowspan="2">Grade</th>
            </tr>
            <tr>
                <th>Lec</th>
                <th>Lab</th>
                <th>Total</th>
            </tr>
            <tr>
            <?php
					$i = 1;
					$qry = $conn->query("SELECT * FROM 41curriculum2022 order by 7pc asc, 7dt asc, 7lc asc, 7lb asc, 7to asc, 7pr asc, 7gr asc");
					while($row= $qry->fetch_assoc()):
					?>
            <td><?php echo $row['7pc'] ?></td>
            <td><?php echo $row['7dt'] ?></td>
            <td><?php echo $row['7lc'] ?></td>
            <td><?php echo $row['7lb'] ?></td>
            <td><?php echo $row['7to'] ?></td>
            <td><?php echo $row['7pr'] ?></td>
            <td><?php echo $row['7gr'] ?></td>
            </tr>
            <?php endwhile; ?>
        </table>
        <table class="curriculum">
            <tr>
                <th colspan="7">Second Semester</th>
            </tr>
            <tr>
                <th rowspan="2">Pen Code</th>
                <th rowspan="2">Descriptive Title</th>
                <th colspan="3">Units</th>
                <th rowspan="2">Pre-requisite</th>
                <th rowspan="2">Grade</th>
            </tr>
            <tr>
                <th>Lec</th>
                <th>Lab</th>
                <th>Total</th>
            </tr>
            <tr>
            <?php
					$i = 1;
					$qry = $conn->query("SELECT * FROM 42curriculum2022 order by 8pc asc, 8dt asc, 8lc asc, 8lb asc, 8to asc, 8pr asc, 8gr asc");
					while($row= $qry->fetch_assoc()):
					?>
            <td><?php echo $row['8pc'] ?></td>
            <td><?php echo $row['8dt'] ?></td>
            <td><?php echo $row['8lc'] ?></td>
            <td><?php echo $row['8lb'] ?></td>
            <td><?php echo $row['8to'] ?></td>
            <td><?php echo $row['8pr'] ?></td>
            <td><?php echo $row['8gr'] ?></td>
            </tr>
            <?php endwhile; ?>
        </table>
    </div>
</div>
<div class="spacer"></div>
<div class="text" id="curriculumTitle" >
        <h1>2022-2023</h1>
    </div>
    <div class="text">
        <h1>First Year</h1>
    </div>
<div class="tablewhole">
    <div class="firsttable">
        <table class="curriculum">
            <tr>
                <th colspan="7">First Semester</th>
            </tr>
            <tr>
                <th rowspan="2">Pen Code</th>
                <th rowspan="2">Descriptive Title</th>
                <th colspan="3">Units</th>
                <th rowspan="2">Pre-requisite</th>
                <th rowspan="2">Grade</th>
            </tr>
            <tr>
                <th>Lec</th>
                <th>Lab</th>
                <th>Total</th>
            </tr>
            <tr>
            <?php
					$i = 1;
					$qry = $conn->query("SELECT * FROM 11curriculum2023 order by 1pc1 asc, 1dt1 asc, 1lc1 asc, 1lb1 asc, 1to1 asc, 1pr1 asc, 1gr1 asc");
					while($row= $qry->fetch_assoc()):
					?>
            <td><?php echo $row['1pc1'] ?></td>
            <td><?php echo $row['1dt1'] ?></td>
            <td><?php echo $row['1lc1'] ?></td>
            <td><?php echo $row['1lb1'] ?></td>
            <td><?php echo $row['1to1'] ?></td>
            <td><?php echo $row['1pr1'] ?></td>
            <td><?php echo $row['1gr1'] ?></td>
            </tr>
            <?php endwhile; ?>
        </table>
        <table class="curriculum">
            <tr>
                <th colspan="7">Second Semester</th>
            </tr>
            <tr>
                <th rowspan="2">Pen Code</th>
                <th rowspan="2">Descriptive Title</th>
                <th colspan="3">Units</th>
                <th rowspan="2">Pre-requisite</th>
                <th rowspan="2">Grade</th>
            </tr>
            <tr>
                <th>Lec</th>
                <th>Lab</th>
                <th>Total</th>
            </tr>
            <tr>
            <?php
					$i = 1;
					$qry = $conn->query("SELECT * FROM 12curriculum2023 order by 2pc1 asc, 2dt1 asc, 2lc1 asc, 2lb1 asc, 2to1 asc, 2pr1 asc, 2gr1 asc");
					while($row= $qry->fetch_assoc()):
					?>
            <td><?php echo $row['2pc1'] ?></td>
            <td><?php echo $row['2dt1'] ?></td>
            <td><?php echo $row['2lc1'] ?></td>
            <td><?php echo $row['2lb1'] ?></td>
            <td><?php echo $row['2to1'] ?></td>
            <td><?php echo $row['2pr1'] ?></td>
            <td><?php echo $row['2gr1'] ?></td>
            </tr>
            <?php endwhile; ?>
        </table>
    </div>
    <div class="text">
        <h1>Second Year</h1>
    </div>

    <div class="firsttable">
        <table class="curriculum">
            <tr>
                <th colspan="7">First Semester</th>
            </tr>
            <tr>
                <th rowspan="2">Pen Code</th>
                <th rowspan="2">Descriptive Title</th>
                <th colspan="3">Units</th>
                <th rowspan="2">Pre-requisite</th>
                <th rowspan="2">Grade</th>
            </tr>
            <tr>
                <th>Lec</th>
                <th>Lab</th>
                <th>Total</th>
            </tr>
            <tr>
            <?php
					$i = 1;
					$qry = $conn->query("SELECT * FROM 21curriculum2023 order by 3pc1 asc, 3lc1 asc, 3dt1 asc, 3lb1 asc, 3to1 asc, 3pr1 asc, 3gr1 asc");
					while($row= $qry->fetch_assoc()):
					?>
            <td><?php echo $row['3pc1'] ?></td>
            <td><?php echo $row['3dt1'] ?></td>
            <td><?php echo $row['3lc1'] ?></td>
            <td><?php echo $row['3lb1'] ?></td>
            <td><?php echo $row['3to1'] ?></td>
            <td><?php echo $row['3pr1'] ?></td>
            <td><?php echo $row['3gr1'] ?></td>
            </tr>
            <?php endwhile; ?>
        </table>
        <table class="curriculum">
            <tr>
                <th colspan="7">Second Semester</th>
            </tr>
            <tr>
                <th rowspan="2">Pen Code</th>
                <th rowspan="2">Descriptive Title</th>
                <th colspan="3">Units</th>
                <th rowspan="2">Pre-requisite</th>
                <th rowspan="2">Grade</th>
            </tr>
            <tr>
                <th>Lec</th>
                <th>Lab</th>
                <th>Total</th>
            </tr>
            <tr>
            <?php
					$i = 1;
					$qry = $conn->query("SELECT * FROM 22curriculum2023 order by  4pc1 asc, 4dt1 asc, 4lc1 asc, 4lb1 asc, 4to1 asc, 4pr1 asc, 4gr1 asc");
					while($row= $qry->fetch_assoc()):
					?>
            <td><?php echo $row['4pc1'] ?></td>
            <td><?php echo $row['4dt1'] ?></td>
            <td><?php echo $row['4lc1'] ?></td>
            <td><?php echo $row['4lb1'] ?></td>
            <td><?php echo $row['4to1'] ?></td>
            <td><?php echo $row['4pr1'] ?></td>
            <td><?php echo $row['4gr1'] ?></td>
            </tr>
            <?php endwhile; ?>
        </table>
    </div>
    <div class="text">
        <h1>Third Year</h1>
    </div>

    <div class="firsttable">
        <table class="curriculum">
            <tr>
                <th colspan="7">First Semester</th>
            </tr>
            <tr>
                <th rowspan="2">Pen Code</th>
                <th rowspan="2">Descriptive Title</th>
                <th colspan="3">Units</th>
                <th rowspan="2">Pre-requisite</th>
                <th rowspan="2">Grade</th>
            </tr>
            <tr>
                <th>Lec</th>
                <th>Lab</th>
                <th>Total</th>
            </tr>
            <tr>
            <?php
					$i = 1;
					$qry = $conn->query("SELECT * FROM 31curriculum2023 order by 5pc1 asc, 5dt1 asc, 5lc1 asc, 5lb1 asc, 5to1 asc, 5pr1 asc, 5gr1 asc");
					while($row= $qry->fetch_assoc()):
					?>
            <td><?php echo $row['5pc1'] ?></td>
            <td><?php echo $row['5dt1'] ?></td>
            <td><?php echo $row['5lc1'] ?></td>
            <td><?php echo $row['5lb1'] ?></td>
            <td><?php echo $row['5to1'] ?></td>
            <td><?php echo $row['5pr1'] ?></td>
            <td><?php echo $row['5gr1'] ?></td>
            </tr>
            <?php endwhile; ?>
        </table>
        <table class="curriculum">
            <tr>
                <th colspan="7">Second Semester</th>
            </tr>
            <tr>
                <th rowspan="2">Pen Code</th>
                <th rowspan="2">Descriptive Title</th>
                <th colspan="3">Units</th>
                <th rowspan="2">Pre-requisite</th>
                <th rowspan="2">Grade</th>
            </tr>
            <tr>
                <th>Lec</th>
                <th>Lab</th>
                <th>Total</th>
            </tr>
            <tr>
            <?php
					$i = 1;
					$qry = $conn->query("SELECT * FROM 32curriculum2023 order by 6pc1 asc, 6dt1 asc, 6lc1 asc, 6lb1 asc, 6to1 asc, 6pr1 asc, 6gr1 asc");
					while($row= $qry->fetch_assoc()):
					?>
            <td><?php echo $row['6pc1'] ?></td>
            <td><?php echo $row['6dt1'] ?></td>
            <td><?php echo $row['6lc1'] ?></td>
            <td><?php echo $row['6lb1'] ?></td>
            <td><?php echo $row['6to1'] ?></td>
            <td><?php echo $row['6pr1'] ?></td>
            <td><?php echo $row['6gr1'] ?></td>
            </tr>
            <?php endwhile; ?>
        </table>
    </div>
    <div class="text">
        <h1>Fourth Year</h1>
    </div>

    <div class="firsttable">
        <table class="curriculum">
            <tr>
                <th colspan="7">First Semester</th>
            </tr>
            <tr>
                <th rowspan="2">Pen Code</th>
                <th rowspan="2">Descriptive Title</th>
                <th colspan="3">Units</th>
                <th rowspan="2">Pre-requisite</th>
                <th rowspan="2">Grade</th>
            </tr>
            <tr>
                <th>Lec</th>
                <th>Lab</th>
                <th>Total</th>
            </tr>
            <tr>
            <?php
					$i = 1;
					$qry = $conn->query("SELECT * FROM 41curriculum2023 order by 7pc1 asc, 7dt1 asc, 7lc1 asc, 7lb1 asc, 7to1 asc, 7pr1 asc, 7gr1 asc");
					while($row= $qry->fetch_assoc()):
					?>
            <td><?php echo $row['7pc1'] ?></td>
            <td><?php echo $row['7dt1'] ?></td>
            <td><?php echo $row['7lc1'] ?></td>
            <td><?php echo $row['7lb1'] ?></td>
            <td><?php echo $row['7to1'] ?></td>
            <td><?php echo $row['7pr1'] ?></td>
            <td><?php echo $row['7gr1'] ?></td>
            </tr>
            <?php endwhile; ?>
        </table>
        
        <table class="curriculum">
            <tr>
                <th colspan="7">Second Semester</th>
            </tr>
            <tr>
                <th rowspan="2">Pen Code</th>
                <th rowspan="2">Descriptive Title</th>
                <th colspan="3">Units</th>
                <th rowspan="2">Pre-requisite</th>
                <th rowspan="2">Grade</th>
            </tr>
            <tr>
                <th>Lec</th>
                <th>Lab</th>
                <th>Total</th>
            </tr>
            <tr>
            <?php
					$i = 1;
					$qry = $conn->query("SELECT * FROM 42curriculum2023 order by 8pc1 asc, 8dt1 asc, 8lc1 asc, 8lb1 asc, 8to1 asc, 8pr1 asc, 8gr1 asc");
					while($row= $qry->fetch_assoc()):
					?>
            <td><?php echo $row['8pc1'] ?></td>
            <td><?php echo $row['8dt1'] ?></td>
            <td><?php echo $row['8lc1'] ?></td>
            <td><?php echo $row['8lb1'] ?></td>
            <td><?php echo $row['8to1'] ?></td>
            <td><?php echo $row['8pr1'] ?></td>
            <td><?php echo $row['8gr1'] ?></td>
            </tr>
            <?php endwhile; ?>
        </table>
        </div>
        <div class="firsttable">
        <table class="curriculum">
            <tr>
                <th colspan="7">Business Informatics</th>
            </tr>
            <tr>
                <th rowspan="2">Pen Code</th>
                <th rowspan="2">Descriptive Title</th>
                <th colspan="3">Units</th>
                <th rowspan="2">Pre-requisite</th>
                <th rowspan="2">Grade</th>
            </tr>
            <tr>
                <th>Lec</th>
                <th>Lab</th>
                <th>Total</th>
            </tr>
            <tr>
            <?php
					$i = 1;
					$qry = $conn->query("SELECT * FROM busin order by 9pc1 asc, 9dt1 asc, 9lc1 asc, 9lb1 asc, 9to1 asc, 9pr1 asc, 9gr1 asc");
					while($row= $qry->fetch_assoc()):
					?>
            <td><?php echo $row['9pc1'] ?></td>
            <td><?php echo $row['9dt1'] ?></td>
            <td><?php echo $row['9lc1'] ?></td>
            <td><?php echo $row['9lb1'] ?></td>
            <td><?php echo $row['9to1'] ?></td>
            <td><?php echo $row['9pr1'] ?></td>
            <td><?php echo $row['9gr1'] ?></td>
            </tr>
            <?php endwhile; ?>
            </table>
                    
            <div class="firsttable">
        <table class="curriculum">
            <tr>
                <th colspan="7">Computer Security</th>
            </tr>
            <tr>
                <th rowspan="2">Pen Code</th>
                <th rowspan="2">Descriptive Title</th>
                <th colspan="3">Units</th>
                <th rowspan="2">Pre-requisite</th>
                <th rowspan="2">Grade</th>
            </tr>
            <tr>
                <th>Lec</th>
                <th>Lab</th>
                <th>Total</th>
            </tr>
            <tr>
            <?php
					$i = 1;
					$qry = $conn->query("SELECT * FROM comse order by 10pc1 asc, 10dt1 asc, 10lc1 asc, 10lb1 asc, 10to1 asc, 10pr1 asc, 10gr1 asc");
					while($row= $qry->fetch_assoc()):
					?>
            <td><?php echo $row['10pc1'] ?></td>
            <td><?php echo $row['10dt1'] ?></td>
            <td><?php echo $row['10lc1'] ?></td>
            <td><?php echo $row['10lb1'] ?></td>
            <td><?php echo $row['10to1'] ?></td>
            <td><?php echo $row['10pr1'] ?></td>
            <td><?php echo $row['10gr1'] ?></td>
            </tr>
            <?php endwhile; ?>
            </table>
            </div>
            <div class="firsttable">
        <table class="curriculum">
            <tr>
                <th colspan="7">Systems Development</th>
            </tr>
            <tr>
                <th rowspan="2">Pen Code</th>
                <th rowspan="2">Descriptive Title</th>
                <th colspan="3">Units</th>
                <th rowspan="2">Pre-requisite</th>
                <th rowspan="2">Grade</th>
            </tr>
            <tr>
                <th>Lec</th>
                <th>Lab</th>
                <th>Total</th>
            </tr>
            <tr>
            <?php
					$i = 1;
					$qry = $conn->query("SELECT * FROM sysdev order by 11pc1 asc, 11dt1 asc, 11lc1 asc, 11lb1 asc, 11to1 asc, 11pr1 asc, 11gr1 asc");
					while($row= $qry->fetch_assoc()):
					?>
            <td><?php echo $row['11pc1'] ?></td>
            <td><?php echo $row['11dt1'] ?></td>
            <td><?php echo $row['11lc1'] ?></td>
            <td><?php echo $row['11lb1'] ?></td>
            <td><?php echo $row['11to1'] ?></td>
            <td><?php echo $row['11pr1'] ?></td>
            <td><?php echo $row['11gr1'] ?></td>
            </tr>
            <?php endwhile; ?>
            
            </table>
            
            <div class="firsttable">
        <table class="curriculum">
            <tr>
                <th colspan="7">Digital Arts</th>
            </tr>
            <tr>
                <th rowspan="2">Pen Code</th>
                <th rowspan="2">Descriptive Title</th>
                <th colspan="3">Units</th>
                <th rowspan="2">Pre-requisite</th>
                <th rowspan="2">Grade</th>
            </tr>
            <tr>
                <th>Lec</th>
                <th>Lab</th>
                <th>Total</th>
            </tr>
            <tr>
            <?php
					$i = 1;
					$qry = $conn->query("SELECT * FROM digar order by 12pc1 asc, 12dt1 asc, 12lc1 asc, 12lb1 asc, 12to1 asc, 12pr1 asc, 12gr1 asc");
					while($row= $qry->fetch_assoc()):
					?>
           <td><?php echo isset($pc1_12) ? $pc1_12: $row['12pc1']; ?></td>
            <td><?php echo $row['12dt1'] ?></td>
            <td><?php echo $row['12lc1'] ?></td>
            <td><?php echo $row['12lb1'] ?></td>
            <td><?php echo $row['12to1'] ?></td>
            <td><?php echo $row['12pr1'] ?></td>
            <td><?php echo $row['12gr1'] ?></td>
            </tr>
            <?php endwhile; ?>
        </table>
        
    </div>
</div>
<div class="buttons">
        <button onclick="makeTableEditable()">Edit</button>
        <button onclick="saveData()">Save</button>
        <button onclick="printPage()">Print</button>
        
    </div>
</body>
<script>
     let setsCreated = 0;

function makeTableEditable() {
    const table = document.getElementById('editableTable');
    const cells = table.querySelectorAll('td');
    cells.forEach(cell => {
        cell.contentEditable = true;
    });
}
function synchronizeColumnWidths() {
// Get all tables with class 'curriculum'
const tables = document.querySelectorAll('.curriculum');

// Initialize an array to store the maximum width of each column
const maxWidths = Array.from({ length: tables[0].rows[0].cells.length }, () => 0);

// Loop through each table to find the maximum width of each column
tables.forEach(table => {
    table.rows.forEach(row => {
        row.cells.forEach((cell, index) => {
            maxWidths[index] = Math.max(maxWidths[index], cell.offsetWidth);
        });
    });
});

// Set the width of each cell in each table to the maximum width of the corresponding column
tables.forEach(table => {
    table.rows.forEach(row => {
        row.cells.forEach((cell, index) => {
            cell.style.width = maxWidths[index] + 'px';
        });
    });
});
}

</script>
</html>
