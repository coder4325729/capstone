<?php
// Include database connection
include 'db_connect.php';

// Check if the student ID is provided
if (isset($_GET['studentId'])) {
    // Retrieve the student ID from the GET parameters
    $studentId = $_GET['studentId'];

    // Initialize an array to store the grades
    $grades = array();

    // Query to retrieve the grades for the specified student ID
    $query = "SELECT "
             . "A.PenCode AS Pencode, "
             . "A.Description AS Description, "
             . "A.Lab AS Lab, "
             . "A.Lec AS Lec, "
             . "A.SubjectID AS subjectID, "
             . "A.Prerequisite AS Prerequisite, "
             . "studentcurriculumsubject.Grade AS Grade, "
             . "studentcurriculumsubject.Status AS Status, "
             . "studentcurriculumsubject.StudentCurriculumSubjectID AS StudentCurriculumSubjectID, "
             . "B.Pencode AS Postrequisite "
             . "FROM subjects A "         
             . "JOIN studentcurriculumsubject ON A.SubjectID = studentcurriculumsubject.SubjectID "
             . "LEFT OUTER JOIN subjects B ON A.PenCode = B.Prerequisite "
             . "WHERE studentcurriculumsubject.StudentID = ?";

    // Prepare the statement
    $stmt = $conn->prepare($query);

    // Bind the student ID parameter
    $stmt->bind_param("i", $studentId);

    // Execute the query
    $stmt->execute();

    // Bind result variables
    $stmt->bind_result($pencode, $description, $lab, $lec, $subjectId, $prerequisite, $grade, $status, $studentCurriculumSubjectID, $postrequisite);

    // Fetch results and store them in the grades array
    while ($stmt->fetch()) {
        // Store the fetched data in the grades array
        $grades[] = array(
            'Pencode' => $pencode,
            'Description' => $description,
            'Lab' => $lab,
            'Lec' => $lec,
            'SubjectID' => $subjectId,
            'Prerequisite' => $prerequisite,
            'Grade' => $grade,
            'Status' => $status,
            'StudentCurriculumSubjectID' => $studentCurriculumSubjectID,
            'Postrequisite' => $postrequisite
        );
    }

    // Close the statement
    $stmt->close();

    // Close the database connection
    $conn->close();

    // Return the grades array as JSON
    echo json_encode($grades);
} else {
    // Return an error message if the student ID is not provided
    echo json_encode(array('error' => 'Student ID not provided'));
}
?>
