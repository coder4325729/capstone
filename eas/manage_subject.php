<?php
include 'db_connect.php';
if(isset($_GET['SubjectID'])){
	$qry = $conn->query("SELECT * FROM subjects where SubjectID ={$_GET['SubjectID']}")->fetch_array();
	foreach($qry as $k => $v){
		$$k = $v;
	}
}
?>
<style>
    /* Style for suggestion list */
    .suggestion {
        padding: 5px;
        cursor: pointer;
    }

    /* Style for suggestion when hovered */
    .suggestion:hover {
        background-color: #f0f0f0;
    }
</style>
<div class="container-fluid">
	<form action="" id="manage-subject">
		<input type="hidden" name="SubjectID" value="<?php echo isset($SubjectID) ? $SubjectID : '' ?>">
		<div id="msg" class="form-group"></div>
		
		<div class="form-group">
			<label for="subject" class="control-label">Pen Code</label>
			<input type="text" class="form-control form-control-sm" name="Pencode" id="Pencode" value="<?php echo isset($Pencode) ? $Pencode : '' ?>">
		</div>
		<div class="form-group">
			<label for="description" class="control-label">Description</label>
			<textarea name="Description" id="Description" cols="30" rows="4" class="form-control"><?php echo isset($Description) ? $Description : '' ?></textarea>
		</div>
		
		<div class="form-group">
			<label for="Lec" class="control-label">Lec</label>
			<input type="int" class="form-control form-control-sm" name="Lec" id="Lec" value="<?php echo isset($Lec) ? $Lec : '' ?>">
		</div>
		<div class="form-group">
			<label for="Lab" class="control-label">Lab</label>
			<input type="int" class="form-control form-control-sm" name="Lab" id="Lab" value="<?php echo isset($Lab) ? $Lab : '' ?>">
		</div>
        <div class="form-group">
    <label for="Prerequisite" class="control-label">Pre-requisite</label>
    <input type="text" class="form-control form-control-sm" name="Prerequisite" id="Prerequisite" value="<?php echo isset($Prerequisite) ? $Prerequisite : '' ?>" placeholder="Example: ITE 310">
    <div id="suggestion-list"></div> <!-- Container for suggestion list -->
</div>
	</form>
</div>
<div class="modal fade" id="successModal" tabindex="-1" aria-labelledby="successModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="successModalLabel">Success</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                Data successfully saved.
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#manage-subject').submit(function (e) {
            e.preventDefault();
            start_load();
            $.ajax({
                url: 'process6.php',
                method: 'POST',
                data: $(this).serialize(),
                success: function (resp) {
					console.log(resp);
                    if (resp == 1) {
                        // Data successfully updated
                        $('#successModal').modal('show');
                        setTimeout(function () {
                            $('#successModal').modal('hide');
                            location.reload(); // Reload the page after 2 seconds
                        }, 1000);
                    } else {
                        // Error in updating data
                        alert('Error updating record. Please try again.');
                    }
                    end_load();
                }
            });
        });
    });
    var textInputs = document.querySelectorAll('input[type="text"], textarea');

// Iterate through each input field and add event listener
textInputs.forEach(function(input) {
    input.addEventListener('input', function(event) {
        // Convert the entered text to uppercase first letter of each word
        let words = this.value.toLowerCase().split(' ');
        for (let i = 0; i < words.length; i++) {
            words[i] = words[i].charAt(0).toUpperCase() + words[i].slice(1);
        }
        this.value = words.join(' ');
    });
});
var textInput = document.querySelectorAll('input[type="text"]');

// Iterate through each input field and add event listener
textInput.forEach(function(input) {
    input.addEventListener('input', function(event) {
        // Convert the entered text to uppercase
        this.value = this.value.toUpperCase();
    });
});
$('#Prerequisite').keyup(function() {
            var query = $(this).val();
            if(query.length > 0) {
                $.ajax({
                    url: 'get_suggestions.php', // Path to your PHP script
                    method: 'POST',
                    data: {query: query},
                    success: function(data) {
                        $('#suggestion-list').html(data);
                    }
                });
            } else {
                $('#suggestion-list').html('');
            }
        });

        // Autocomplete functionality
        $(document).on('click', '.suggestion', function() {
            $('#Prerequisite').val($(this).text());
            $('#suggestion-list').html('');
        });
</script>