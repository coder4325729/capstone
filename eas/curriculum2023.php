<?php include 'db_connect.php'; ?>
<?php
// Check if the student ID is provided in the URL
if (isset($_GET['student_code'])) {
    // Retrieve the student ID from the URL
    $student_code = $_GET['student_code'];
} else {
    // Handle the case when the student ID is not provided in the URL
    echo "Student ID not provided.";
    exit; // Exit PHP execution
}

// Check if the form is submitted
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Retrieve the HTML content from the POST data
    $tableHtml = $_POST['tableHtml'];

    // Update the content column in the students table for the specified student_code
    $updateQuery = "UPDATE students SET content = ? WHERE student_code = ?";
    $stmt = $conn->prepare($updateQuery);
    $stmt->bind_param('ss', $tableHtml, $student_code);
    if ($stmt->execute()) {
        echo "Data saved successfully!";
    } else {
        echo "Failed to save data.";
    }
    exit; // Exit PHP execution after saving data
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<style>
    table, th, td {
        border: 1px solid black;
    }
    .curriculum {
        margin-left: 30px;
        margin-right: 30px;
    }

    body {
        margin: 0;
        padding: 0;
    }
    .firsttable {
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .text {
        display: flex;
        justify-content: center;
    }
    .buttons {
        position: fixed;
        bottom: 20px; /* Adjust this value as needed */
        left: 0;
        right: 0;
        text-align: center;
    }

    .buttons button {
        margin: 0 5px; /* Adjust horizontal margin as needed */
        background-color: transparent;
        border: 1px solid #77d491;
        border-radius: 5px;
        cursor: pointer;
        transition: background-color 0.3s ease;
        display: inline-block; /* Ensure buttons align horizontally */
        vertical-align: middle; /* Align buttons vertically */
        background-color: greenyellow;
        padding: 10px 15px;
    }

    .buttons button:hover {
        background-color: #77d491;
        color: white;
    }
   
    .highlight-green {
            background-color: lightgreen;
        }
        .highlight-red {
            background-color: salmon;
        }
</style>

<body class class="curriculum" id="editableTable">



    <div class="text" id="curriculumTitle"s>
        <h1>2021-2022</h1>
    </div>
    <div class="text">
        <h1>First Year</h1>
    </div>
<div class="tablewhole">
    <div class="firsttable">
        <table class="curriculum">
            <tr>
                <th colspan="7">First Semester</th>
            </tr>
            <tr>
                <th rowspan="2">Pen Code</th>
                <th rowspan="2">Descriptive Title</th>
                <th colspan="3">Units</th>
                <th rowspan="2">Pre-requisite</th>
                <th rowspan="2">Grade</th>
            </tr>
            <tr>
                <th>Lec</th>
                <th>Lab</th>
                <th>Total</th>
            </tr>
            <tr>
            <?php
					$i = 1;
					$qry = $conn->query("SELECT * FROM 11curriculum2022 order by 1pc asc, 1dt asc, 1lc asc, 1lb asc, 1to asc, 1pr asc, 1gr asc");
					while($row= $qry->fetch_assoc()):    
					?>
                    
                    <td> <?php
        // Retrieve the corresponding 2pc data from 12curriculum2022
        $qry_2pc = $conn->query("SELECT 2pc FROM 12curriculum2022 WHERE 1");
        $row_2pc = $qry_2pc->fetch_assoc();

        // Get the values for comparison
        $value_1pc = $row['1pc'];
        $value_2pc = $row_2pc['2pc'];

        // Check if the values are the same and apply highlighting
        $highlight_class = ($value_1pc == $value_2pc) ? 'highlight-green' : 'highlight-red';
        ?>
       
        <span class="<?php echo $highlight_class; ?>"><?php echo $value_1pc; ?></span>
    </td>
    <td><span class="hidden"><?php echo $row['1dt'] ?></span></td>
    <td><span class="hidden"><?php echo $row['1lc'] ?></span></td>
    <td><span class="hidden"><?php echo $row['1lb'] ?></span></td>
    <td><span class="hidden"><?php echo $row['1to'] ?></span></td>
    <td><span class="hidden"><?php echo $row['1pr'] ?></span></td>
    <td><span class="hidden"><?php echo $row['1gr'] ?></span></td>
            </tr>
            <?php endwhile; ?>
        </table>
        <table class="curriculum">
            <tr>
                <th colspan="7">Second Semester</th>
            </tr>
            <tr>
                <th rowspan="2">Pen Code</th>
                <th rowspan="2">Descriptive Title</th>
                <th colspan="3">Units</th>
                <th rowspan="2">Pre-requisite</th>
                <th rowspan="2">Grade</th>
            </tr>
            <tr>
                <th>Lec</th>
                <th>Lab</th>
                <th>Total</th>
            </tr>
            <tr>
            <?php
					$i = 1;
					$qry = $conn->query("SELECT * FROM 12curriculum2022 order by 2pc asc, 2dt asc, 2lc asc, 2lb asc, 2to asc, 2pr asc, 2gr asc");
					while($row= $qry->fetch_assoc()):
					?>
            <td><?php echo $row['2pc'] ?></td>
            <td><?php echo $row['2dt'] ?></td>
            <td><?php echo $row['2lc'] ?></td>
            <td><?php echo $row['2lb'] ?></td>
            <td><?php echo $row['2to'] ?></td>
            <td><?php echo $row['2pr'] ?></td>
            <td><?php echo $row['2gr'] ?></td>
            </tr>
            <?php endwhile; ?>
        </table>
    </div>
    <div class="text">
        <h1>Second Year</h1>
    </div>

    <div class="firsttable">
        <table class="curriculum">
            <tr>
                <th colspan="7">First Semester</th>
            </tr>
            <tr>
                <th rowspan="2">Pen Code</th>
                <th rowspan="2">Descriptive Title</th>
                <th colspan="3">Units</th>
                <th rowspan="2">Pre-requisite</th>
                <th rowspan="2">Grade</th>
            </tr>
            <tr>
                <th>Lec</th>
                <th>Lab</th>
                <th>Total</th>
            </tr>
            <tr>
            <?php
					$i = 1;
					$qry = $conn->query("SELECT * FROM 21curriculum2022 order by 3pc asc, 3lc asc, 3dt asc, 3lb asc, 3to asc, 3pr asc, 3gr asc");
					while($row= $qry->fetch_assoc()):
					?>
            <td><?php echo $row['3pc'] ?></td>
            <td><?php echo $row['3dt'] ?></td>
            <td><?php echo $row['3lc'] ?></td>
            <td><?php echo $row['3lb'] ?></td>
            <td><?php echo $row['3to'] ?></td>
            <td><?php echo $row['3pr'] ?></td>
            <td><?php echo $row['3gr'] ?></td>
            </tr>
            <?php endwhile; ?>
        </table>
        <table class="curriculum">
            <tr>
                <th colspan="7">Second Semester</th>
            </tr>
            <tr>
                <th rowspan="2">Pen Code</th>
                <th rowspan="2">Descriptive Title</th>
                <th colspan="3">Units</th>
                <th rowspan="2">Pre-requisite</th>
                <th rowspan="2">Grade</th>
            </tr>
            <tr>
                <th>Lec</th>
                <th>Lab</th>
                <th>Total</th>
            </tr>
            <tr>
            <?php
					$i = 1;
					$qry = $conn->query("SELECT * FROM 22curriculum2022 order by  4pc asc, 4dt asc, 4lc asc, 4lb asc, 4to asc, 4pr asc, 4gr asc");
					while($row= $qry->fetch_assoc()):
					?>
            <td><?php echo $row['4pc'] ?></td>
            <td><?php echo $row['4dt'] ?></td>
            <td><?php echo $row['4lc'] ?></td>
            <td><?php echo $row['4lb'] ?></td>
            <td><?php echo $row['4to'] ?></td>
            <td><?php echo $row['4pr'] ?></td>
            <td><?php echo $row['4gr'] ?></td>
            </tr>
            <?php endwhile; ?>
        </table>
    </div>
    <div class="text">
        <h1>Third Year</h1>
    </div>

    <div class="firsttable">
        <table class="curriculum">
            <tr>
                <th colspan="7">First Semester</th>
            </tr>
            <tr>
                <th rowspan="2">Pen Code</th>
                <th rowspan="2">Descriptive Title</th>
                <th colspan="3">Units</th>
                <th rowspan="2">Pre-requisite</th>
                <th rowspan="2">Grade</th>
            </tr>
            <tr>
                <th>Lec</th>
                <th>Lab</th>
                <th>Total</th>
            </tr>
            <tr>
            <?php
					$i = 1;
					$qry = $conn->query("SELECT * FROM 31curriculum2022 order by 5pc asc, 5dt asc, 5lc asc, 5lb asc, 5to asc, 5pr asc, 5gr asc");
					while($row= $qry->fetch_assoc()):
					?>
            <td><?php echo $row['5pc'] ?></td>
            <td><?php echo $row['5dt'] ?></td>
            <td><?php echo $row['5lc'] ?></td>
            <td><?php echo $row['5lb'] ?></td>
            <td><?php echo $row['5to'] ?></td>
            <td><?php echo $row['5pr'] ?></td>
            <td><?php echo $row['5gr'] ?></td>
            </tr>
            <?php endwhile; ?>
        </table>
        <table class="curriculum">
            <tr>
                <th colspan="7">Second Semester</th>
            </tr>
            <tr>
                <th rowspan="2">Pen Code</th>
                <th rowspan="2">Descriptive Title</th>
                <th colspan="3">Units</th>
                <th rowspan="2">Pre-requisite</th>
                <th rowspan="2">Grade</th>
            </tr>
            <tr>
                <th>Lec</th>
                <th>Lab</th>
                <th>Total</th>
            </tr>
            <tr>
            <?php
					$i = 1;
					$qry = $conn->query("SELECT * FROM 32curriculum2022 order by 6pc asc, 6dt asc, 6lc asc, 6lb asc, 6to asc, 6pr asc, 6gr asc");
					while($row= $qry->fetch_assoc()):
					?>
            <td><?php echo $row['6pc'] ?></td>
            <td><?php echo $row['6dt'] ?></td>
            <td><?php echo $row['6lc'] ?></td>
            <td><?php echo $row['6lb'] ?></td>
            <td><?php echo $row['6to'] ?></td>
            <td><?php echo $row['6pr'] ?></td>
            <td><?php echo $row['6gr'] ?></td>
            </tr>
            <?php endwhile; ?>
        </table>
    </div>
    <div class="text">
        <h1>Fourth Year</h1>
    </div>

    <div class="firsttable">
        <table class="curriculum">
            <tr>
                <th colspan="7">First Semester</th>
            </tr>
            <tr>
                <th rowspan="2">Pen Code</th>
                <th rowspan="2">Descriptive Title</th>
                <th colspan="3">Units</th>
                <th rowspan="2">Pre-requisite</th>
                <th rowspan="2">Grade</th>
            </tr>
            <tr>
                <th>Lec</th>
                <th>Lab</th>
                <th>Total</th>
            </tr>
            <tr>
            <?php
					$i = 1;
					$qry = $conn->query("SELECT * FROM 41curriculum2022 order by 7pc asc, 7dt asc, 7lc asc, 7lb asc, 7to asc, 7pr asc, 7gr asc");
					while($row= $qry->fetch_assoc()):
					?>
            <td><?php echo $row['7pc'] ?></td>
            <td><?php echo $row['7dt'] ?></td>
            <td><?php echo $row['7lc'] ?></td>
            <td><?php echo $row['7lb'] ?></td>
            <td><?php echo $row['7to'] ?></td>
            <td><?php echo $row['7pr'] ?></td>
            <td><?php echo $row['7gr'] ?></td>
            </tr>
            <?php endwhile; ?>
        </table>
        <table class="curriculum">
            <tr>
                <th colspan="7">Second Semester</th>
            </tr>
            <tr>
                <th rowspan="2">Pen Code</th>
                <th rowspan="2">Descriptive Title</th>
                <th colspan="3">Units</th>
                <th rowspan="2">Pre-requisite</th>
                <th rowspan="2">Grade</th>
            </tr>
            <tr>
                <th>Lec</th>
                <th>Lab</th>
                <th>Total</th>
            </tr>
            <tr>
            <?php
					$i = 1;
					$qry = $conn->query("SELECT * FROM 42curriculum2022 order by 8pc asc, 8dt asc, 8lc asc, 8lb asc, 8to asc, 8pr asc, 8gr asc");
					while($row= $qry->fetch_assoc()):
					?>
            <td><?php echo $row['8pc'] ?></td>
            <td><?php echo $row['8dt'] ?></td>
            <td><?php echo $row['8lc'] ?></td>
            <td><?php echo $row['8lb'] ?></td>
            <td><?php echo $row['8to'] ?></td>
            <td><?php echo $row['8pr'] ?></td>
            <td><?php echo $row['8gr'] ?></td>
            </tr>
            <?php endwhile; ?>
        </table>
    </div>
</div>
<div class="buttons">
        <button onclick="makeTableEditable()">Edit</button>
        <button onclick="saveData('<?php echo $student_code; ?>')">Save</button>
        <button onclick="printPage()">Print</button>
        <a href="./mainpage.php?page=results" class="nav-link nav-results nav-new_result nav-edit_result"><button>Back</button></a>
    </div>
</body>
<script>
    function hideHintText(input) {
        input.classList.remove('hint-text');
    }

    let setsCreated = 0;

function makeTableEditable() {
    const table = document.getElementById('editableTable');
    const cells = table.querySelectorAll('td');
    cells.forEach(cell => {
        cell.contentEditable = true;
    });
}
function synchronizeColumnWidths() {
// Get all tables with class 'curriculum'
const tables = document.querySelectorAll('.curriculum');

// Initialize an array to store the maximum width of each column
const maxWidths = Array.from({ length: tables[0].rows[0].cells.length }, () => 0);

// Loop through each table to find the maximum width of each column
tables.forEach(table => {
    table.rows.forEach(row => {
        row.cells.forEach((cell, index) => {
            maxWidths[index] = Math.max(maxWidths[index], cell.offsetWidth);
        });
    });
});

// Set the width of each cell in each table to the maximum width of the corresponding column
tables.forEach(table => {
    table.rows.forEach(row => {
        row.cells.forEach((cell, index) => {
            cell.style.width = maxWidths[index] + 'px';
        });
    });
});
}

function saveData(student_code) {
    // Exclude the script and buttons before gathering the HTML content
    const tableHtml = document.getElementById('editableTable').cloneNode(true);
    tableHtml.querySelector('.buttons').remove(); // Remove the buttons div

    // Prepare the data to be sent
    const formData = new FormData();
    formData.append('student_code', student_code);
    formData.append('tableHtml', tableHtml.outerHTML); // Use outerHTML to get the HTML as a string

    // Send the data using AJAX
    const xhr = new XMLHttpRequest();
    xhr.open('POST', 'save_data.php', true);
    xhr.onload = function () {
        if (xhr.status === 200) {
            alert('Data saved successfully!');
        } else {
            alert('Failed to save data.');
        }
    };
    xhr.send(formData);
}

function printPage() {
            window.print();
        }

</script>
</html>

