<?php
// Connect to the database
$conn = mysqli_connect("localhost", "root", "", "eas_db");

// Check the connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

// Retrieve data from the POST parameters
$student_code = $_POST['student_code'];
$tableHtml = $_POST['tableHtml'];

// Prepare the HTML content to be inserted into the database
$htmlContent = mysqli_real_escape_string($conn, $tableHtml);

// Update the content for the specific student
$updateSql = "UPDATE students SET content = '$htmlContent' WHERE student_code = '$student_code'";

if (mysqli_query($conn, $updateSql)) {
    echo "Data saved successfully!";
} else {
    echo "Error updating record: " . mysqli_error($conn);
}

// Close the connection
mysqli_close($conn);
?>
