<?php include 'db_connect.php'; ?>
<style>
    .btn-square {
    width: 50px; /* Adjust the width as needed */
    height: 50px; /* Same as the width to make it square */
    border-radius: 0;
    background-color: green; /* Removes rounded corners */
}
</style>
<div class="col-lg-12">
    <div class="card card-outline card-secondary">
        <div class="card-header">
            <div class="card-tools">
                <a class="btn btn-block btn-sm btn-default btn-flat border-primary new_subject" href="./index.php?page=curriculum"><i class="fa fa-plus"></i> Add New</a>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-hover table-bordered" id="list">
                <colgroup>
                    <col width="10%">
                    <col width="10%">
                    <col width="10%">
                    <col width="30%">
                    <col width="10%">
                    <col width="20%">
                    <col width="10%">
                </colgroup>
                <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th>School Year</th>
                        <th>Semester</th>
                        <th>Description</th>
                        <th>Track</th>
                        <th>Subject</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 1;
                    $curriculum_query = $conn->query("SELECT * FROM curriculum ORDER BY syear DESC");
                    while($curriculum_row = $curriculum_query->fetch_assoc()):
                    ?>
                    <tr>
                        <td class="text-center"><?php echo $i++ ?></td>
                        <td><b><?php echo ucwords($curriculum_row['syear']) ?></b></td>
                        <td><b><?php echo ucwords($curriculum_row['semester']) ?></b></td>
                        <td><b><?php echo ucwords($curriculum_row['descriptive']) ?></b></td>
                        <td><b><?php echo ucwords($curriculum_row['track']) ?></b></td>
                        <td>
                            <?php
                            $CurriculumID = $curriculum_row['CurriculumID'];
                            $subject_query = $conn->query("SELECT subjects.Pencode AS 'pencode'
                                FROM subjects 
                                JOIN curriculumsubject ON subjects.SubjectID = curriculumsubject.SubjectID
                                JOIN curriculum ON curriculumsubject.CurriculumID = curriculum.CurriculumID
                                WHERE curriculum.CurriculumID = $CurriculumID");
                            while($subject_row = $subject_query->fetch_assoc()):
                                echo ucwords($subject_row['pencode']). "<br>"; 
                            endwhile;
                            ?>
                        </td>
                        <td class="text-center">
                       
                            <div class="btn-group">
                                <a href="javascript:void(0)" style="background-color: #1F3761; color: white;" data-id='<?php echo $curriculum_row['CurriculumID'] ?>' class="btn btn-primary btn-flat manage_curriculum">
                                    <i class="fas fa-edit"></i>
                                </a>
                                <button class="btn btn-secondary btn-square insert_sub" data-id='<?php echo $curriculum_row['CurriculumID'] ?>'>
                                    <i class="fas fa-book"></i>
                                </button>
                                <button type="button" class="btn btn-danger btn-flat delete_curriculum" data-id="<?php echo $curriculum_row['CurriculumID'] ?>">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </div>
                        </td>
                    </tr>   
                    <?php endwhile; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
   $(document).ready(function(){
    $('#list').dataTable();
    
    // Click event for the "New Subject" button
    $(document).on('click', '.new_subject', function(){
        uni_modal("New Subject","manage_curriculum.php");
    });
    
    // Click event for the "Manage Curriculum" button
    $(document).on('click', '.manage_curriculum', function(){
        var curriculumID = $(this).attr('data-id');
        uni_modal("Manage Curriculum","manage_curriculum.php?CurriculumID="+ curriculumID);
    });
    
    // Click event for the "Insert Subject" button
    $(document).on('click', '.insert_sub', function(){
        var curriculumID = $(this).attr('data-id');
        uni_modal("Manage Curriculum","addsubjecttocurriculum.php?CurriculumID="+ curriculumID);
    });
    
    // Click event for the "Delete Curriculum" button
    $(document).on('click', '.delete_curriculum', function(){
        var curriculumID = $(this).attr('data-id');
        _conf("Are you sure to delete this curriculum?","delete_curriculum",[ curriculumID ]);
    });
});

    function delete_curriculum($CurriculumID){
        start_load()
        $.ajax({
            url:'ajax.php?action=delete_curriculum',
            method:'POST',
            data:{CurriculumID:$CurriculumID},
            success:function(resp){
                if(resp==1){
                    alert_toast("Data successfully deleted",'success')
                    setTimeout(function(){
                        location.reload()
                    },1500)
                }
            }
        })
    }
</script>
