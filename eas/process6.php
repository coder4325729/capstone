<?php
include 'db_connect.php'; // Include your database connection file
include 'history.php';
include 'query.php';

// Check if the form is submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Retrieve form data
    $SubjectID = $_POST['SubjectID'];
    $Pencode = strtoupper($_POST['Pencode']);
    $Description = ucwords(strtolower($_POST['Description']));
    $Lec = $_POST['Lec'];
    $Lab = $_POST['Lab'];

    // Check if the Prerequisite is "None"
    if (strtoupper($_POST['Prerequisite']) === "NONE") {
        // Transform to uppercase
        $Prerequisite = ucwords(strtolower($_POST['Prerequisite']));
    } else {
        // Keep the original value in uppercase
        $Prerequisite = strtoupper($_POST['Prerequisite']);
    }

    // Prepare update statement
    $stmt = $conn->prepare("UPDATE subjects SET 
                            Pencode = ?, 
                            `Description` = ?, 
                            Lec = ?, 
                            Lab = ?, 
                            Prerequisite = ? 
                            WHERE SubjectID = ?");

    // Bind parameters
    $stmt->bind_param("sssssi", $Pencode, $Description, $Lec, $Lab, $Prerequisite, $SubjectID);

    // Execute statement
    if ($stmt->execute()) {
        // Query executed successfully
        echo 1; // Return 1 for success
    } else {
        // Error in query execution
        echo 0; // Return 0 for failure
    }

    // Close prepared statement
    $stmt->close();

    // Close database connection
    $conn->close();
}
?>
