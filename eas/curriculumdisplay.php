<?php
include 'db_connect.php';
include 'query.php';
include 'maxyear.php';

// Retrieve the distinct syear from the database
$schoolyear_query = $conn->query("SELECT DISTINCT syear FROM curriculum");
$schoolyears = [];
while ($row = $schoolyear_query->fetch_assoc()) {
    $schoolyears[] = $row['syear'];
}
sort($schoolyears);
?>
<style>@media print {
    th.action,
    td.action {
        display: none;
       
    }
    .rmove,
    .new_curriculum,
    .no-print {
        display: none !important;
    }
}
.table td,
.table th {
    padding: 6px; /* Adjust as needed */
}
.table{
  font-size: 15px;
}
.action button{
  padding: 3px 8px 3px 8px;
}

</style>
<div>

<div class="card card-outline card-secondary">
  <div class="card-header">
    <div class="row">  
      <div class="col-md-2 d-flex align-items-center">  <label for="schoolyear_select" class="mr-2">Curriculum:</label>
        <select id="schoolyear_select" class="form-control">
          <?php foreach ($schoolyears as $schoolyear) { ?>
            <option value="<?php echo $schoolyear; ?>"><?php echo $schoolyear; ?></option>
          <?php } ?>
        </select>
      </div>
      <div class="col-md">  
        <div class="d-flex justify-content-end">  
          <a href="#" class="btn btn-sm btn-secondary btn-flat border-secondary btn-default new_curriculum">
            <i class="fas fa-plus"></i> Add New Curriculum
          </a>
          <span style="margin-left: 10px;"></span>
          <a href="#" class="btn btn-sm btn-secondary btn-flat border-secondary btn-default new_specialization">
            <i class="fas fa-plus"></i> Add Track
          </a>
        </div>
      </div>
    </div>
  </div>
</div>

<?php foreach ($schoolyears as $schoolyear) { ?>
  <div class="col-12 curriculum-data" id="curriculum_data_<?php echo $schoolyear; ?>" style="display: none;">
    <h5><b>Curriculum <?php echo $schoolyear; ?></b></h5>
    <div class="row">
      <?php 
        $curriculumTitle;
        $curriculumID = 0; 
        $maxyear = CountCurriculum($schoolyear);
                   
        for ($i = 1; $i <= 18; $i++) {
          $curriculumTitle_query = $conn->query("SELECT CurriculumID, descriptive FROM curriculum WHERE semester = $i AND syear = $schoolyear");
          $curriculumTitle_row = $curriculumTitle_query->fetch_assoc();
          $curriculumID = $curriculumTitle_row['CurriculumID'] ?? 0; 
          if ($i > 8) { 
            $curriculumTitle = isset($curriculumTitle_row['descriptive']) ? $curriculumTitle_row['descriptive'] : 'No data available';
          } else {
            $curriculumTitle = "Year " . ceil($i / 2) . " - " . ($i % 2 == 1 ? "First" : "Second") . " Semester";
          }
          $semesterExistsQuery = $conn->query("SELECT COUNT(*) as count FROM curriculum WHERE syear = $schoolyear AND semester = $i");
          $semesterExists = $semesterExistsQuery->fetch_assoc()['count'] > 0;
          if($semesterExists) { // Only display the table if there is a semester
      ?>
            <div class="col-md-6">
              <div class="btn-group mb-2">
                <h4><?php echo $curriculumTitle; ?></h4>
                <div class="btn-group ml-2">
                  <button type="button" class="btn btn-flat manage_curriculum" style="background-color: #1F3761; color: white;" data-id="<?php echo $curriculumID ?>">
                    <i class="fas fa-edit"></i>
                  </button>
                  <button type="button" class="btn btn-success btn-square insert_sub" data-id="<?php echo $curriculumID ?>">
                    <i class="fas fa-book"></i>
                  </button>
                  <button type="button" class="btn btn-danger btn-flat delete_curriculum" data-id="<?php echo $curriculumID ?>">
                    <i class="fas fa-trash"></i>
                  </button>
                </div>
              </div>
              <!-- Table for curriculum subjects -->
              <div class="table-responsive">
                <table class="table table-hover table-bordered" id="list_<?php echo $i; ?>">
                  <thead>
                    <tr>
                      <th>Pen Code</th>
                      <th>Descriptive Title</th>
                      <th>Lec</th>
                      <th>Lab</th>
                      <th>Total</th>
                      <th>Pre-requisite</th>
                      <th class="action">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                      $qry = $conn->query(select_CurriculumSubject($schoolyear, $i));
                      $total_units = 0; 
                      $total_lab = 0;
                      $total_lec = 0; 
                      while ($row = $qry->fetch_assoc()) : 
                        if ($row != null){
                          $total_lec += $row['Lec'];
                          $total_lab += $row['Lab'];
                          $total_subject_units = $row['Lec'] + $row['Lab'];
                          $total_units += $total_subject_units;
                         
                    ?>
                          <tr>
                            <td class="col-2"><?php echo ucwords($row['Pencode']) ?></td>
                            <td class="col-5"><?php echo ucwords($row['Description']) ?></td>
                            <td class="col-1"><?php echo ucwords($row['Lec']) ?></td>
                            <td class="col-1"><?php echo ucwords($row['Lab']) ?></td>
                            <td class="col-1"><?php echo ucwords($row['Lab'] + $row['Lec']) ?></td>
                            <td class="col-2"><?php echo ucwords($row['Prerequisite']) ?></td>
                            <td class="text-center col-1 action">
                              <div class="btn-group">
                                <button type="button" class="btn btn-danger btn-flat delete_CurriculumSubject" data-id="<?php echo $row['CurriculumSubjectID'] ?>">
                                  <i class="fas fa-trash"></i>
                                </button>
                              </div>
                            </td>
                          </tr>
                    <?php 
                        }
                      endwhile; 
                    ?>
                  </tbody>
                  <tr><td colspan ="2" style="text-align: center;">TOTAL UNITS</td>
                                <td><?php echo $total_lec?></td>
                                <td><?php echo $total_lab?></td>
                                <td><?php echo $total_units; ?></td>
                                <td></td>
                                <td></td>
                        </tr>
                </table>
              </div>
              <!-- End of table -->
            </div>
      <?php 
          } 
        } 
      ?>
    </div>
  </div>
<?php } ?>



</div>
<script>
    $(document).ready(function () {
        $('#schoolyear_select').change(function () {
            var selectedYear = $(this).val();
            $('.curriculum-data').hide();
            $('#curriculum_data_' + selectedYear).show();
            // Store the selected school year in session storage
            sessionStorage.setItem('selectedYear', selectedYear);
            console.log(selectedYear);
        });

        // Check if there's a selected school year in session storage
        var selectedYear = sessionStorage.getItem('selectedYear');
        if (selectedYear) {
            // If a school year is stored, select it in the dropdown and show its curriculum data
            $('#schoolyear_select').val(selectedYear).change();
        }

        $(document).on('click', '.new_curriculum', function(){
            uni_modal("New Curriculum","curriculum.php");
        });

        $(document).on('click', '.new_specialization', function(){
            uni_modal("New Specialization","addspecializationtocurriculum.php");
        });

        $('.delete_CurriculumSubject').click(function () {
            _conf("Are you sure to delete this Subject?", "delete_CurriculumSubject", [$(this).attr('data-id')])
        });

        // Click event for the "Manage Curriculum" button
        $(document).on('click', '.manage_curriculum', function(){
            var curriculumID = $(this).attr('data-id');
            uni_modal("Manage Curriculum","manage_curriculum.php?CurriculumID="+ curriculumID);
            console.log(curriculumID);
        });

        // Click event for the "Insert Subject" button
        $(document).on('click', '.insert_sub', function(){
            var curriculumID = $(this).attr('data-id');
            uni_modal("Manage Curriculum","addsubjecttocurriculum.php?CurriculumID="+ curriculumID);
        });

        // Click event for the "Delete Curriculum" button
        $(document).on('click', '.delete_curriculum', function(){
            var curriculumID = $(this).attr('data-id');
            _conf("Are you sure to delete this curriculum?","delete_curriculum",[ curriculumID ]);
        });
    });

    function delete_CurriculumSubject(CurriculumSubjectID) {
        start_load();
        $.ajax({
            url: 'ajax.php?action=delete_CurriculumSubject',
            method: 'POST',
            data: {
                CurriculumSubjectID: CurriculumSubjectID
            },
            success: function (resp) {
                if (resp == 1) {
                    alert_toast("Data successfully deleted", 'success');
                    setTimeout(function () {
                        location.reload();
                    }, 1500);
                }
            }
        });
    }

    function delete_curriculum(CurriculumID){
        start_load()
        $.ajax({
            url:'ajax.php?action=delete_curriculum',
            method:'POST',
            data:{CurriculumID:CurriculumID},
            success:function(resp){
                if(resp==1){
                    alert_toast("Data successfully deleted",'success')
                    setTimeout(function(){
                        location.reload()
                    },1500)
                }
            }
        });
    }



</script>