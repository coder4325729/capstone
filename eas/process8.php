<?php
include 'db_connect.php';

// Check if the form data is submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Check if Grade is set in the POST data
    if(isset($_POST['Status'])) {
        $Status = $_POST['Status'];

        // Check if StudentCurriculumSubjectID is set to determine whether to update or insert
        if(isset($_POST['StudentCurriculumSubjectID'])) {
            $StudentCurriculumSubjectID = $_POST['StudentCurriculumSubjectID'];

            // Prepare and execute the SQL query to update the grade
            $stmt = $conn->prepare("UPDATE studentcurriculumsubject SET Status = ? WHERE StudentCurriculumSubjectID = ?");
            $stmt->bind_param("si", $Status, $StudentCurriculumSubjectID);
        } else {
            // Prepare and execute the SQL query to insert the new record
            $stmt = $conn->prepare("INSERT INTO studentcurriculumsubject (Status) VALUES (?)");
            $stmt->bind_param("s", $Status);
        }

        // Execute the statement
        if ($stmt->execute()) {
            // Close statement and database connection
            $stmt->close();
            $conn->close();

            // Return a success message
            echo "1";
        } else {
            // Return an error message if the query fails
            echo "Error: " . $stmt->error;
        }
    } else {
        // Return an error message if Grade is not set in the POST data
        echo "Status is not set!";
    }
} else {
    // Return an error message if the form data is not submitted
    echo "Invalid request!";
}
?>
