<?php
include 'db_connect.php';

if(isset($_POST['CurriculumID'])) {
    $CurriculumID = $conn->real_escape_string($_POST['CurriculumID']);
    $subject_query = $conn->query("SELECT subjects.SubjectID, subjects.Pencode AS 'pencode'
        FROM subjects 
        JOIN curriculumsubject ON subjects.SubjectID = curriculumsubject.SubjectID
        JOIN curriculum ON curriculumsubject.CurriculumID = curriculum.CurriculumID
        WHERE curriculum.CurriculumID = $CurriculumID");

    if($subject_query->num_rows > 0) {
        $semester_data = '<div class="form-group"><label>Select Subjects:</label><br>';
        while($subject_row = $subject_query->fetch_assoc()) {
            $semester_data .= '<label><input type="checkbox" class="pencode-checkbox" data-subject-id="' . $subject_row['SubjectID'] . '"> ' . ucwords($subject_row['pencode']) . '</label><br>'; 
        }
        $semester_data .= '</div>';
        echo $semester_data;
    } else {
        echo 'No semester data found for the selected curriculum.';
    }
} else {
    echo 'CurriculumID is not set.';
}
?>
