<?php
// Connect to the database
$conn = mysqli_connect("localhost", "root", "", "eas_db");

// Check for form submission
if (isset($_POST['update'])) {
    $id = $_POST['id'];
    $content = $_POST['content'];

    // Prepare and execute the update query
    $sql = "UPDATE table_data SET content = ? WHERE id = ?";
    $stmt = mysqli_prepare($conn, $sql);
    mysqli_stmt_bind_param($stmt, "si", $content, $id);
    mysqli_stmt_execute($stmt);

    if (mysqli_affected_rows($conn) > 0) {
        header("Location: index.php?updated=true"); // Redirect with success message
    } else {
        // Handle update error
    }
} else {
    // Retrieve content for editing
    $id = $_GET['id'];
    $result = mysqli_query($conn, "SELECT * FROM table_data WHERE id = $id");
    $row = mysqli_fetch_assoc($result);
    $content = $row['content'];
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit HTML Content</title>
</head>
<body>
    <form action="edit.php" method="post">
        <input type="hidden" name="id" value="<?php echo $id; ?>">
        <textarea name="content"><?php echo $content; ?></textarea>
        <button type="submit" name="update">Update</button>
    </form>
</body>
</html>