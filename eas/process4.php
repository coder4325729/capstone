<?php
// Include your database connection file
include 'db_connect.php';

// Check if the form is submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Retrieve form data
    $CurriculumID = $_POST['CurriculumID'];
    $SubjectIDs = $_POST['SubjectIDs']; // Notice the 'SubjectIDs' instead of 'SubjectID'

    // Set initial ordinality value
    $ordinality = 1;

    // Begin a transaction
    $conn->begin_transaction();

    try {
        // Loop through each SubjectID and insert into curriculumsubject table
        foreach ($SubjectIDs as $SubjectID) {
            // Perform SQL query to insert data into the curriculumsubject table
            $query = "INSERT INTO curriculumsubject (CurriculumID, SubjectID, ordinality) VALUES ('$CurriculumID', '$SubjectID', '$ordinality')";
            $result = $conn->query($query);

            // Check if the query was successful
            if (!$result) {
                throw new Exception("Error: " . $conn->error);
            }

            // Increment ordinality for the next insertion
            $ordinality++;
        }

        // Commit the transaction for curriculumsubject table
        $conn->commit();
        echo "Subjects inserted into curriculumsubject table successfully.<br>";

        // Begin another transaction for studentcurriculumsubject table
        $conn->begin_transaction();

        // Retrieve syear and semester from curriculum table based on CurriculumID
        $query_syear = "SELECT syear, semester, descriptive FROM curriculum WHERE CurriculumID = '$CurriculumID'";
        $result_syear = $conn->query($query_syear);

        if ($result_syear->num_rows > 0) {
            $row_syear = $result_syear->fetch_assoc();
            $schoolyear = $row_syear['syear'];
            $semester = $row_syear['semester'];
            $descriptive = $row_syear['descriptive'];

            // Retrieve StudentIDs of students who have the retrieved syear and semester
            $query_students = "SELECT StudentID FROM studentcurriculumsubject WHERE syear = '$schoolyear' AND Semester = '$semester'";
            $result_students = $conn->query($query_students);

            if ($result_students->num_rows > 0) {
                while ($row_student = $result_students->fetch_assoc()) {
                    $StudentID = $row_student['StudentID'];

                    // Insert or update each SubjectID into studentcurriculumsubject table
                    foreach ($SubjectIDs as $SubjectID) {
                        if (!empty($descriptive)) {
                            // Check for existing subject with "IT Elective" description
                            $query_check_replace = "SELECT scs.StudentCurriculumSubjectID, scs.Semester 
                                                    FROM studentcurriculumsubject scs
                                                    JOIN subjects s ON scs.SubjectID = s.SubjectID
                                                    JOIN students st ON scs.StudentID = st.id
                                                    WHERE s.Description LIKE '%IT Elective%'
                                                      AND scs.StudentID = '$StudentID'
                                                      AND st.track = '$descriptive'";
                            $result_check_replace = $conn->query($query_check_replace);

                            if ($result_check_replace->num_rows > 0) {
                                // Replace the existing "IT Elective" subject
                                $row_check_replace = $result_check_replace->fetch_assoc();
                                $existingStudentCurriculumSubjectID = $row_check_replace['StudentCurriculumSubjectID'];
                                $existingSemester = $row_check_replace['Semester'];

                                // Delete the existing "IT Elective" subject
                                $query_delete_existing = "DELETE FROM studentcurriculumsubject 
                                                          WHERE StudentCurriculumSubjectID = '$existingStudentCurriculumSubjectID'";
                                $result_delete_existing = $conn->query($query_delete_existing);

                                if (!$result_delete_existing) {
                                    throw new Exception("Error: " . $conn->error);
                                }

                                // Insert the new subject, inheriting the semester from the deleted record
                                $query_insert_student = "INSERT INTO studentcurriculumsubject (StudentID, SubjectID, syear, Semester) VALUES ('$StudentID', '$SubjectID', '$schoolyear', '$existingSemester')";
                                $result_insert_student = $conn->query($query_insert_student);

                                if (!$result_insert_student) {
                                    throw new Exception("Error: " . $conn->error);
                                }
                            } else {
                                // Insert the new subject if no "IT Elective" found
                                $query_insert_student = "INSERT INTO studentcurriculumsubject (StudentID, SubjectID, syear, Semester) VALUES ('$StudentID', '$SubjectID', '$schoolyear', '$semester')";
                                $result_insert_student = $conn->query($query_insert_student);

                                if (!$result_insert_student) {
                                    throw new Exception("Error: " . $conn->error);
                                }
                            }
                        } else {
                            // Insert the new subject if no descriptive value
                            $query_insert_student = "INSERT INTO studentcurriculumsubject (StudentID, SubjectID, syear, Semester) VALUES ('$StudentID', '$SubjectID', '$schoolyear', '$semester')";
                            $result_insert_student = $conn->query($query_insert_student);

                            if (!$result_insert_student) {
                                throw new Exception("Error: " . $conn->error);
                            }
                        }
                    }
                }
            } else {
                throw new Exception("Error: No matching students found for syear $schoolyear and semester $semester");
            }
        } else {
            throw new Exception("Error: No matching syear found for CurriculumID $CurriculumID");
        }

        // Commit the transaction for studentcurriculumsubject table
        $conn->commit();
        echo "Insertion/Update into studentcurriculumsubject table successful";
    } catch (Exception $e) {
        // Rollback the transaction in case of an error
        $conn->rollback();
        echo $e->getMessage();
    }

    // Close database connection
    $conn->close();
}
?>
