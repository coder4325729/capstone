<?php
include 'db_connect.php';
if(isset($_GET['StudentCurriculumSubjectID'])){
	$qry = $conn->query("SELECT * FROM studentcurriculumsubject where StudentCurriculumSubjectID ={$_GET['StudentCurriculumSubjectID']}")->fetch_array();
	foreach($qry as $k => $v){
		$$k = $v;
	}
}
?>
<div class="container-fluid">
	<form action="" id="manage-grade">
		<input type="hidden" name="StudentCurriculumSubjectID" value="<?php echo isset($StudentCurriculumSubjectID) ? $StudentCurriculumSubjectID : '' ?>">
		<div id="msg" class="form-group"></div>
		
		
		<div class="form-group">
			<label for="Grade" class="control-label">Grade</label>
			<input type="number" class="form-control form-control-sm" name="Grade" id="Grade" value="<?php echo isset($Grade) ? $Grade : '' ?>">
		</div>
	</form>
</div>
<script>
	$(document).ready(function(){
		$('#manage-grade').submit(function(e){
			e.preventDefault();
			start_load()
			$.ajax({
				url: 'process2.php',
				method:'POST',
				data:$(this).serialize(),
				success:function(resp){
					if(resp == 1){
						alert_toast("Data successfully saved.","success");
						setTimeout(function(){
							location.reload()	
						},1750)
					}else if(resp == 2){
						$('#msg').html('<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Subject Code already exist.</div>')
						end_load()
					}
				}
			})
		})
	})

</script>