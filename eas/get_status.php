<?php
// Include your database connection file
include 'db_connect.php';

// Check if the StudentCurriculumSubjectID is provided in the POST data
if (isset($_POST['StudentCurriculumSubjectID'])) {
    // Retrieve the StudentCurriculumSubjectID from the POST data
    $studentCurriculumSubjectID = $_POST['StudentCurriculumSubjectID'];

    // Query the database to fetch the status for the specified StudentCurriculumSubjectID
    $status_query = $conn->query("SELECT Status FROM studentcurriculumsubject WHERE StudentCurriculumSubjectID = $studentCurriculumSubjectID");

    // Check if the query was successful
    if ($status_query) {
        // Fetch the status from the query result
        $status_row = $status_query->fetch_assoc();
        $status_numeric = isset($status_row['Status']) ? $status_row['Status'] : null;

        // Map numeric status to human-readable labels
        $status_labels = array(
            2 => 'NC',
            3 => 'Passed',
            4 => 'Failed',
            5 => 'FA',
            6 => 'INC'
            // Add more mappings as needed
        );

        // Get the corresponding status label
        $status_label = isset($status_labels[$status_numeric]) ? '<b>' . $status_labels[$status_numeric] . '</b>' : '<b>Unknown</b>';

        // Return the status label as the response
        echo $status_label;
    } else {
        // If the query fails, return an error message
        echo 'Error fetching status from the database';
    }
} else {
    // If StudentCurriculumSubjectID is not provided in the POST data, return an error message
    echo 'StudentCurriculumSubjectID not provided';
}
?>