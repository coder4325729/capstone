<?php include 'db_connect.php' ?>
<style>
</style>

<div class="card card-outline card-secondary">
    <div class="card-header">
        <div class="card-tools">
            <div class="row">  
                <div class="col-md-6">
                    <div class="d-flex justify-content-center">  
                        <!-- Add New button -->
                        <div class="mb-3">
                            <a class="btn btn-block btn-sm btn-default btn-flat border-secondary new_subject" href="javascript:void(0)"><i class="fa fa-plus"></i> Add New</a>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="d-flex justify-content-end">  
                        <!-- Import button with hidden file input -->
                        <div class="mb-3">
                            <label for="import-file" class="btn btn-block btn-sm btn-default btn-flat border-secondary import_subject" style="width: 100px;"><i class="fa fa-upload"></i> Import</label>
                            <input type="file" id="import-file" accept=".csv" style="display: none;">
                        </div>
                    </div>
                    <div class="mb-3" id="imported-file-info" style="display: none;">
                        <span id="file-info"></span>
                        <button id="remove-btn" class="btn btn-danger btn-sm ml-2"><i class="fas fa-times"></i> Remove</button>
                    </div>
                    <div id="upload-btn-container" style="display: none;">
                        <button id="upload-btn" style="background-color: #1F3761; color: white;" class="btn btn-sm mt-2">Upload</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive"> <!-- Wrap the table with this div -->
            <table class="table table-hover table-bordered" id="list">
                <colgroup>
                    <col width="10%">
                    <col width="15%">
                    <col width="30%">
                    <col width="10%">
                    <col width="10%">
                    <col width="15%">
                    <col width="10%">
                </colgroup>
                <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th>Subjects</th>
                        <th>Subject Title</th>
                        <th>Lec</th>
                        <th>Lab</th>
                        <th>Pre-requisite</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 1;
                    $qry = $conn->query("SELECT * FROM subjects order by unix_timestamp(date_created) desc ");
                    while($row= $qry->fetch_assoc()):
                    ?>
                    <tr>
                        <th class="text-center"><?php echo $i++ ?></th>
                        <td><b><?php echo ucwords($row['Pencode']) ?></b></td>
                        <td><b><?php echo ucwords($row['Description']) ?></b></td>
                        <td><b><?php echo ucwords($row['Lec']) ?></b></td>
                        <td><b><?php echo ucwords($row['Lab']) ?></b></td>
                        <td><b><?php echo ucwords($row['Prerequisite']) ?></b></td>
                        <td class="text-center">
                            <div class="btn-group">
                                <a href="javascript:void(0)" data-id='<?php echo $row['SubjectID'] ?>' class="btn btn-flat manage_subject" style="background-color: #1F3761; color: white;">
                                    <i class="fas fa-edit"></i>
                                </a>
                                <button type="button" class="btn btn-danger btn-flat delete_subject" data-id="<?php echo $row['SubjectID'] ?>">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </div>
                        </td>
                    </tr>
                    <?php endwhile; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#list').dataTable();

        // Click event for new subject button
        $(document).on('click', '.new_subject', function () {
            uni_modal("New Subject", "new_subject.php");
        });

        // Click event for managing subject
        $(document).on('click', '.manage_subject', function () {
            var subjectID = $(this).attr('data-id');
            uni_modal("Manage Subject", "manage_subject.php?SubjectID=" + subjectID);
        });

        // Click event for deleting subject
        $(document).on('click', '.delete_subject', function () {
            var subjectID = $(this).attr('data-id');
            _conf("Are you sure to delete this Subject?", "delete_subject", [subjectID]);
        });
    });

    // Function to handle subject deletion
    function delete_subject(subjectID) {
        start_load();
        $.ajax({
            url: 'ajax.php?action=delete_subject',
            method: 'POST',
            data: { SubjectID: subjectID },
            success: function (resp) {
                if (resp == 1) {
                    alert_toast("Data successfully deleted", 'success');
                    setTimeout(function () {
                        location.reload();
                    }, 1500);
                }
            }
        });
    }

    $('#import-file').change(function () {
        var file = $(this)[0].files[0];
        if (file) {
            var fileSize = file.size;
            var fileName = file.name;
            $('#file-info').html('Selected file: ' + fileName + ' (' + formatBytes(fileSize) + ')');
            $('#imported-file-info').show(); // Show file info and remove button
            $('#upload-btn-container').show(); // Show the upload button
        }
    });

    // Remove button click event
    $('#remove-btn').click(function () {
        $('#import-file').val(''); // Clear the file input value
        $('#imported-file-info').hide(); // Hide file info and remove button
        $('#upload-btn-container').hide(); // Hide the upload button
    });

    // Upload button click event
    $('#upload-btn').click(function () {
        var file = $('#import-file')[0].files[0];
        if (file) {
            var formData = new FormData();
            formData.append('file', file);
            $.ajax({
                url: 'csv_upload.php',
                method: 'POST',
                data: formData,
                contentType: false,
                processData: false,
                success: function (response) {
                    alert_toast("Data successfully uploaded.", "success");
                    // Reload the page after importing
                    setTimeout(function () {
                        location.reload();
                    }, 1500);
                },
                error: function (xhr, status, error) {
                    console.error(error);
                    alert_toast("Error uploading data. Please try again.", "error");
                }
            });
        } else {
            alert_toast("Please select a file to upload.", "warning");
        }
    });

    // Function to format file size in bytes
    function formatBytes(bytes, decimals = 2) {
        if (bytes === 0) return '0 Bytes';
        const k = 1024;
        const dm = decimals < 0 ? 0 : decimals;
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
        const i = Math.floor(Math.log(bytes) / Math.log(k));
        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    }
</script>