<?php if (!isset($conn)) {
    include 'db_connect.php';
} ?>

<div class="col-lg-12">
    <div class="card card-outline card-secondary">
        <div class="card-body">
            <form action="" id="manage-student">
                <input type="hidden" name="id" value="<?php echo isset($id) ? $id : '' ?>">
                <div class="row">
                    <div class="col-md-6">
                        <div id="msg" class=""></div>
                        <div class="form-group text-dark">
                            <div class="form-group">
                                <label for="" class="control-label">Student ID #</label>
                                <input type="text" class="form-control form-control-sm" name="student_code" value="<?php echo isset($student_code) ? $student_code : '' ?>" required placeholder="Example: 03-2133-024671">
                            </div>
                        </div>
                        <div class="form-group text-dark">
                            <div class="form-group">
                                <label for="" class="control-label">First Name</label>
                                <input type="text" class="form-control form-control-sm" name="firstname" value="<?php echo isset($firstname) ? $firstname : '' ?>" required>
                            </div>
                        </div>
                        <div class="form-group text-dark">
                            <div class="form-group">
                                <label for="" class="control-label">Middle Name</label>
                                <input type="text" class="form-control form-control-sm" name="middlename" value="<?php echo isset($middlename) ? $middlename : '' ?>">
                            </div>
                        </div>
                        <div class="form-group text-dark">
                            <div class="form-group">
                                <label for="" class="control-label">Last Name</label>
                                <input type="text" class="form-control form-control-sm" name="lastname" value="<?php echo isset($lastname) ? $lastname : '' ?>" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group text-dark">
                            <div class="form-group">
                                <label for="" class="control-label">Curriculum Year</label>
                                <select name="year" id="year" class="form-control select2 select2-sm" required>
                                    <option></option>
                                    <?php 
                                    // Fetch unique syear values from the database
                                    $table_data = $conn->query("SELECT DISTINCT syear FROM curriculum ORDER BY syear ASC");
                                    while($row = $table_data->fetch_array()):
                                    ?>
                                        <option value="<?php echo $row['syear'] ?>" <?php echo isset($year) && $year == $row['syear'] ? "selected" : '' ?>><?php echo ($row['syear']) ?></option>
                                    <?php endwhile; ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group text-dark">
                            <div class="form-group">
                                <label for="" class="control-label">Track</label>
                                <select name="track" id="track" class="custom-select custom-select-m" required>
                                    <!-- Options will be dynamically populated using JavaScript -->
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        </form>
    </div>
    </div>
</div>
</div>

<script>
    $('#year').change(function() {
        var curriculumYear = $(this).val();

        // Fetch the corresponding Track options from the database using AJAX
        $.ajax({
            url: 'fetch_track_options.php', // Replace with the file path where you handle the AJAX request to fetch track options
            method: 'POST',
            data: {
                curriculumYear: curriculumYear
            },
            dataType: 'html', // Change the data type as per your response
            success: function(response) {
                // Update the Track select options
                $('#track').html(response);
            },
            error: function(xhr, status, error) {
                // Handle errors if any
                console.error(xhr.responseText);
            }
        });
    });

    $('#manage-student').submit(function (e) {
            e.preventDefault();
            start_load();
            $.ajax({
                url:'ajax.php?action=save_student',
data: new FormData($(this)[0]),
            cache: false,
            contentType: false,
            processData: false,
                method: 'POST',
                type: 'POST',
            success: function(data,state) {
                alert_toast("Data successfully saved.","success");
						setTimeout(function(){
							location.reload()	
						},1750)
           console.log(data);
          console.log(state);
          // alert('ajax success');
       },

            // addsubjects(resp)
                //if(resp == 1){
                    //alert_toast('Data successfully saved',"success");
                        //setTimeout(function(){
                      //  location.href = 'index.php?page=student_list'
                    //},2000)
                //}else if(resp == 2){
                        //   $('#msg').html('<div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Student Code already exist.</div>')
               //     end_load()
                //}
            //}
    })
        
        end_load()
        })
    function displayImgCover(input,_this) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#cover').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    function addsubjects($StudentID, $syear){
        start_load()
        $.ajax({
            url:'addsubjects.php',
            method:'POST',
            data:{id:$id},
            success:function(resp){
                if(resp==1){
                    alert_toast("Data successfully deleted",'success')
                    setTimeout(function(){
                        location.reload()
                    },1500)
                }
            }
        })
    } 
    var textInputs = document.querySelectorAll('input[type="text"], textarea');

// Iterate through each input field and add event listener
textInputs.forEach(function(input) {
    input.addEventListener('input', function(event) {
        // Convert the entered text to uppercase first letter of each word
        let words = this.value.toLowerCase().split(' ');
        for (let i = 0; i < words.length; i++) {
            words[i] = words[i].charAt(0).toUpperCase() + words[i].slice(1);
        }
        this.value = words.join(' ');
    });
});

$('#import-file').change(function () {
        var file = $(this)[0].files[0];
        if (file) {
            var fileSize = file.size;
            var fileName = file.name;
            $('#file-info').html('Selected file: ' + fileName + ' (' + formatBytes(fileSize) + ')');
            $('#imported-file-info').show(); // Show file info and remove button
            $('#upload-btn-container').show(); // Show the upload button
        }
    });

    // Remove button click event
    $('#remove-btn').click(function () {
        $('#import-file').val(''); // Clear the file input value
        $('#imported-file-info').hide(); // Hide file info and remove button
        $('#upload-btn-container').hide(); // Hide the upload button
    });

    // Upload button click event
    $('#upload-btn').click(function () {
        var file = $('#import-file')[0].files[0];
        if (file) {
            var formData = new FormData();
            formData.append('file', file);
            $.ajax({
                url: 'csv_upload1.php',
                method: 'POST',
                data: formData,
                contentType: false,
                processData: false,
                success: function (response) {
                    alert_toast("Data successfully uploaded.", "success");
                    // Reload the page after importing
                    setTimeout(function () {
                        location.reload();
                    }, 1500);
                },
                error: function (xhr, status, error) {
                    console.error(error);
                    alert_toast("Error uploading data. Please try again.", "error");
                }
            });
        } else {
            alert_toast("Please select a file to upload.", "warning");
        }
    });

    // Function to format file size in bytes
    function formatBytes(bytes, decimals = 2) {
        if (bytes === 0) return '0 Bytes';
        const k = 1024;
        const dm = decimals < 0 ? 0 : decimals;
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
        const i = Math.floor(Math.log(bytes) / Math.log(k));
        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    }
</script>
