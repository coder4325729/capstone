<?php
// Include the database connection file
include 'db_connect.php';

if(isset($_POST['query'])) {
    $query = $_POST['query'];
    $output = '';

    // Fetch suggestions from the database based on the user input
    $sql = "SELECT Pencode FROM subjects WHERE Pencode LIKE '%$query%' LIMIT 5";
    $result = mysqli_query($conn, $sql);

    if(mysqli_num_rows($result) > 0) {
        while($row = mysqli_fetch_assoc($result)) {
            $output .= '<div class="suggestion">' . $row['Pencode'] . '</div>';
        }
    } else {
        $output .= '<div class="suggestion">No suggestions found</div>';
    }

    echo $output;
}

// Close the connection (optional, depending on your setup)
mysqli_close($conn);
?>
