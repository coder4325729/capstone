<?php
session_start(); // Start the session to access session variables
include 'db_connect.php';
include 'history.php';
include 'query.php';

// Check if the form data is submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Check if Grade is set in the POST data
    if(isset($_POST['Grade'])) {
        $Grade = $_POST['Grade'];
        $input = strtolower($Grade); // Convert input to lowercase for case insensitivity

        // Default Status
        $Status = null;

        // Check if the input is a number, decimal, or 'b'
        if (is_numeric($Grade) || preg_match("/^\d+(\.\d+)?$/", $Grade) || $input == 'b') {
            // Special handling for a grade of zero or 'b'
            if ($Grade == 0 || $input == 'b') {
                if (isset($_POST['StudentCurriculumSubjectID'])) {
                    $StudentCurriculumSubjectID = $_POST['StudentCurriculumSubjectID'];

                    // Fetch the prerequisite from the subjects table
                    $qry_prerequisite = "SELECT s.Prerequisite 
                                         FROM studentcurriculumsubject scs
                                         JOIN subjects s ON scs.SubjectID = s.SubjectID
                                         WHERE scs.StudentCurriculumSubjectID = ?";
                    $stmt_prerequisite = $conn->prepare($qry_prerequisite);
                    $stmt_prerequisite->bind_param("i", $StudentCurriculumSubjectID);
                    $stmt_prerequisite->execute();
                    $result_prerequisite = $stmt_prerequisite->get_result();

                    if ($result_prerequisite->num_rows > 0) {
                        $row_prerequisite = $result_prerequisite->fetch_assoc();
                        $Prerequisite = strtolower($row_prerequisite['Prerequisite']);

                        // Determine status based on prerequisite
                        $Status = ($Prerequisite == "none") ? 0 : 1;
                    }
                    $stmt_prerequisite->close();
                }
            } else {
                // If input is a number or decimal other than zero, set the status based on the grade
                $Status = ($Grade > 0 && $Grade <= 3) ? 3 : 4;
            }
        } else {
            // Map input to corresponding status values
            switch ($input) {
                case 'inc':
                    $Status = 6; // Assuming 'INC' corresponds to a specific status
                    break;
                case 'nc':
                    $Status = 2; // Assuming 'NC' corresponds to a specific status
                    break;
                case 'c':
                    $Status = 7; // Assuming 'C' corresponds to a specific status
                    break;
                case 'fa':
                    $Status = 5; // Assuming 'FA' corresponds to a specific status
                    break;
                default:
                    // If input is not recognized, return an error
                    echo "Invalid input!";
                    exit(); // Exit the script
            }
        }

        // Ensure Status is set before proceeding
        if ($Status === null) {
            echo "Could not determine status!";
            exit();
        }

        // Check if StudentCurriculumSubjectID is set to determine whether to update or insert
        if (isset($_POST['StudentCurriculumSubjectID'])) {
            $StudentCurriculumSubjectID = $_POST['StudentCurriculumSubjectID'];

            // Prepare and execute the SQL query to update the grade and status
            $stmt = $conn->prepare("UPDATE studentcurriculumsubject SET Grade = ?, Status = ? WHERE StudentCurriculumSubjectID = ?");
            $stmt->bind_param("dii", $Grade, $Status, $StudentCurriculumSubjectID);
        } else {
            // Prepare and execute the SQL query to insert the new record
            $stmt = $conn->prepare("INSERT INTO studentcurriculumsubject (Grade, Status) VALUES (?, ?)");
            $stmt->bind_param("di", $Grade, $Status);
        }

        // Execute the statement
        if ($stmt->execute()) {
            // Retrieve the user ID from the session
            $user_id = $_SESSION['login_id']; // Assuming 'login_id' is the session variable containing the user ID

            // Call get_history function to fetch user history
            get_history($conn, $user_id);

            // Log the history
            log_history($conn, 2, $StudentCurriculumSubjectID, "", "", "", $user_id, "");

            // Close statement and database connection
            $stmt->close();
            $conn->close();

            // Return a success message
            echo "1";
        } else {
            // Return an error message if the query fails
            echo "Error: " . $stmt->error;
        }
    } else {
        // Return an error message if Grade is not set in the POST data
        echo "Grade is not set!";
    }
} else {
    // Return an error message if the form data is not submitted
    echo "Invalid request!";
}
?>
