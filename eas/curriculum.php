<div class="col-lg-12">
    <div class="card card-outline card-secondary">
        <div class="card-body">
            <form id="curriculum-form" method="post">
                <input type="hidden" name="CurriculumID">
                <div class="row">
                    <div class="col-md-6">
                        <div id="msg" class=""></div>
                        <div class="form-group text-dark">
                            <div class="form-group">
                                <label for="" class="control-label">Curriculum Year #</label>
                                <input type="text" value="2024" class="form-control form-control-sm" name="syear">
                            </div>
                            </div>
                        <div class="form-group text-dark">
                            <div class="form-group">
                                <label for="" class="control-label" hidden>Description</label>
                                <input type="text" class="form-control form-control-sm" name="descriptive" placeholder="(Optional)" hidden>
                            </div>
                        </div>
                        <div class="form-group text-dark">
                            <div class="form-group">
                                <label for="semester">Semester:</label>
                                <select name="semester" id="semester" class="form-control form-control-sm" required>
                                 
                                    <option value="1">First Year - First Semester</option>
                                    <option value="2">First Year - Second Semester</option>
                                    <option value="3">Second Year - First Semester</option>
                                    <option value="4">Second Year - Second Semester</option>
                                    <option value="5">Third Year - First Semester</option>
                                    <option value="6">Third Year - Second Semester</option>
                                    <option value="7">Fourth Year - First Semester</option>
                                    <option value="8">Fourth Year - Second Semester</option>
                                </select>
                            </div>
                        </div>
                        <!-- <div class="form-group text-dark">
                            <div class="form-group">
                                <label for="track" class="control-label">Track</label>
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" name="track" value="1">
                                    <label class="form-check-label">Track Option</label>
                                </div>
                            </div>
                        </div> -->
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Success Modal -->
<div class="modal fade" id="successModal" tabindex="-1" aria-labelledby="successModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="successModalLabel">Success</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                Data successfully saved.
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function(){
    $('#curriculum-form').submit(function(e){
        e.preventDefault();
        start_load();
        var formData = $('#curriculum-form').serialize();
        $.ajax({
            url: 'process3.php',
            method: 'POST',
            data: formData,
            success: function(response){
                $('#successModal').modal('show');
                setTimeout(function(){
                        location.reload(); // Reload the page after 2 seconds
                    }, 1000);
                end_load();
            }
        });
    });
});
</script>
