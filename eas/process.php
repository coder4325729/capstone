<?php
// Include your database connection file
include 'db_connect.php';

// Check if the form is submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Retrieve form data
    $syear = $_POST['syear'];
    $descriptive = strtoupper($_POST['descriptive']);
    $semester = $_POST['semester'];
    $track = $_POST['track'];

    // Check if CurriculumID is provided (for update)
    if(isset($_POST['CurriculumID'])) {
        $CurriculumID = $_POST['CurriculumID'];
        // Perform SQL query to update data in the curriculum table
        $query = "UPDATE curriculum SET syear='$syear', descriptive='$descriptive', semester='$semester', track='$track' WHERE CurriculumID = $CurriculumID";
    } else {
        // Perform SQL query to insert data into the curriculum table
        $query = "INSERT INTO curriculum (syear, descriptive, semester, track) VALUES ('$syear', '$descriptive', '$semester', '$track')";
    }
    
    $result = $conn->query($query);

    // Check if the query was successful
    if ($result) {
        echo "Data saved successfully!";
    } else {
        echo "Error: " . $conn->error;
    }

    // Close database connection
    $conn->close();
}
?>
