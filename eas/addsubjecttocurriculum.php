<?php
include 'db_connect.php';

$curriculum_data = [];
$subject_data = [];
$existing_subjects = [];

if (isset($_GET['CurriculumID'])) {
    $CurriculumID = $_GET['CurriculumID'];

    // Fetch curriculum details
    $qry = $conn->query("SELECT * FROM curriculum WHERE CurriculumID = $CurriculumID");
    $curriculum_row = $qry->fetch_assoc();
    if ($curriculum_row) {
        $syear = $curriculum_row['syear'];
        $curriculum_data[$curriculum_row['CurriculumID']] = $curriculum_row['descriptive'];

        // Fetch subjects already associated with the given curriculum syear
        $existing_subjects_query = $conn->query("SELECT cs.SubjectID FROM curriculumsubject cs 
                                                 JOIN curriculum c ON cs.CurriculumID = c.CurriculumID 
                                                 WHERE c.syear = '$syear'");
        while ($existing_subject = $existing_subjects_query->fetch_assoc()) {
            $existing_subjects[] = $existing_subject['SubjectID'];
        }

        // Fetch all subjects and filter out those already associated with the syear
        $specialization = $conn->query("SELECT * FROM subjects ORDER BY Pencode ASC");
        while ($subject_row = $specialization->fetch_assoc()) {
            if (!in_array($subject_row['SubjectID'], $existing_subjects)) {
                // Concatenate Pencode and Prerequisite if both are present
                if (!empty($subject_row['Pencode']) && !empty($subject_row['Prerequisite'])) {
                    $subject_data[$subject_row['SubjectID']] = $subject_row['Pencode'] . ' - ' . $subject_row['Prerequisite'];
                } elseif (!empty($subject_row['Pencode'])) {
                    $subject_data[$subject_row['SubjectID']] = $subject_row['Pencode'];
                } else {
                    $subject_data[$subject_row['SubjectID']] = $subject_row['Description']; // Display Description if Pencode is empty
                }
            }
        }
    }
}
?>

<div class="container-fluid">
    <form action="" id="manage-curriculum">
        <input type="hidden" name="CurriculumID" value="<?php echo isset($CurriculumID) ? $CurriculumID : '' ?>">
        <div id="msg" class="form-group"></div>
        <div class="form-group">
            <input type="text" class="form-control" id="searchInput" placeholder="Search Subjects">
        </div>
        <div class="row">
            <?php foreach ($curriculum_data as $year => $curriculum_description): ?>
                <div class="col-12">
                    <h6><b>Pencode - Prerequisite</b></h6>
                    <div class="form-group">
                        <?php 
                        $i = 1;
                        foreach ($subject_data as $subject_id => $subject): ?>
                            <?php if ($i % 3 == 1): ?>
                                <div class="row">
                            <?php endif; ?>
                            <div class="form-check col-4"> 
                                <input class="form-check-input subjectCheckbox" type="checkbox" name="SubjectID[]" value="<?php echo $subject_id ?>" id="subject_<?php echo $subject_id ?>">
                                <label class="form-check-label" for="subject_<?php echo $subject_id ?>">
                                    <?php echo $subject; ?>
                                </label>
                            </div>
                            <?php if ($i % 3 == 0): ?>
                                </div>
                            <?php endif; ?>
                            <?php $i++; ?> 
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </form>
</div>

<div class="modal fade" id="successModal" tabindex="-1" aria-labelledby="successModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="successModalLabel">Success</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                Data successfully saved.
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        var selectedOrder = [];

        // Function to handle form submission
        $('#manage-curriculum').submit(function(e) {
            e.preventDefault();
            var CurriculumID = <?php echo isset($CurriculumID) ? $CurriculumID : 'null' ?>;
            var SubjectIDs = selectedOrder;

            // Perform AJAX request to insert data into curriculumsubject table
            $.ajax({
                url: 'process4.php',
                method: 'POST',
                data: { CurriculumID: CurriculumID, SubjectIDs: SubjectIDs },
                success: function(response) {
                    $('#successModal').modal('show'); // Show success modal
                    setTimeout(function() {
                        location.reload(); // Reload the page after 2 seconds
                    }, 1000);
                },
                error: function(xhr, status, error) {
                    console.error(xhr.responseText);
                    // Handle error here
                }
            });
        });

        // Search functionality
        $('#searchInput').on('keyup', function() {
            var searchText = $(this).val().toLowerCase();
            $('.form-check-label').each(function() {
                var text = $(this).text().toLowerCase();
                var $checkbox = $(this).prev('.form-check-input');
                if (text.includes(searchText)) {
                    $checkbox.closest('.form-check').show();
                } else {
                    $checkbox.closest('.form-check').hide();
                }
            });
        });

        // Track selection order
        $(document).on('change', '.subjectCheckbox', function() {
            var subjectID = $(this).val();
            if ($(this).is(':checked')) {
                selectedOrder.push(subjectID);
            } else {
                selectedOrder = selectedOrder.filter(function(id) {
                    return id !== subjectID;
                });
            }
        });
    });
</script>
