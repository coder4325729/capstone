<?php
include 'querys.php';
include 'gethighlight.php';
// Initialize $studentsData as an empty array

$studentsData = [];
$status_name = array("None", "None", "NC", "Passed", "Failed", "FA", "INC");
$status_color = array("White", "Green", "#add8e6", "#EE6B6E", "Red", "Black");
// Check if the student code is provided in the URL
if(isset($_GET['student_code'])) {
    $student_code = $_GET['student_code'];
    // Connect to the database
    $conn = mysqli_connect("localhost", "root", "", "eas_db");

    // Check the connection
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }

    // Retrieve student ID and information for the specific student
    $query = "SELECT id, firstname, middlename, lastname
              FROM students
              WHERE student_code = '$student_code'";

    $result = mysqli_query($conn, $query);

    if ($result && mysqli_num_rows($result) > 0) {
        // Fetch student data
        $row = mysqli_fetch_assoc($result);
        
        // Assign student ID to $student_id
        $student_id = $row['id'];

        // Store student information
        $studentInfo = [
            'firstname' => $row['firstname'],
            'middlename' => $row['middlename'],
            'lastname' => $row['lastname']
        ];

        // Add student data to $studentsData array
        $studentsData[] = [
            'studentInfo' => $studentInfo
        ];
    } else {
        // If no student data found, add a message to $studentsData
        $studentsData[] = ["No student data found for student code: $student_code"];
    }
}

$student_curriculum_query = $conn->query("SELECT year FROM students WHERE id = $student_id");
$row = $student_curriculum_query->fetch_assoc();
$year = $row['year'];

// Query to retrieve the school year based on the curriculum ID
$schoolyear_query = $conn->query("SELECT syear FROM curriculum WHERE syear = $year");
$row = $schoolyear_query->fetch_assoc();
$schoolyear = $row['syear'];
?>
<style>
 .btn-square {
    width: 50px; /* Adjust the width as needed */
    height: 50px; /* Same as the width to make it square */
    border-radius: 0;
    background-color: green; /* Removes rounded corners */
}

/* Style for the fixed button */
#saveGradesBtn {
    position: fixed;
    bottom: 20px; /* Adjust the distance from the bottom */
    right: 20px; /* Adjust the distance from the right */
    z-index: 9999; /* Ensure the button stays above other content */
}


    @media print {
        table {
            width: 45% !important; 
            float: left; 
            margin-right: 5%; 
            margin-bottom: 20px;
            font-size: 80%; 
        }

        .card-body {
            overflow: auto; 
        }
    }


/* Adjust table styles for smaller screens */
@media (max-width: 768px) {
    .col-6 {
        width: 100%;
    }
}
</style>

<div>
<div class="no-print">
    
    <button id="printButton" class="btn" style="background-color: #1F3761; color: white;" data-id="<?php echo $student_id; ?>">Print</button>
    
    </div>
    <div class="card card-outline card-secondary">
        <div class="card-header">
            <div class="card-tools">
            </div>
        </div>
        <div class="card-body row">
            <?php
            // Loop through each curriculum
            for ($i = 1; $i <= 8; $i++) {
                if ($i > 8) {
                    // Fetch the curriculum data from the database for the descriptive column with a specific condition
                    $curriculumTitle_query = $conn->query("SELECT descriptive FROM curriculum WHERE semester = $i AND syear = $schoolyear");
                    $curriculumTitle_row = $curriculumTitle_query->fetch_assoc();
                    $curriculumTitle = isset($curriculumTitle_row['descriptive']) ? $curriculumTitle_row['descriptive'] : 'No data available';
                } else {
                    $curriculumTitle = "Year " . ceil($i / 2) . " - " . ($i % 2 == 1 ? "First" : "Second") . " Semester";
                }
                
                // Fetch data from the database
                $qry = $conn->query(select_StudentCurriculumSubject(2024, $i, $student_id));
                $r = array();
                while ($row = $qry->fetch_assoc()) {
                    if (empty($r)) {
                        $r = [$row['Pencode'] => $row];
                    } else {
                        if  (array_key_exists($row['Pencode'], $r)) {
                            $postreq = "," . $row['Postrequisite'];
                            $r[$row['Pencode']]['Postrequisite'] .= $postreq;
                        } else {
                            $r[$row['Pencode']] = $row;
                        }
                    }
                }
                
                // Check if there are any rows fetched for this curriculum
                if (!empty($r)) {
                    ?>
                    <div class="col-12 col-md-6 mb-4">
                        <h5><?php echo $curriculumTitle; ?></h5>
                        <div class="table-responsive">
                            <table class="table tabe-hover table-bordered" id="list_<?php echo $i; ?>">
                                <thead>
                                    <tr>
                                        <th>Pen Code</th>
                                        <th>Descriptive Title</th>
                                        <th>Lec</th>
                                        <th>Lab</th>
                                        <th>Total</th>
                                        <th>Pre-requisite</th>
                                        <th>Grade</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($r as $key => $value) {
                                        $row = $value;
                                        ?>
                                        <tr>
                                            <td class="col-2" id="penCodeColumn_<?php echo formatpencode($row['Pencode']) ?>">
                                                <b id="pencode_<?php echo formatpencode($row['Pencode']) ?>" class="pencode <?php echo gethighlight($row['Grade'], $row['Prerequisite']) ?>"><?php echo ucwords($row['Pencode']) ?></b>
                                            </td>
                                            <td class="col-3"><?php echo ucwords($row['Description']) ?></td>
                                            <td class="col-1"><b><?php echo ucwords($row['Lec']) ?></b></td>
                                            <td class="col-1"><?php echo ucwords($row['Lab']) ?></td>
                                            <td class="col-1"><?php echo ucwords($row['Lab'] + $row['Lec']) ?></td>
                                            <td class="col-1" ><?php echo ucwords($row['Prerequisite']) ?></td>
                                            <td class="col-1" id="grade_<?php echo formatpencode($row['Pencode']) ?>" data-grade-id="<?php echo $row['StudentCurriculumSubjectID'] ?>"><?php echo ucwords($row['Grade']) ?></td>
                                            <td class="col-1 status_column"><b><?php echo ucwords($status_name[$row['Status']]) ?></b></td>
                                        </tr>
                                        <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
            <?php
                }
            }
            ?>
        </div>
        
    </div>
</div>

<script>
$(document).ready(function(){
    $('#list').dataTable();

    $('.delete_StudentCurriculumSubject').click(function(){
        var studentCurriculumSubjectID = $(this).attr('data-id');
        if(confirm("Are you sure to delete this Subject?")) {
            $.ajax({
                url: 'delete_scs.php',
                method: 'POST',
                data: { StudentCurriculumSubjectID: studentCurriculumSubjectID },
                success: function(resp){
                    alert(resp);
                    location.reload();
                },
                error: function(xhr, status, error) {
                    console.error(xhr.responseText);
                }
            });
        }
    });

    $('.insert_grade').click(function(){
        uni_modal("Manage Curriculum","insert_grade.php?StudentCurriculumSubjectID="+$(this).attr('data-id'));
    });

    // Function to update Pencode color based on grade input and prerequisite
    // function updatePencodeColor(gradeInput, pencodeCell, prerequisite) {
    //     console.log("Prerequisite:", prerequisiteCellId);
    //     console.log("Grade:", gradeCellId);
    //     console.log("Grade Cell:", gradeInput);
    //     console.log("pencodeCell:", pencodeCellId);
    //     var grade = parseInt(gradeInput.innerText);

    //     if (!isNaN(grade) && grade >= 1 && grade <= 100) {
    //         if (grade >= 50) {
    //             pencodeCell.style.color = 'black';
    //             pencodeCell.parentElement.classList.add('highlight');
    //         } else {
    //             pencodeCell.style.color = 'green';
    //             pencodeCell.parentElement.classList.remove('highlight');
    //         }

    //         // Check if pencodeCell color is black
    //         if (pencodeCell.style.color === 'black') {
    //             var allPencodes = $('[id^="pencode_"]');
    //             var foundMatch = false;
    //             allPencodes.each(function() {
    //                 var prevPencode = $(this);
    //                 if (prevPencode.text() === prerequisiteCellId && prevPencode.css('color') === 'rgb(0, 0, 0)') {
    //                     pencodeCell.style.color = 'green';
    //                     pencodeCell.parentElement.classList.add('highlight');
    //                     foundMatch = true;
    //                     return false; // Exit each loop
    //                 }
    //             });

    //             // If no match found and the color is still black, remove highlight
    //             if (!foundMatch) {
    //                 pencodeCell.parentElement.classList.remove('highlight');
    //             }
    //         }
    //     }
    // }
    function updatePencodeColor(pencode, grade, is_postreq = false){
        clearpencodecolor(pencode)
        var color = "pencodered"; 
        var postreq_grade = 0;
        if (is_postreq){
            postreq_grade = $("#grade_" + pencode).text();
            console.log(postreq_grade);
            if (grade >= 50){
                if(postreq_grade >= 50){
                    color = "pencodeblack";
                }
                else{
                    color = "pencodegreen";
                }
            }
        }
        else{
            if (grade >= 50){
                color = "pencodeblack";           
            }   
            else if (grade > 0){
                color = "pencodegreen";
            }
            else{ 
            }
        }
       
        $("#pencode_" + pencode).addClass(color);
    }

    function updatepostreq(pencode, grade){
        pencode
    }

    function clearpencodecolor(pencode){
        $("#pencode_"+ pencode).removeClass("pencodered pencodegreen pencodeblack")
    }

    function pencodetopostrequisite(pencode, postrequisite) {
  var similarPostrequisites = {}; 

  
  if (!similarPostrequisites[pencode]) {
    similarPostrequisites[pencode] = postrequisite;
  } else {

    similarPostrequisites[pencode] += ', ' + postrequisite;
  }

 
  console.log('Pencode:', pencode, 'Postrequisites:', similarPostrequisites[pencode]);
}

// Event listener for grade input
$('[contenteditable="true"]').on('input', function() {
  var studentCurriculumSubjectID = $(this).data('grade-id');
  var gradeInput = this;
  var pencode = $(this).attr('id');
  pencode = pencode.replace("grade_", "");
  var grade = $(this).text();
  var prerequisite = $(this).siblings('td[data-prerequisite-id]').data('prerequisite-id');
  var postrequisites = $(this).siblings('td[data-postrequisite-id]').data('postrequisite-id')
  var postrequisite = postrequisites.split(',');

 

//   pencodetopostrequisite(pencode, postrequisite)
  
    
   //var prerequisiteCellId = '#prerequisite_' + studentCurriculumSubjectID;
   // var pencodeCellId = '#pencode_' + studentCurriculumSubjectID;
    //  var gradeCellId = '#grade_' + pencode;
    // var prerequisite = $(prerequisiteCellId).text(); // Get the prerequisite value from the cell
    updatePencodeColor(pencode, grade);
    // console.log("ABC", pencode, postrequisite);
    for (p in postrequisite){
        // console.log("DEF", postrequisite[p]);
        updatePencodeColor(postrequisite[p], grade, true)
        
    }
    
    

    //Automatically save grades when input changes
     var formData = [];
     var grade = $(this).text();
     formData.push({ StudentCurriculumSubjectID: studentCurriculumSubjectID, grade: grade });
    
        var success = false;
        $.ajax({
            url: 'process2.php',
            method: 'POST',
            data: { formData: formData },
            success: function(resp) {
                console.log(resp);
                success = true;
            },
            error: function(xhr, status, error) {
                console.error(xhr.responseText);
            }
        });
      $
    });
   
    // Trigger highlighting on page load
    // $('[contenteditable="true"]').each(function() {
    //     var studentCurriculumSubjectID = $(this).data('grade-id');
    //     var gradeInput = this;
    //     var pencodeCell = document.getElementById('pencode_' + studentCurriculumSubjectID);
    //     var prerequisiteCell = $(this).closest('tr').find('#prerequisite_' + studentCurriculumSubjectID); // Target the prerequisite cell
    //     var prerequisite = prerequisiteCell.text(); // Get the prerequisite value from the cell
    //     updatePencodeColor(gradeInput, pencodeCell, prerequisite);
    
    
    // the commented code below is my attempt to call the pencode id using the nearest td
    function getPenCodeIdByPrerequisite(prerequisiteId) {
        var penCodeId = null; // Initialize penCodeId to null

        // Loop through each row in the table body
        $("#list_<?php echo $i; ?> tbody tr").each(function() {
            var prerequisiteTd = $(this).siblings('td[data-prerequisite-id="' + prerequisiteId + '"]').data('prerequisite-id');  // Find Prerequisite column by ID

            if (prerequisiteTd.length) { // If Prerequisite column exists in this row
                penCodeId = $(this).find('td:first-child').attr('id'); // Get the ID of the first column (PenCode column)
                return false; // Exit the loop once the PenCode ID is found
            }
        });

        return penCodeId; // Return the PenCode column ID (or null if not found)
    }
    
   


    // Example usage:
    


});
document.getElementById('printButton').addEventListener('click', function() {
    var studentId = this.getAttribute('data-id');
    var printWindow = window.open('print.php?id=' + studentId, '_blank');
    printWindow.onload = function() {
// Hide date/time and text for printing
        printWindow.document.querySelectorAll('.no-print').forEach(function(element) {
            element.style.display = 'none';
        });
        printWindow.print();
    };
});



</script>
