<?php
function gethighlight($grade, $prerequisite){

    if (strtolower($prerequisite) == "none"){
        if ($grade >= 50)
        {
            return "pencodeblack";
        }      
        else {
            return "pencodegreen";
        }
    }
    else {
        // Implement logic to handle prerequisite
        // For now, let's assume if there's a prerequisite, it always returns 'pencodered'
        return "pencodered";
    }
}

function getPrereqGrade(){
    // Implement logic to retrieve prerequisite grade
}

function formatpencode($pencode){
    return str_replace(" ","",$pencode);
}

function formatid($name, $value){
    return  $name."_".str_replace(" ","",$value);
}
?>
